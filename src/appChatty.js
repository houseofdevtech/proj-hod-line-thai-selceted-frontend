import React, { Component } from 'react'
import { connect } from 'react-redux'

import Loading from './views/loadingPage'
import AppRoute from './appRoute'

// [Purpose]
// -Chatty api - checkUser
// If have, store userid and passed to other step
// If not have, call CreateUser
// - createUser and store userid and passed to other step
// If create User is have been done, chattyUserId === ''
// this will lock down in Loading Page

class AppChatty extends Component {
  _isUpdated = false

  state = {
    isLineUserId: false,
  }

  componentDidMount() {
    // 0. Manually set ChattyUserId
    // this.props.setChattyUserId('wittest123')
    // 1. Check ChattyUserId from line_id (line.userId)
    // If unsuccess check it will call createUser() to create User immediately
    // ***** No set line userId anymore due to by pass userId ******
    // this.props.checkUser(this.props.user.line.userId)
  }

  componentDidUpdate() {
    if (this.props.user.line.userId && !this.state.isLineUserId) {
      this.props.checkUser(this.props.user.line.userId)
      this.setState({ isLineUserId: true })
    }
  }

  render() {
    console.log(`appChatty: user.chattyUserId = ${this.props.user.chattyUserId}`)
    // console.log(`user :: ${JSON.stringify(this.props.user)}`)

    const { chattyUser, chattyUserId } = this.props.user
    return (
      <div>
        {chattyUser === true && (chattyUserId === '' || chattyUserId === undefined) ? (
          <Loading label="Error retrieve userId" />
        ) : (
          <AppRoute />
        )}
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  user: state.user,
})

const mapDispatchToProps = (dispatch) => {
  return {
    checkUser: dispatch.user.checkUser,
    setChattyUserId: dispatch.user.setChattyUserId,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppChatty)
