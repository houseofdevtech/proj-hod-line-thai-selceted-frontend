import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import _ from 'lodash'

class AppControl extends Component {
  componentDidMount() {
    const qString = queryString.parse(this.props.location.search)
    console.log(`qString page : ${JSON.stringify(qString)}`)
    if (_.isEmpty(qString) || !('page' in qString)) {
      // console.log('push not found')
      // console.log(` isEmpty ::${_.isEmpty(qString) }`)
      // console.log(` isPage ::${'page' in qString }`)

      // console.log(`qString page : ${JSON.stringify(qString)}`)
      if ('liff.state' in qString) {
        const params = new URLSearchParams(qString['liff.state'])
        console.log(`params:: ${params}`)
        // ?liff.state=%3Fpage%3Dproduct%26shop_id%3D9%26product_id%3D307
        // {"liff.state":"?page=product&shop_id=9&product_id=307"}
        // translate query string behind liff.state into Object

        let liffState_qString = {}
        for (let entry of params.entries()) {
          console.log(entry[0] + ', ' + entry[1])
          const [key, value] = entry
          if (key.includes('/?')) {
            console.log('contain /?')
            const key2 = key.replace('/?', '')
            liffState_qString[key2] = value
          } else {
            console.log('not contain')
            liffState_qString[key] = value
          }
        }
        // { page: product, shop_id : 9 , product_id  :307}
        console.log(`liffState_qString :: ${JSON.stringify(liffState_qString)}`)
        this.queryControl(liffState_qString.page, liffState_qString)
      } else {
        this.props.history.push('/notFound')
      }
    } else {
      console.log(`[Product Page] qString is not Empty`)
      console.log(`qString page : ${JSON.stringify(qString)}`)

      this.queryControl(qString.page, qString)
    }
  }

  // ***************************************************
  // *** Control query string and push page to route ***
  // ***************************************************
  //
  // Example::
  // http://localhost:3000/proj-hod-line-shop-buyer-frontend/ask
  // http://{{host}}/proj-hod-line-shop-buyer-frontend/?page=product
  //
  // ***************************************************
  queryControl = (page, qsObj) => {
    page === 'product'
      ? this.props.history.push('/product', qsObj)
      : page === 'detail'
      ? this.props.history.push('/detail', qsObj)
      : page === 'ask'
      ? this.props.history.push('/ask', qsObj) // {shop_id : 42, product_id : 42, external : true}
      : page === 'list'
      ? this.props.history.push('/list')
      : page === 'edit'
      ? this.props.history.push('/edit', qsObj)
      : page === 'payment'
      ? this.props.history.push('/address', qsObj)
      : // ? this.props.history.push('/emptyorder', qsObj)
      page === 'tracking'
      ? this.props.history.push('/tracking')
      : page === 'mystatus'
      ? this.props.history.push('/mystatus')
      : page === 'loading'
      ? this.props.history.push('/loading', qsObj)
      : page === 'address'
      ? this.props.history.push('/address')
      : page === 'checkpayment'
      ? this.props.history.push('/checkpayment', qsObj)
      : page === 'slip'
      ? this.props.history.push('/slip', qsObj)
      : page === 'checkout'
      ? this.props.history.push('/checkout', qsObj)
      : // this is for temporary
        this.props.history.push('/notFound')
    // *** When site is complete if there is no match ***
    // *** it should be push page to NOT FOUND page ***
    // this.props.history.push('/notFound')
  }

  // ************************************************
  // *********** Test Query String Method ***********
  // ************************************************
  // queryString = () => {
  //   console.log(`${JSON.stringify(this.props.location.search)}`)
  //   //=> '?foo=bar'
  //   const values = queryString.parse(this.props.location.search)
  //   console.log(`values page : ${values.page}`)
  //   console.log(`values id : ${values.id}`)
  //   //=> {foo: 'bar'}
  // }

  // ***********  This component have no render ***********
  render() {
    // this.queryString()
    return <div></div>
  }
}

export default withRouter(AppControl)
