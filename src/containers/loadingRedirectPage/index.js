import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoadingScreen from 'react-loading-screen'

import Frame from '../../components/frameLayout'
import { withTheme } from 'styled-components'
import { withRouter } from 'react-router-dom'

import request from '../../utils/urlRequest'
// import logo from './logo.svg'
// <g fill="#61DAFB"></g>
// textColor="#17ceff"
// spinnerColor="#9ee5f8"
// const values = queryString.parse(this.props.location.search);

const liff = window.liff

const requestLoading = async (type, message, shop_id) => {
  let res = false
  try {
    request.interceptors.request.use((request) => {
      console.log('Starting Request', request)
      return request
    })
    request.interceptors.response.use((response) => {
      console.log('Response:', response)
      return response
    })
    if (type === 'recommend') {
      res = await request.post(`/loading/buyer/recommend/${shop_id}`, message)
    }
    if (type === 'promotion') {
      res = await request.post(`/loading/buyer/promotion/${shop_id}`, message)
    }
    if (type === 'hotdeal') {
      res = await request.post(`/loading/buyer/hotdeal/${shop_id}`, message)
    }
    if (res) {
      // console.log(res)
      if (res.data === 'OK') {
        liff.closeWindow()
      } else {
        liff
          .sendMessages([
            {
              type: 'text',
              text: res.data.message,
            },
          ])
          .then(() => {
            // console.log('message sent')
            liff.closeWindow()
          })
      }
      return true
    }
  } catch (err) {
    console.log(err)
    // liff.closeWindow()
  }
}

class LoadingRedirectPage extends Component {
  _isLoaded = false
  state = {
    text: 'Loading',
  }

  componentDidMount = async () => {}

  componentDidUpdate() {
    console.log('DidUpdate')
    const values = this.props.location.state
    if (this.isLineUserId(this.props) && !this._isLoaded) {
      this._isLoaded = true
      const shop_id = values.shop_id

      let message = {
        source: {
          line: this.props.user.line,
        },
        loading: {
          isLoading: 'true',
          loadingType: values.type,
        },
      }
      console.log(`values:: ${JSON.stringify(values)}`)
      console.log(message)
      if (values.type !== 'promotion' && values.type !== 'recommend' && values.type !== 'hotdeal') {
        console.log('Wrong Type')
      } else {
        // res = await requestLoading(values.type, JSON.stringify(message),shop_id)
        console.log('>>>>>>>> trigger loading <<<<<<')
        requestLoading(values.type, message, shop_id)
      }
    }
  }

  isLineUserId = (props) => {
    const result = props.user.line.userId !== ''
    // Check DisplayName must have been loaded by liff.init first
    // && (props.user.line.displayName !== '' && props.user.line.displayName !== undefined)
    console.log(`isLineUserId :: ${result}`)
    console.log(`user.line :: ${JSON.stringify(props.user.line)}`)
    // console.log(props.user.line.displayName)
    // console.log(result)
    return result
  }

  render() {
    // console.log(this.props.theme)
    // console.log(this.props.location)
    return (
      <Frame
        primaryColor={this.props.theme.secondary}
        secondaryColor={'#fff'}
        topHeight="20vh"
        bodyHeight="50vh">
        <LoadingScreen
          loading={true}
          bgColor="#ffffff"
          spinnerColor={this.props.theme.secondary}
          textColor={this.props.theme.primary}
          //logoSrc={logo}
          text={this.state.text}
        />
      </Frame>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = () => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(withRouter(LoadingRedirectPage)))
