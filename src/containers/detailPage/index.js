import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import ManageState from './detailManage'
import LoadingScreen from '../../views/loadingPage'

import { isValidProductQS } from '../../utils/assist'

class DetailLandingPage extends Component {
  state = {}

  componentDidMount = async () => {

    // *** this is for test ***
    // *** to make query string directly to this page ***
    // *** with product_detail from api ***
    const qsObject = this.props.location.state
    console.log(`detail page qs:: ${JSON.stringify(qsObject)}`)
    if (isValidProductQS(qsObject)) {
      // if all queryString have been validated, then send to api method to call
      await this.props.getProduct(qsObject)
    } else {
      console.log(`incomplete queryString Do Something `)
    }
  }


  isRecievedData = (state) => {
    return 'product' in state.product
  }

  render() {
    console.log(this.props.product)
    // console.log(`handleBack :: ${this.props.handleBack}`)
    return (
      <div>
        {!this.isRecievedData(this.props) ? <LoadingScreen /> : <ManageState {...this.props} />}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  product: state.product,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    getProduct: dispatch.product.getProduct,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DetailLandingPage))
