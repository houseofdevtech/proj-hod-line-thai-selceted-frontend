import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import Page from './detailPage'

class ManageDetailState extends Component {
  state = {
    // ... from component did mount

    selected_items: [],
    selectedTotalAmount: 0,
    selectedTotalPrice: 0,
    preload: false,
    selected: false,
  }

  async componentDidMount() {
    // this.forceUpdate()
    const productsData = await this.props.product.product
    // console.log(product)
    // await this.props.getProduct(this.props.qsObject)
    // const product = this.props.data
    // make sure that product is get only one shop and one product

    // if (productsData && productsData.length === 1 && this.state.preload === false) {
    if (productsData && this.state.preload === false) {
      console.log('key:"product" in state and only has 1 product_id')
      // console.log(`product :: ${product[0].product_data[0].product_items}`)
      // Extract Items inside the shop
      const Items = productsData.product_data[0].product_items
      const images = this.setImage(Items)
      const minPrice = this.setMinPrice(Items)
      const colorTemplate = this.setColor(Items)
      const product_name = productsData.product_data[0].product_name
      const product_id = productsData.product_data[0].product_id
      const product_detail = productsData.product_data[0].detail
      const sizes = this.setSize(Items)

      // Set Render Array for spread_products [{ size: '' product_items: [] }]
      const spread_products = this.setDetailRender(sizes, Items)

      // Start Order
      this.props.setOrderUserId(this.props.user.chattyUserId)
      this.props.setOrder({ product_id: product_id })
      this.props.location.state.shop_id &&
        this.props.setOrder({ shop_id: this.props.location.state.shop_id })

      this.setState({
        images: images,
        colors: colorTemplate,
        product_minPrice: minPrice,
        product_name: product_name,
        product_detail: product_detail,
        spread_products: spread_products,
        sizes: sizes,
        // colorXsize: colorXsize,
        preload: true,
      })
    } else {
      console.log('more than one product')
    }
  }

  componentDidUpdate(prevState) {
    if (
      prevState.selectedColor !== this.state.selectedColor &&
      this.state.selectedColor !== null &&
      // selected interlocking prevent loop
      this.state.selected
    ) {
      console.log(`DidUpdate`)

      // console.log(this.findSizeXColor(this.state.selectedColor))
      const sizes = this.findSizeXColor(this.state.selectedColor)
      // reset Size selected
      this.setState({ sizes: sizes, selectedSize: null, selected: false })
      // e.preventDefault()
    }
    // if (this.state.selectedColor !== null && this.state.selectedSize !== null ){
    //   const { selectedColor, selectedSize, selectedAmount} = this.state
    //   const product = this.props.product.product
    //   const Items = product[0].product_data[0].product_items
    //   console.log(`${JSON.stringify(selectedColor)} || ${selectedSize}`)
    //   const selectedProduct = this.findSelectedProduct(Items,this.state.selectedColor,this.state.selectedSize)
    //   this.setState({

    //     selectedPrice: selectedProduct.price,
    //     // if current Amount more than max amount
    //     // [then] set to max amount (selectedProduct.qty)
    //     // [else] set to current Amount (selectedAmount)
    //     selectedAmount: selectedAmount > selectedProduct.qty? selectedProduct.qty : selectedAmount,
    //     selectedMaxAmount:selectedProduct.qty })
    //   // console.log(sku_id)
    // }
  }

  setImage = (product_items) => {
    let images = []
    // push 1st image from "main_img_path"
    images.push(product_items[0].main_img_path)
    // push other images fron "sub_img_path"
    product_items[0].sub_img_path.map((obj) => images.push(obj.img_path))
    // return array of images url
    return images
  }

  setMinPrice = (product_items) => {
    let prices = []
    product_items.map((obj) => prices.push(obj.price))
    // console.log(prices)
    // console.log (`Math :: ${_.min( prices )}`)
    // return min Price from array
    return _.min(prices)
  }

  setColor = (product_items) => {
    const colorTemplate = []
    product_items.map((obj) => colorTemplate.push(obj.color))
    // filter duplicate field
    const keys = ['color_name', 'color_value']
    const filtered = colorTemplate.filter(
      ((s) => (o) => ((k) => !s.has(k) && s.add(k))(keys.map((k) => o[k]).join('|')))(new Set())
    )

    return filtered
  }

  setSize = (product_items) => {
    const sizeTemplate = []
    product_items.map((obj) => sizeTemplate.push(obj.size))
    // filter duplicate field
    const result = _.uniq(sizeTemplate)

    return result
  }

  isEqualColor = (c1, c2) => {
    return _.isEqual(c1.color_value, c2.color_value) && _.isEqual(c1.color_name, c2.color_name)
  }

  findSizeXColor = (color) => {
    // console.log(color)
    let result = null
    this.state.colorXsize.map((obj) => {
      // color && console.log(` ${obj.color.color_value} :: ${color.color}`)
      if (color && this.isEqualColor(obj.color, color)) {
        // console.log(obj.sizes)
        result = obj.sizes
      }
      return null
    })
    return result
  }

  // findSelectedProduct = (product_items, color, size) => {
  //   const result = product_items.filter(obj => this.isEqualColor(obj.color,color) && obj.size === size )
  //   // console.log(result[0].sku_id)
  //   return result[0]
  // }

  setDetailRender = (sizesArray, product_items) => {
    const detail_render = sizesArray.map((size) => {
      return {
        size: size,
        sizeSelectedAmount: 0,
        product_items: product_items.filter((obj) => obj.size === size),
      }
    })
    console.log(`detail :: ${JSON.stringify(detail_render)}`)
    return detail_render
  }

  handleSelect = (key, value) => {
    this.setState({ ...this.state, [key]: value, selected: true })
  }

  isRecievedData = (state) => {
    return 'product' in state.product
  }

  render() {
    // console.log(`manage State :: ${JSON.stringify(this.state)}`)
    // console.log(`manage Props :: ${JSON.stringify(this.props.product)}`)
    // console.log(`handleBack :: ${this.props.handleBack}`)
    return (
      <div>
        {//If there is spread_product (render field)
        'spread_products' in this.state && (
          <div>
            <Page data={this.state} handleSelect={this.handleSelect} {...this.props} />
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  product: state.product,
  user: state.user,
})

const mapDispatchToProps = (dispatch) => {
  return {
    getProduct: dispatch.product.getProduct,
    setOrderUserId: dispatch.order.setOrderUserId,
    setOrder: dispatch.order.setOrder,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageDetailState)
