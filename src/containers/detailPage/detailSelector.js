import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import _ from 'lodash'

import {
  // Container,
  WhiteContainer,
  ThemesContainer,
  FrameContainer,
  TitleArea,
  Title,
  //Text,
  Price,
  SelectedContainer,
  BottomRowContainer,
  BottomColumnContainer,
  BottomLeftFiller,
  BottomLeftBackground,
  BottomLeftSquare,
} from '../../style/contentStyle'

import TabComponent from '../../components/tabComponent'
import TemplateDetail from './detailTemplate'

// This is Template of TabData >> TabComponent
// This is for Mockup Tab
// const tabData = [
//   {
//     label: 'S',
//     size: 1,
//     key: '0',
//     maxStock: 10,
//   },
//   {
//     label: 'M',
//     size: 0,
//     key: '1',
//     maxStock: 10,
//   },
//   {
//     label: 'L',
//     size: 0,
//     key: '2',
//     maxStock: 10,
//   },
//   {
//     label: 'XL',
//     size: 0,
//     key: '3',
//     maxStock: 10,
//   },
// ]

const WhiteContainerModify = styled.div`
  position: relative;
`
const TitleAreaModify = styled.div`
  margin-bottom: 30px;
`

class DetailContent extends Component {
  state = {
    // selectedTabIndex is start by "index" in componentDidMount
    // and control is of index on spread_product
    selectedTabIndex: 0,
    selectedItems: [],
    selectedTotalAmount: 0,
    selectedTotalPrice: 0,
  }

  componentDidMount() {
    const { spread_products } = this.props.data
    const tabData = spread_products.map((obj, index) => {
      return {
        label: obj.size,
        amount: obj.sizeSelectedAmount,
        key: index,
        active: index === this.state.selectedTabIndex ? true : false,
      }
    })
    this.setState({ ...this.state, tabData: tabData })
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState !== this.state) {
      this.props.copyState(this.state)
    }
  }

  onChangeTab = (index) => {
    const tabData = this.state.tabData.map((obj) => {
      return {
        ...obj,
        active: obj.key === index ? true : false,
      }
    })
    this.setState({
      selectedTabIndex: index,
      tabData: tabData,
    })
  }

  isExistingProduct = (arrayItems, id) => {
    return arrayItems.find((item) => item.id === id)
  }

  findExistingProduct = (arrayItems, id) => {
    return arrayItems.findIndex((item) => item.id === id)
  }

  updateTabData = (selectedItems) => {
    // console.log('updateTabData')
    const { spread_products } = this.props.data
    // Prepare Array From Spread_product map with selectedItems
    // {"size":"S","sku_ids":[1510,1511,1514],"amount":[1,0,0]}
    const min_spread_products = spread_products.map((o) => {
      return {
        size: o.size,
        sku_ids: o.product_items.map((p) => p.sku_id),
        amount: o.product_items.map((p) => {
          const inside = selectedItems.map((q) => {
            // console.log(`p - q :: ${p.sku_id} ===  ${q.id} `)
            return p.sku_id === q.id ? q.qty : 0
          })
          const reduceInside = inside.reduce((acc, ini) => acc + ini, 0)
          // console.log(reduceInside)
          return reduceInside
        }),
      }
    })

    // Reduce Amount from array into Integer(number)
    const reduce_min = min_spread_products.map((o) => {
      return {
        ...o,
        amount: o.amount.reduce((acc, ini) => acc + ini, 0),
      }
    })
    // console.log(`min_spread_product::${JSON.stringify(min_spread_products)}`)
    // console.log(`reduce_min::${JSON.stringify(reduce_min)}`)

    // Assign new amount on tabData
    const tabData = this.state.tabData.map((o) => {
      let obj = {}
      reduce_min.map((p) => {
        // let obj = {}
        if (o.label === p.size) {
          obj = {
            ...o,
            amount: o.label === p.size && p.amount,
          }
        }
        return null
      })
      return obj
    })
    // console.log(`tab_Data::${JSON.stringify(tabData)}`)
    return tabData
  }

  setStateSelectedItems = (selectedItems) => {
    // console.log(selectedItems)
    const allQty = selectedItems.map((o) => o.qty)
    const totalQty = !_.isEmpty(allQty) ? allQty.reduce((acc, ini) => acc + ini) : 0
    console.log(`total :: ${totalQty}`)
    const ProductsData = this.props.product.product
    const product_items = ProductsData.product_data[0].product_items
    // prepare array of price x quantity
    // example :: [{"price":251,"qty":3},{"price":252,"qty":2}]
    const prices = selectedItems.map((o) => {
      let res = {}
      product_items.filter((p) => p.sku_id === o.id).map((q) => (res.price = q.price))
      res.qty = o.qty
      return res
    })
    // reduce (calculate total price) == (price x qty)
    const totalPrice = prices.reduce((acc, obj) => acc + obj.qty * obj.price, 0)
    // console.log(`prices::${JSON.stringify(prices)}`)
    // console.log(`totalPrice::${(totalPrice)}`)
    const tabData = this.updateTabData(selectedItems)

    this.setState({
      selectedItems: selectedItems,
      selectedTotalAmount: totalQty,
      selectedTotalPrice: totalPrice,
      tabData: tabData,
    })
  }

  onChangeSelectedItems = (id, value) => {
    const { selectedItems } = this.state
    // console.log(`id :: ${id}`)
    // console.log(`value :: ${value}`)
    const index = this.findExistingProduct(selectedItems, id)
    // console.log(this.isExistingProduct(selectedItems, id))
    // console.log(index)
    if (this.isExistingProduct(selectedItems, id)) {
      if (value !== 0) {
        // if Value is not Zero => adjust cart value
        let selectedItems = this.state.selectedItems
        selectedItems[index].qty = value
        this.setStateSelectedItems(selectedItems)
      } else {
        // if Value is Zero => remove from cart
        let selectedItems = this.state.selectedItems
        const resultItems = selectedItems.filter((o) => o.id !== id)
        // this.setState({ selectedItems: resultItems })
        this.setStateSelectedItems(resultItems)
      }
    } else {
      let selectedItems = this.state.selectedItems
      // this.setState({ selectedItems: [...selectedItems, { id: id, qty: 1 }] })
      selectedItems.push({ id: id, qty: 1 })
      this.setStateSelectedItems(selectedItems)
    }
  }

  render() {
    // const { title, price } = this.props.data
    const { product_name, product_minPrice, spread_products } = this.props.data
    const { selectedTabIndex, selectedItems } = this.state

    // console.log(this.props.data)
    console.log(`detailSelector state: ${JSON.stringify(this.state)}`)
    // console.log(`this state selectedItems: ${JSON.stringify(this.state.selectedItems)}`)
    return (
      <ThemesContainer>
        {//If ComponentDidMount will resolve
        // There is tabData in this state to render
        'tabData' in this.state && (
          <WhiteContainerModify>
            <WhiteContainer>
              <FrameContainer paddingBottom={'0px'}>
                <TitleAreaModify>
                  <TitleArea>
                    <Title> {product_name}</Title>
                    <Price>{`${product_minPrice.toLocaleString('th')} THB`}</Price>
                  </TitleArea>
                </TitleAreaModify>
                <TabComponent
                  transparent
                  tabData={this.state.tabData}
                  onChangeTab={this.onChangeTab}
                />
              </FrameContainer>
              <BottomColumnContainer>
                <BottomRowContainer>
                  <BottomLeftFiller>
                    <BottomLeftBackground>
                      <BottomLeftSquare />
                    </BottomLeftBackground>
                  </BottomLeftFiller>
                  <ThemesContainer>
                    <SelectedContainer>
                      <TemplateDetail
                        spread_products={spread_products[selectedTabIndex]}
                        selectedItems={selectedItems}
                        onChangeSelectedItems={this.onChangeSelectedItems}
                      />
                    </SelectedContainer>
                  </ThemesContainer>
                </BottomRowContainer>
              </BottomColumnContainer>
            </WhiteContainer>
          </WhiteContainerModify>
        )}
      </ThemesContainer>
    )
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  product: state.product,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailContent)
