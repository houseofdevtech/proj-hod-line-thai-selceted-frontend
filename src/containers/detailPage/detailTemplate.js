import React, { Component } from 'react'
import { connect } from 'react-redux'

import styled from 'styled-components'
import { darken } from 'polished'
import _ from 'lodash'

// import NumberInputToggleSmall from '../../components/numberInputToggleSmall'
import NumberInputToggle from '../../components/numberInputToggleDetail'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 5vw 10vw 10px 8vw;
  @media only screen and (min-width: 451px) {
    padding: 20px 50px 10px 50px;
  }

  // margin: 5px 0px 0px;
  // justify-content: flex-end;
  // background: #efefef;
`
const ColorContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 10;
`

const Circle = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;

  background: ${(props) => `${props.color}`};
  border : ${(props) => {
    return props.color === '#ffffff' ? `1px solid ${props.theme.primary}` : `none`
  }}
  border-radius: 30px;
  width: 10vw;
  height: 10vw;

  // div:focus {
  //   border: 2px solid ${(props) => `${darken(0.1, `${props.theme.primary}`)}`};
  // }

  @media only screen and (min-width: 451px) {
    width: 40px;
    height: 40px;
    border-radius: 40px;
  }
 
`

const Text = styled.div`
  font-size: 4vw;
  margin-bottom: 5px;
  @media only screen and (min-width: 451px) {
    font-size: 1em;
    margin-bottom: 5px;
  }
`

const SelectionContainer = styled.div`
  display: flex;
  flex:1;
  justify-content: space-between;
  align-items: flex-end;
  width:100%
  height: max-content;
  margin-bottom: 10px;
`

class TemplateDetail extends Component {
  state = {
    colors: [],
    // colors: [
    //   {
    //      key : [number]
    //      color : #color code
    //      name : 'color name'
    //      selected : #false
    //   },
    // ]
    //
  }

  InitialState() {
    const { product_items } = this.props.spread_products
    console.log(`product_items :: ${JSON.stringify(product_items)}`)
    // const { colors } = this.props
    let result = []
    if (product_items) {
      const colorDistribute = product_items.map((obj, index) => {
        return {
          key: obj.sku_id,
          color: obj.color.color_value,
          name: obj.color.color_name,
          maxQty: obj.qty,
          selected: false,
        }
      })
      return colorDistribute
    } else {
      console.log(`detailTemplate :: there is no product_items from this.props.spread_products`)
    }
    return result
  }

  componentDidMount() {
    this.setState({
      colors: this.InitialState(),
    })
  }

  componentDidUpdate(prevProp) {
    if (prevProp !== this.props)
      this.setState({
        colors: this.InitialState(),
      })
  }

  handleChange = (e) => {
    console.log(`id : ${e.target.id}`)
    // console.log(this.state.colors[0])
    const id = e.target.id

    // Set Main Redux State
    this.state.colors[id].selected === true
      ? this.props.setCartColor('')
      : this.props.setCartColor(this.state.colors[id].color)

    // Set Local State if the Item is selected or not
    this.setState((prevState) => ({
      colors: prevState.colors.map((obj) =>
        // make same data type and compare
        String(obj.key).valueOf() === String(id).valueOf()
          ? { ...obj, selected: !obj.selected }
          : { ...obj, selected: false }
      ),
    }))
  }

  showValue = (selectedItems, key) => {
    // console.log(`showValue -> selectedItems :: ${JSON.stringify(selectedItems)}`)
    const value = selectedItems.filter((o) => {
      if ('id' in o) {
        return o.id === key
      } else {
        return null
      }
    })
    // console.log(`this showValue :: ${JSON.stringify(value)}`)
    const result = _.isEmpty(value) ? 0 : value[0].qty
    // console.log(`showValue :: ${result}`)
    return result
  }

  render() {
    const { colors } = this.state
    // console.log(colors);
    console.log(this.state)

    return (
      <Container>
        {colors.map((obj, i) => (
          //Add 'key' to fix warning
          //on templateDetail the circle is remove onClick to unresponse change
          <SelectionContainer key={i}>
            <ColorContainer color={obj.color} selected={obj.selected}>
              <Text>{obj.name}</Text>
              <Circle color={obj.color} selected={obj.selected}></Circle>
            </ColorContainer>
            {/* {this.showValue(this.props.selectedItems,obj.key)} */}
            <NumberInputToggle
              id={obj.key}
              onChangeSelectedItems={this.props.onChangeSelectedItems}
              value={this.showValue(this.props.selectedItems, obj.key)}
              maxQty={obj.maxQty}
            />
          </SelectionContainer>
        ))}
      </Container>
    )
  }
}

const mapStateToProps = () => ({
  // cart: state.cart,
})

const mapDispatchToProps = () => {
  return {
    // setCartColor: dispatch.cart.setCartColor,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplateDetail)
