import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import styled, { withTheme } from 'styled-components'
import Button from '../../components/button'
import {
  Container,
  SubContainer,
  Header,
  Footer,
  FooterContainer,
  FooterText,
  ButtonContainer,
} from '../../style/productStyle'

// import AskSellerButton from '../../components/buttonAsk'
import CarouselSection from '../../components/carousel'
import DetailSelector from './detailSelector'

import { BackArrow } from '../../components/footerButton'

// import { ProductData } from '../../inputData/productData'

const HeaderModify = styled.div`
  justify-content: flex-start;
`

const FooterSummary = styled.div`
  display: flex;
`
const TotalAmount = styled.div`
  margin-right: 10px;
`

const FooterNoLine = styled.div`
  display: flex;
`

class DetailPage extends Component {
  state = {
    carouselHeight: null,
  }

  isCompleteCart() {
    console.log(`isCompleteCart :DetailPage`)
  }

  handleCheckout = () => {
    console.log('handleCheckout')
    // this.isCompleteCart()
    // this.goCheckOutform()
  }

  copyState = (payload) => {
    // copy state from selector to page
    this.setState(payload)
  }

  componentDidUpdate(prevProp, prevState) {
    if (prevState !== this.state) {
      this.props.setOrder({
        items: this.state.selectedItems,
      })
    }
  }

  setCarouselHeight = (obj) => {
    this.setState({ carouselHeight: obj })
  }

  isZeroAmount() {
    // console.log(`this.props.cart.amount : ${this.props.cart.amount}`)
    // return this.props.cartMultiple.amount === 0 ? true : false
    return this.state.selectedTotalAmount === 0 ? true : false
  }

  isInternalLink = (props) => {
    return 'from' in props.location.state
  }

  // <AskSellerButton
  //   top
  //   link="/ask"
  //   data={this.props.data}
  //   carouselHeight={this.state.carouselHeight}
  // />
  render() {
    // console.log(`cartMultiple :: ${JSON.stringify(this.props.cartMultiple)}`)
    // console.log(`handleBack :: ${this.props.handleBack}`)
    // console.log(`this state: ${JSON.stringify(this.state)}`)
    console.log(`order: ${JSON.stringify(this.props.order)}`)
    console.log(`this state selectedItems: ${JSON.stringify(this.state.selectedItems)}`)
    return (
      <Container>
        <SubContainer>
          <Header>
            <HeaderModify>
              <CarouselSection
                small
                images={this.props.data.images}
                setCarouselHeight={this.setCarouselHeight}
                carouselHeight={this.state.carouselHeight}
              />
              <DetailSelector data={this.props.data} copyState={this.copyState} />
            </HeaderModify>
          </Header>
          {this.state.selectedTotalAmount > 0 && this.props.user.line.userId !== '' ? (
            <Footer height={14}>
              <FooterContainer height={14}>
                <FooterText medium>
                  <FooterSummary>
                    <TotalAmount>{`จำนวน x ${this.state.selectedTotalAmount} `}</TotalAmount>
                    <div> {`${this.state.selectedTotalPrice.toLocaleString('en')} THB`}</div>
                  </FooterSummary>
                </FooterText>
                <ButtonContainer>
                  {this.isInternalLink(this.props) && (
                    <Button footer back onClick={() => this.props.handleBack(this.props.history)}>
                      <BackArrow fill={this.props.theme.primary} />
                    </Button>
                  )}
                  {this.isZeroAmount() ? (
                    <Button inactive>ซื้อสินค้า</Button>
                  ) : (
                    <Button primary onClick={() => this.props.postOrder()}>
                      ซื้อสินค้า
                    </Button>
                  )}
                </ButtonContainer>
              </FooterContainer>
            </Footer>
          ) : this.state.selectedTotalAmount > 0 ? (
            <Footer height={14}>
              <FooterContainer height={14}>
                <FooterText>
                  <FooterNoLine>คุณยังไม่ได้มี line_id</FooterNoLine>
                </FooterText>
                <ButtonContainer>
                  {this.isInternalLink(this.props) && (
                    <Button footer back onClick={() => this.props.handleBack(this.props.history)}>
                      <BackArrow fill={this.props.theme.primary} />
                    </Button>
                  )}
                  <Button inactive>ซื้อสินค้า</Button>
                </ButtonContainer>
              </FooterContainer>
            </Footer>
          ) : (
            <Footer>
              <FooterContainer>
                <ButtonContainer>
                  {this.isInternalLink(this.props) && (
                    <Button footer back onClick={() => this.props.handleBack(this.props.history)}>
                      <BackArrow fill={this.props.theme.primary} />
                    </Button>
                  )}
                  <Button inactive>ซื้อสินค้า</Button>
                </ButtonContainer>
              </FooterContainer>
            </Footer>
          )}
        </SubContainer>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  cartMultiple: state.cartMultiple,
  product: state.product,
  order: state.order,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    postOrder: dispatch.order.postOrder,
    setOrder: dispatch.order.setOrder,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(DetailPage)))
