import React, { Component } from 'react'
import styled, { withTheme, css } from 'styled-components'
import moment from 'moment'
import Frame from '../../components/frameLayout'
import TopBackground from '../../images/topTracking.png'
import CheckMark from '../../images/checkmark.svg'

const TopContent = styled.div`
  position: relative;
  display: flex;
  width: 100vw;
  flex: 1 100%;
  align-self: flex-start;
  z-index: 20;
  ;
  color: ${(props) => props.theme.textPurple};
  img {
    position: absolute;
    object-fit: fill;
    width: 100%;
    height: 30vw;
  }
  div {
    z-index: 30;
    flex: 1;
    postion: absolute;
    margin: 4vh 0 5vh 5vw;
    font-size: 0.9rem;
  }
  span {
    font-size: 1.2rem;
    font-weight: 500;
  }
  @media only screen and (min-width: 451px) {
    width: 100%;
    img {
      position: absolute;
      object-fit: fill;
      width: inherit;
      height: 150px;
    }
    div {
      z-index: 30;
      width:
      postion: absolute;
      margin: 4vh 0 5vh 5vw;
      font-size: 0.9rem;
    }
  
  }

`

const TopUserContent = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1 100%;
  align-self: flex-start;
  z-index: 20;
  color: ${(props) => props.theme.textPurpleLight};
  padding: 0 4vw 4vw;
  div {
    z-index: 30;
    flex: 1;
    postion: absolute;
    font-size: 0.9rem;
    // margin: 0 2vw 0 2vw;
  }
`

const TopUserName = styled.div`
  align-self: center;
  color: ${(props) => props.theme.textPurple};
  font-size: 1.1rem;
`

// const UserDetailContent = styled.div`
//   display: flex;
//   flex-direction: column;
// `
const Top = (props) => {
  return (
    <div>
      <TopContent>
        <img alt="top-background" src={TopBackground} />
        <div>
          <span>ปลายทางได้รับพัสดุเรียบร้อย</span>
          <br />
          Tracking No. : RE20190831sssssTxxxxxx
        </div>
      </TopContent>
      <TopUserContent>
        <TopUserName>{props.user.name}</TopUserName>
        <div>ที่อยู่จัดส่ง :</div>
        <div>{props.user.address}</div>
      </TopUserContent>
    </div>
  )
}

const BodyContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 10px 10px 20px 5vw;
  font-size: 0.7rem;
  white-space: nowrap;
  position: relative;

  @media only screen and (min-width: 451px) {
    width: 90%;
  }
`

const BodyRow = styled.div`
  display: flex;
  z-index: 20;
  margin-bottom: 3vh;
  align-items: center;
  width: 100%;
  @media only screen and (min-width: 451px) {
    margin-bottom: 3vh;
  }
`

const BodyCol = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 10vw;

  @media only screen and (min-width: 451px) {
    margin-right: 50px;
  }
`
const BodyColCircle = styled.div`
  display: flex;
  flex-direction: column;
`

const BodyLeftDate = styled.div`
  align-self: flex-end;
  word-break: keep-all;
  white-space: nowrap;
`
const BodyLeftTime = styled.div`
  align-self: flex-end;
`

const BodyRightName = styled.div`
  align-self: flex-start;
  font-size: 1rem;
`
const BodyRightLocation = styled.div`
  align-self: flex-start;
  font-size: 0.75rem;
  color: #4c426c;
  word-break: break-word;
`
// const VLine = styled.div`
//   position: absolute;
//   border-left: 2px solid #707070;
//   ${(props) => {
//     return css`
//       height: ${props.height}px;
//     `
//   }}
//   margin: 5vw 0 0 30vw;
//   z-index: 0;
// `

const Circle = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 30px;
  align-self: center;
  z-index: 20;
  ${(props) => {
    if (props.active) {
      return css`
        background: ${(prop) => prop.theme.primary};
      `
    } else {
      return css`
        background: ${(prop) => prop.theme.secondary};
      `
    }
  }}
  img {
    object-fit: fill;
    width: 22px;
    height: 22px;
    margin: 4px;
  }
`

moment.updateLocale('th', {
  months: [
    'ม.ค.',
    'ก.พ.',
    'มี.ค.',
    'เม.ย.',
    'พ.ค.',
    'มิ.ย.',
    'ก.ค.',
    'ส.ค.',
    'ก.ย.',
    'ต.ค.',
    'พ.ย.',
    'ธ.ค.',
  ],
})

const MockData = {
  name: 'นางสาวสวย ช้อปปิ้งเก่ง',
  address: '123/456 คอนโดทวีสุข ตึกเอ แขวงบางซื่อ เขตบางซื่อ กรุงเทพฯ 10800',
  history: [
    {
      status: 5,
      status_active: 'active',
      status_name: 'สินค้าถึงผู้รับแล้ว',
      date: '2019-11-07T12:36:10.000Z',
      location: 'บางซื่อ กรุงเทพมหานคร',
    },
    {
      status: 4,
      status_active: 'inactive',
      status_name: 'เตรียมนำจ่าย',
      date: '2019-11-07T12:36:10.000Z',
      location: 'บางซื่อ กรุงเทพมหานคร',
    },
    {
      status: 3,
      status_active: 'inactive',
      status_name: 'รับถุง',
      date: '2019-11-07T12:36:10.000Z',
      location: 'บางซื่อ กรุงเทพมหานคร',
    },
    {
      status: 2,
      status_active: 'inactive',
      status_name: 'ปิดถุง',
      date: '2019-11-07T12:36:10.000Z',
      location: 'Mega BangNa กรุงเทพมหานคร',
    },
    {
      status: 1,
      status_active: 'inactive',
      status_name: 'รับเข้าระบบ',
      date: '2019-11-07T12:36:10.000Z',
      location: 'Mega BangNa กรุงเทพมหานคร',
    },
  ],
}

const LineSVG = styled.svg`
  position: absolute;
  width: 95vw;
  height: 50vh;
  zindex: 10;
  // Add transition to redraw svg component
  -webkit-transition: height 0.015s ease-out; /* For Safari 3.1 to 6.0 */
  transition: height 0.015s ease-out;
  @media only screen and (min-width: 451px) {
    width: 100%;
    height: 100%;
  }
`

class TrackingPage extends Component {
  _isUpdated = false
  // ref = this.getPositionAtCenter(this.ref1.current)
  ref = null

  constructor(props) {
    super(props)
    this.state = {
      // vlineHeight: null,
    }
    this.ref1 = React.createRef()
    this.ref2 = React.createRef()
    this.ref3 = React.createRef()
  }

  componentDidMount() {
    // Calculate height of the Vline by ref1 and ref2 which mark in this.Body
    // const distance = this.getDistanceBetweenElements(this.ref1.current, this.ref2.current)
    // this.setState({ vlineHeight: Math.round(distance) })
    // console.log(Math.round(distance))
    window.addEventListener('resize', this.updateDimensions)
    window.addEventListener('transitionend', this.updateDimensions)
    this.updateDimensions()
  }

  updateDimensions = () => {
    // this.setState({ width: window.innerWidth, height: window.innerHeight });
    this.makeLine(this.ref1.current, this.ref2.current, this.ref3.current)
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
    window.removeEventListener('transitionend', this.updateDimensions)
  }

  getPositionAtCenter = (element) => {
    const { top, left, width, height } = element.getBoundingClientRect()
    return {
      x: left + width / 2,
      y: top + height / 2,
    }
  }

  makeLine = (a, b, c) => {
    const aPosition = this.getPositionAtCenter(a)
    const bPosition = this.getPositionAtCenter(b)
    // console.log(aPosition)
    // console.log(bPosition)
    // console.log(c)

    const cOffset_x = c.getBoundingClientRect().x
    const cOffset_y = c.getBoundingClientRect().y

    // Because width of svg is 100vh so there is no offset in x position
    // But height of svg is 50vh there is reduction offset in y position
    this.setState({
      // x1: Math.round(aPosition.x-cOffset_x),
      // y1: Math.round(aPosition.y-cOffset_y),
      // x2: Math.round(bPosition.x-cOffset_x),
      // y2: Math.round(bPosition.y-cOffset_y),
      x1: Math.round(aPosition.x - cOffset_x),
      y1: Math.round(aPosition.y - cOffset_y),
      x2: Math.round(bPosition.x - cOffset_x),
      y2: Math.round(bPosition.y - cOffset_y),
    })

    // this.x1 = Math.round(aPosition.x - cOffset_x)
    // this.y1 = Math.round(aPosition.y - cOffset_y)
    // this.x2 = Math.round(bPosition.x - cOffset_x)
    // this.y2 = Math.round(bPosition.y - cOffset_y)

    // return <svg ><line  x1={aPosition.x} y1={aPosition.y} x2={bPosition.x} y2={bPosition.y} stroke="black" /></svg>
  }

  // this.Body have to be in class
  // because ref (React.createRef()) is more efficient to track down ref1 / ref2
  // to create the line
  Body = (props) => {
    moment().year()
    const yearTH = moment().year() + 543
    const date = moment(props.data.date)
      .locale('th')
      .year(yearTH)
    const dateToFormat = moment(date).format('วันที่ DD MMMM YY')
    const timeToFormat = moment(date).format('hh:mm น.')

    return (
      <BodyRow>
        <BodyCol>
          <BodyLeftDate>{dateToFormat}</BodyLeftDate>
          <BodyLeftTime>{timeToFormat}</BodyLeftTime>
        </BodyCol>
        <BodyCol>
          {props.data.status_active === 'active' ? (
            <BodyColCircle>
              <Circle active className="intent1" ref={this.ref1}>
                <img alt="checked" src={CheckMark} />
              </Circle>
            </BodyColCircle>
          ) : props.data.status === 1 ? (
            <BodyColCircle className="intent2" ref={this.ref2}>
              <Circle>
                <img alt="checked" src={CheckMark} />
              </Circle>
            </BodyColCircle>
          ) : (
            <BodyColCircle>
              <Circle>
                <img alt="checked" src={CheckMark} />
              </Circle>
            </BodyColCircle>
          )}
        </BodyCol>
        <BodyCol>
          <BodyRightName>{props.data.status_name}</BodyRightName>
          <BodyRightLocation>{props.data.location}</BodyRightLocation>
        </BodyCol>
      </BodyRow>
    )
  }

  render() {
    const { x1, y1, x2, y2 } = this.state
    console.log(this.state)

    return (
      <div>
        <TopContent />
        <Frame
          primaryColor={this.props.theme.tertiary}
          secondaryColor={this.props.theme.white}
          topContent={<Top user={MockData} />}
          topHeight="max-content"
          bodyHeight="50vh"
          justifyContent="flex-start">
          {/* Set vertical line */}
          <LineSVG>
            <line x1={x1} y1={y1} x2={x2} y2={y2} stroke="#707070" strokeWidth="3" />
          </LineSVG>
          <BodyContainer ref={this.ref3}>
            {MockData.history.map((obj, index) => (
              <this.Body key={index} data={obj} />
            ))}
          </BodyContainer>
        </Frame>
      </div>
    )
  }
}

export default withTheme(TrackingPage)
