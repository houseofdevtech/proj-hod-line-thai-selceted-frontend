import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import UploadPage from './page'
// import LoadingScreen from '../../views/loadingPage'
import { isValidSlipQS } from '../../utils/assist'
// import LoadingScreen from '../../views/loadingPage'
import ChangePayment from '../checkPaymentPage/changePayment'
import LoadingScreen from '../../views/loadingPage'

class SlipLanding extends Component {
  _isUpdated = false

  state = {
    isLineUserId: false,
  }

  componentDidMount = async () => {}

  loadSlipInfomation = async () => {
    const qsObject = this.props.location.state
    console.log(qsObject)

    if (isValidSlipQS(qsObject)) {
      console.log('Slip landing :: valid query String')

      // get infomation for render slip page
      this.props.getShopBank(qsObject)
      this.props.getShopDetail(qsObject)
      this.props.getBills(qsObject)
    } else {
      console.log(`Slip landing :: INVALID query String  `)
    }
  }

  componentDidUpdate = async () => {
    if (!this._isUpdated && !this.state.isLineUserId && this.props.user.line.userId !== '') {
      // console.log('Address didUpdated')
      // console.log(`chattyUserId:  ${this.props.user.chattyUserId} !== ''`)
      if (this.props.user.chattyUserId && this.props.user.chattyUserId !== '') {
        // console.log('Address : check chattyUserId')
        // console.log(`chattyUserId:  ${this.props.user.chattyUserId} !== ''`)
        this._isUpdated = true
        this.setState({ isLineUserId: true })

        // check Order for render
        this.props.getOrder(this.props.user)
        this.loadSlipInfomation()
      }
    }
    // isCompleteCheck = () => {
    //   if (this.props.checkPayment.check === 'bank') {
    //     return true
    //   }
    // }
    // if (this.props.payment.isUpdated) {
    //   console.log('payment update :: set state dataloaded = false')
    //   this.setState({ dataLoaded: false })
    //   console.log('payment update :: set redux isUpdated = false')
    //   this.props.setUpdated(false)
    //   const res = await this.props.getAddress(this.props.user)
    //   if (res) {
    //     console.log('payment update :: set dataloaded = true (if have response)')
    //     this.setState({ dataLoaded: res })
    //   }
    // }
  }

  handleMockupFlow = () => {
    this.props.handleMockupFlow2Msg(this.props.qrcode, this.props.receipt)
  }

  isAddress = (props) => {
    return 'address' in props
  }

  isRender = () => {
    console.log(this.props.slip.isRender)
    return this.props.slip.isRender
  }

  isBankPaymentType = () => {
    console.log(`isBankPaymentType (checkPayment.check):: ${this.props.checkPayment.check}`)
    return this.props.checkPayment.check === 'bank'
  }

  isCompleteCheck = () => {
    console.log(this.props.checkPayment.check)

    if (this.props.checkPayment.check !== null) {
      return true
    } else {
      return false
    }
  }

  render() {
    // console.log(`slip State :: ${JSON.stringify(this.props.slip)}`)
    return (
      <div>
        {this.isCompleteCheck() ? (
          this.isBankPaymentType() ? (
            this.isRender() ? (
              <UploadPage {...this.props} />
            ) : (
              <LoadingScreen />
            )
          ) : (
            <ChangePayment />
          )
        ) : (
          <LoadingScreen />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  slip: state.slip,
  checkPayment: state.checkPayment,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    getBills: dispatch.slip.getBills,
    getShopBank: dispatch.slip.getShopBank,
    getShopDetail: dispatch.slip.getShopDetail,
    // checkPayment: state.checkPayment,
    getOrder: dispatch.checkPayment.getOrder,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SlipLanding))
