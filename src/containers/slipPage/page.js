import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
// import Condition from '../checkPaymentPage'
// import CheckPaymentPage from '../checkPaymentPage'

import { CopyToClipboard } from 'react-copy-to-clipboard'
import styled, { withTheme, css } from 'styled-components'
import BeatLoader from 'react-spinners/BeatLoader'
// import SHA1 from 'crypto-js/sha1'
// import HEX from 'crypto-js/enc-hex'

import Frame from '../../components/frameLayout'
import Button from '../../components/button'
// import TopBackground from '../../images/topPayment.png'
import TopBackground from '../../images/passioneBanner.png'
import UploadPicture from '../../images/upload.svg'

const TopContent = styled.div`
  position: relative;
  display: block;
  // width: 100vw;
  width: inherit;
  padding-bottom: 10px;
  flex: 1 100%;
  align-self: flex-start;
  z-index: 20;
  color: ${(props) => props.theme.textPurple};
  padding-bottom: 5vh;
  img {
    position: relative;
    object-fit: cover;
    width: 100%;
    height: 20vw;
  }
  div {
    height: 35vw;
  }

  @media only screen and (min-width: 451px) {
    width: inherit;
    img {
      position: relative;
      object-fit: cover;
      width: 100%;
      height: 100%;
    }
  }
`

const Top = (props) => {
  return (
    <TopContent>
      <img alt="top-background" src={TopBackground} />
    </TopContent>
  )
}

const Section = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 1;
  width: 80vw;
  height: max-content;
  // padding: 0 2vw;

  @media only screen and (min-width: 451px) {
    padding: 0 10px;
    width: 400px;
  }
`

const UploadContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: inherit;
  height: inherit;
  // height: 35vw;
  // label {
  //   width: max-content;
  //   height: max-content;
  // }
  input {
    display: none;
    position: absolute;
  }

  @media only screen and (min-width: 451px) {
    height: max-content;
  }
`

const UploadBox = styled.div`
  ${(props) => {
    if (props.cover) {
      return css`
        width: max-content;
        height: max-content;
        padding: 0;

        img {
          border-radius: 6px;

          padding: 0;
          margin: 0;
          max-width: 65vw;
        }
        @media only screen and (min-width: 451px) {
          width: max-content;
          height: max-content;
          img {
            border-radius: 5px;
            max-width: 350px;
          }
        }
      `
    }
    return css`
      width: 45vw;
      height: max-content;

      padding: 7px 0 0;

      img {
        margin-left: 2vw;
        max-width: 65vw;
      }
      @media only screen and (min-width: 451px) {
        width: 150px;
        img {
          margin-left: 5px;
        }
      }
    `
  }}

  border-radius: 6px;
  border: 2px solid ${(props) => props.theme.primary};
  font-size: 0.85rem;
  font-weight: 100;
  box-shadow: 0 0 10px rgb(0, 0, 0, 0.3);
  display: flex;
  flex-direction: column;
  align-items: center;
`
const SuccessText = styled.div`
  color: ${(props) => props.theme.primary};
  font-weight: 500;
`

const UploadButton = (props) => {
  if (props.file === null) {
    return (
      <UploadContainer>
        <label htmlFor="file-input">
          <UploadBox>
            <img alt="upload" src={UploadPicture} />
            <div>อัพโหลดรูปภาพสลิป</div>
          </UploadBox>
        </label>
        <input
          id="file-input"
          type="file"
          accept="image/png,image/jpg,image/jpeg"
          onChange={props.handleSingle}
        />
      </UploadContainer>
    )
  }
  if (props.file === 'waiting') {
    return (
      <UploadContainer>
        <label htmlFor="file-input">
          <UploadBox>
            <BeatLoader
              //css={override}
              size={15}
              //size={"150px"} this also works
              color={props.theme.primary}
              loading={true}
            />
            <div>อัพโหลดรูปภาพสลิป</div>
          </UploadBox>
        </label>
        <input
          id="file-input"
          type="file"
          accept="image/png,image/jpg,image/jpeg"
          onChange={props.handleSingle}
        />
      </UploadContainer>
    )
  } else {
    // Reduce Image size equal width of detail ***
    // keep aspect ratio
    // Best Practice :: wait api to present picture show
    console.log(props.file)
    return (
      <UploadContainer>
        <SuccessText>อัพโหลดรูปภาพสำเร็จ</SuccessText>
        <label htmlFor="file-input">
          <UploadBox cover={true}>
            <img alt="upload" src={props.file} />
          </UploadBox>
        </label>
        <input
          id="file-input"
          type="file"
          accept="image/png,image/jpg,image/jpeg"
          onChange={props.handleSingle}
        />
      </UploadContainer>
    )
  }
}

const DetailContainer = styled.div`
  position: relative;
  margin: 20px 0;

  font-size: 0.9rem;
  display: flex;
  flex-direction: column;
  color: ${(props) => props.theme.textGrey};
  @media only screen and (min-width: 451px) {
  }
`
const DetailRow = styled.div`
  display: flex;
  margin-bottom: 10px;
  @media only screen and (min-width: 451px) {
    width: 300px;
  }
`
const DetailName = styled.div`
  width: 25vw;
  @media only screen and (min-width: 451px) {
    padding-left: 20px;
    width: 150px;
  }
`
const DetailValue = styled.div`
  margin-left: 30px;
  @media only screen and (min-width: 451px) {
    margin-left: 0px;
  }
`
const DetailButton = styled.div`
  width: inherit;
  height: 12vw;
  @media only screen and (min-width: 451px) {
    height: 50px;
  }
`

const Detail = (props) => {
  console.log(`Detail props:: ${props.renderData}`)
  return (
    <DetailContainer>
      {props.renderData !== null && (
        <div>
          <DetailRow>
            <DetailName>ชำระผ่าน</DetailName>
            <DetailValue>{props.renderData.bankName}</DetailValue>
          </DetailRow>
          <DetailRow>
            <DetailName>ชำระให้กับร้าน</DetailName>
            <DetailValue>
              {props.renderData.shopName === 'Passione Line Shoping'
                ? 'บ. มาย แพชชั่น แอนด์ อินสไปเรชั่น จำกัด'
                : props.renderData.shopName}
            </DetailValue>
          </DetailRow>
          <DetailRow>
            <DetailName>เลขบัญชี</DetailName>
            <DetailValue>{props.renderData.bankNumber}</DetailValue>
          </DetailRow>
          <DetailRow>
            <DetailName>รวมทั้งสิ้น</DetailName>
            <DetailValue>{props.renderData.total.toLocaleString('th')} THB</DetailValue>
          </DetailRow>
        </div>
      )}
      <DetailButton>
        <CopyToClipboard
          //text={this.state.bankAccId}
          text={props.text}
          //onCopy={() => this.setState({ copied: true })}>
          onCopy={props.handleCopy}>
          <Button primary>คัดลอกเลขบัญชี</Button>
        </CopyToClipboard>
      </DetailButton>
    </DetailContainer>
  )
}

const isCompletedOutputData = (outputData) => {
  return (
    outputData !== null &&
    'bank_id' in outputData &&
    'bill_id' in outputData &&
    'shop_id' in outputData &&
    'user_id' in outputData
  )
}

// const upload = document.getElementById('file-input')

// upload.addEventListener(
//   'change',
//   (evt) => {
//     const files = [...evt.target.files]
//     compress
//       .compress(files, {
//         size: 0.5, // the max size in MB, defaults to 2MB
//         quality: 0.75, // the quality of the image, max is 1,
//         maxWidth: 1920, // the max width of the output image, defaults to 1920px
//         maxHeight: 1920, // the max height of the output image, defaults to 1920px
//         resize: true, // defaults to true, set false if you do not want to resize the image width and height
//       })
//       .then((data) => {
//         // returns an array of compressed images
//         console.log(data)
//       })
//   },
//   false
// )

class UploadPage extends Component {
  _isCheckUpdate = false
  _isUpdated = false
  _isMounted = false
  _isUserIdLoaded = false
  _isPosted = false
  state = {
    // for api
    inputs: { file: '' },
    // for testing to show picture
    file: null,
    // for Copy Paste function
    bankNumberForCopy: null,
    copied: false,
    // for Render / Output  Data
    renderData: null,
    outputData: null,
  }

  componentDidMount() {
    console.log(`Slip Didmount :: ${JSON.stringify(this.props.location.state)}`)
    const qsObject = this.props.location.state
    this.setState({
      ...this.state,
      outputData: {
        bank_id: qsObject.bank_id,
        bill_id: qsObject.bill_id,
        shop_id: qsObject.shop_id,
        path_gcs_name: 'slips',
      },
    })
  }

  componentDidUpdate() {
    if (this.props.checkPayment.check && !this._isCheckUpdate) {
      this._isCheckUpdate = true
    }
    // Check is All information is prompt to show and is not Mounted
    // set Bank Account Number for Copy & Paste function
    if (this.props.slip.isRender && !this._isMounted) {
      this._isMounted = true
      // this.props.setSlip({isMounted: true})

      this.setState({
        ...this.state,
        renderData: this.props.slip.render,
        bankNumberForCopy: this.props.slip.render.bankNumber,
      })
    }
    // Check when chattyUserId is update
    // so it will included in outputData for API33 - create_slip
    if (!this._isUserIdLoaded) {
      if (this.props.user.chattyUser && this.props.user.chattyUserId !== '') {
        this._isUserIdLoaded = true
        this.setState({
          ...this.state,
          outputData: {
            ...this.state.outputData,
            user_id: this.props.user.chattyUserId,
          },
        })
      } else {
        console.log('Slip :: ChattyUserId cannot be loaded')
      }
    }
    // Check img_path is received from API34 - get_slip_bill
    if (this._isPosted && this.props.slip.img_path !== null) {
      this._isPosted = false
      this.setState({ ...this.state, file: this.props.slip.img_path })
    }
  }

  handleCopy = () => {
    this.setState({ copied: true })
  }

  handleChange = (field, value) => {
    this._isUpdated = true
    this.setState({ [field]: value })
  }

  // changeFileName = (files) => {
  //     // ******** Encode file name
  //     // ******** By Adding HEX + SHA1 of current Date
  //     // console.log(event.target.files[0].name)
  //     console.log(files.name)
  //     // let s = event.target.files[0].name
  //     // var filename = s.replace(/[^a-z0-9]/gi, '_').toLowerCase();
  //     // console.log(filename)
  //     let newDate = new Date()
  //     const encodeDate = HEX.stringify(SHA1(newDate.toJSON()))
  //     let file = files
  //     let blob = file.slice(0,file.size,file.type)
  //     const newFileName = encodeDate + '_' + file.name
  //     const NewFile = new File([blob],newFileName,{type: file.type})
  //     console.log(NewFile)
  //     return NewFile
  // }

  handleSingle = (event) => {
    event.persist()
    if (isCompletedOutputData(this.state.outputData)) {
      const file = event.target.files[0]
      // let file_size = event.target.files[0].size
      // console.log(file_size)
      // if (file_size > 900000) {
      //   this.fileChangedHandler(file)
      //   console.log(file_size)
      //   let newfile_size = newfile.size
      //   console.log(newfile_size)
      // }

      // const NewFile = this.changeFileName(event.target.files[0])

      const formData = new FormData()
      formData.append('main_img_path', file)
      formData.append('body_data', JSON.stringify(this.state.outputData))

      // ***** Check formData
      // for (var value of formData.values()) {
      //   console.log(value)
      // }
      //
      const output = {
        shop_id: this.state.outputData.shop_id,
        formData: formData,
      }

      // get File Object
      let fileObject = file
      // reCreate new Object and set File Data into it
      let newFileObject = {
        lastModified: fileObject.lastModified,
        lastModifiedDate: fileObject.lastModifiedDate,
        name: fileObject.name,
        size: fileObject.size,
        type: fileObject.type,
      }
      // then use JSON.stringify on new object
      // JSON.stringify(newFileObject)

      // print out all field in formData which readable in console.log
      console.log(`main_img_path (slip_img_path) :: ${JSON.stringify(newFileObject)}`)
      console.log(`body_data :: ${JSON.stringify(this.state.outputData)}`)
      console.log(JSON.stringify(output))
      this.props.postSlip(output)
      this.props.setSlip({ img_path: null })
      this._isPosted = true

      this.setState({
        inputs: {
          ...this.state.inputs,
          file: event.target.files[0],
        },
        // file: URL.createObjectURL(event.target.files[0]),
        file: 'waiting',
      })
    } else {
      console.log('body_data is not meet api requirement')
    }
  }

  render() {
    // console.log(`payment (payment):: ${JSON.stringify(this.props.payment)}`)
    // console.log(`payment (bills):: ${JSON.stringify(this.props.bills)}`)
    console.log(`upload (state):: ${JSON.stringify(this.state)}`)
    console.log(`upload (slip):: ${JSON.stringify(this.props.slip)}`)
    console.log(`CHECK PAYMENT :: ${JSON.stringify(this.props.checkPayment)}`)
    
      return (
        <div>
          <Frame
            primaryColor={this.props.theme.tertiary}
            secondaryColor={this.props.theme.white}
            topContent={<Top history={this.props} />}
            paddingBottom={false}
            topHeight="max-content"
            bodyHeight="max-content"
            marginRight="0vw">
            <Section>
              <UploadButton
                file={this.state.file}
                handleSingle={this.handleSingle}
                theme={this.props.theme}
              />
              <Detail
                text={this.state.bankNumberForCopy}
                renderData={this.props.slip.render}
                handleCopy={this.handleCopy}
              />
            </Section>
          </Frame>
        </div>
      )
   
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  slip: state.slip,
  checkPayment: state.checkPayment,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    postSlip: dispatch.slip.postSlip,
    setSlip: dispatch.slip.setSlip,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(UploadPage)))
