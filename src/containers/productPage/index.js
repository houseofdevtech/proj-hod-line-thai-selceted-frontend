import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import Axios from 'axios'

import ManageState from './manage'
import { isValidProductQS } from '../../utils/assist'
import ScrollingWrapper from '../../components/ScollWrapper'

// import { ProductData } from '../../inputData/productData'

class LandingProductPage extends Component {
  state = {
    data: [],
    name: '',
  }

  componentDidMount = async () => {
    await this.getAllStoreProvince()
    const qsObject = this.props.location.state
    if (isValidProductQS(qsObject)) {
      // if all queryString have been validated, then send to api method to call

      // const data = await this.props.getProduct(qsObject)
      await this.props.getProduct(qsObject)
      // this.setState({qsObject : qsObject})
    } else {
      console.log(`incomplete queryString Do Something `)
    }
  }

  onScroll = () => {
    if (this.scrollingWrapper.scrollTop > 100 && !this.state.hasScrolled) {
      this.setState({ hasScrolled: true })
    } else if (this.scrollingWrapper.scrollTop < 100 && this.state.hasScrolled) {
      this.setState({ hasScrolled: false })
    }
  }

  reference = (id) => (ref) => {
    this[id] = ref
  }

  componentWillUnmount() {
    // console.log('product UnMount')
    // this.props.location.state.from === '/list' && this.props.setProductData({})
    // this.props.location.state={}
    // this.props.setProductData({})
  }

  getAllStoreProvince = async () => {
    const id = window.location.pathname.split('/')[4]
    const name = window.location.pathname.split('/')[3]
    let thai = decodeURIComponent(name)
    const res = await Axios.get(
      `https://dev.houseofdev.tech/thai-selected/admin/pages/api/selectByProvince.php?pid=${id}`
    )
      .then((res) => res.data)
      .catch((e) => console.log(e))
    this.setState({ data: res })
    this.setState({ name: thai })
  }

  isRecievedData = (props) => {
    return 'product' in props.product
  }

  render() {
    return (
      <>
        <div>
          <ManageState name={this.state.name} data={this.state.data} {...this.props} />
        </div>
      </>
    )
  }
}

const mapStateToProps = (state) => ({
  product: state.product,
  user: state.user,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    setProductData: dispatch.product.setProductData,
    getProduct: dispatch.product.getProduct,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LandingProductPage))
