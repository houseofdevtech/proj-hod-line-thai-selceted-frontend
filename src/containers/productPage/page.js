import React, { Component } from 'react'
import { connect } from 'react-redux'
// import _ from 'lodash'
// import queryString from 'query-string'
import { withTheme } from 'styled-components'
import { withRouter } from 'react-router-dom'

// import Pic1 from '../../images/main-1.jpg'

import CarouselSection from '../../components/carousel'

import Selector from './selector'
import Button from '../../components/button'
import LiffStatus from '../../components/liffStatus'
import { BackArrow } from '../../components/footerButton'

import {
  Container,
  SubContainer,
  Header,
  Footer,
  FooterText,
  FooterContainer,
  ButtonContainer,
} from '../../style/productStyle'

class ProductPage extends Component {
  state = {
    warning: false,
    selected: false,
    carouselHeight: null,
  }

  // ref = React.createRef()

  componentDidMount() {
    // console.log(this.ref)
    // console.log(`product redux :: ${JSON.stringify(this.props.productData)}`)
  }

  // componentDidUpdate(prevProps) {
  //   console.log(`selectedAmount :: ${this.props.data.selectedAmount}`)
  //   if (this.isCompleteSelected(this.props)) {
  //     const resultProduct = this.findProduct(this.props)
  //     console.log(resultProduct)
  //     if (!_.isEmpty(resultProduct)) {
  //       this.props.setOrder({
  //         items: [
  //           {
  //             id: resultProduct.sku_id,
  //             qty: this.props.data.selectedAmount,
  //           },
  //         ],
  //       })
  //     }
  //   }
  // }

  // isCompleteSelected = (props) => {
  //   return (
  //     props.data.selectedSize !== null &&
  //     props.data.selectedColor !== null &&
  //     props.data.selectedAmount !== 0
  //   )
  // }

  // findProduct = (props) => {
  //   const product = props.product.product
  //   const product_items = product[0].product_data[0].product_items
  //   const selectedColor = props.data.selectedColor
  //   const selectedSize = props.data.selectedSize
  //   const findProduct = this.findSelectedProduct(product_items, selectedColor, selectedSize)
  //   console.log(`findproduct ::${JSON.stringify(findProduct)}`)
  //   return findProduct
  // }

  // isEqualColor = (c1, c2) => {
  //   return _.isEqual(c1.color_value, c2.color_value) && _.isEqual(c1.color_name, c2.color_name)
  // }

  // findSelectedProduct = (product_items, color, size) => {
  //   const result = product_items.filter(
  //     (obj) => this.isEqualColor(obj.color, color) && obj.size === size
  //   )
  //   // console.log(result[0].sku_id)
  //   return result[0]
  // }

  isZeroAmount() {
    // console.log(`this.props.cart.amount : ${this.props.cart.amount}`)
    return this.props.data.selectedAmount === 0 ? true : false
  }

  isCompleteCart() {
    let warning = false
    let selected = false
    warning = this.props.cart.color === '' || this.props.cart.size === '' ? true : false
    selected = this.props.cart.color !== '' || this.props.cart.size !== '' ? true : false
    // return this.setState({ ...this.state, warning: true })
    return this.setState(
      { ...this.state, warning: warning, selected: selected },
      // This method will perform after setState
      () => this.goCheckOutform()
    )
  }

  setCarouselHeight = (obj) => {
    this.setState({ carouselHeight: obj })
  }

  goCheckOutform = () => {
    // Check condition in the state
    // 1. there is no warning (size / color is selected)
    // 2. user have been selected some item at once
    // If the condition have been met, it will transfer to the next page (check out form)
    // !this.state.warning && this.state.selected && this.props.history.push('/form')
    // !this.state.warning && this.state.selected && this.props.handleSubmitOrder()
    !this.state.warning && this.state.selected && this.props.handleRequestQRcode()
  }

  handleCheckout = () => {
    // console.log('handleCheckout')
    this.isCompleteCart()
    // this.goCheckOutform()
  }

  isInternalLink = (props) => {
    return 'from' in props.location.state
  }

  // <AskSellerButton
  //               link="/ask"
  //               data={this.props.data}
  //               carouselHeight={this.state.carouselHeight}
  //             />
  //  <Button footer primary onClick={() => this.props.handleRequestQRcode(this.props.cart)}></Button>
  render() {
    // console.log(this.state)
    // console.log(this.props.product)
    // console.log(this.props.cart)
    // console.log(`product redux :: ${JSON.stringify(this.props.productData)}`)
    // console.log(this.props.product.images)
    // console.log(`history: ${JSON.stringify(this.props.history)}`)
    // console.log(`location :: ${JSON.stringify(this.props.location.state)}`)
    // {!small ? <AskSellerButton link="/ask"  /> : <AskSellerButton top link="/ask"  />}
    console.log(`order : ${JSON.stringify(this.props.order)}`)
    // console.log(`data : ${JSON.stringify(this.props.data)}`)
    // console.log(`carousel Height : ${JSON.stringify(this.state.carouselHeight)}`)
    return (
      <Container>
        <SubContainer>
          <Header>
            <CarouselSection
              images={this.props.data.images}
              setCarouselHeight={this.setCarouselHeight}
              carouselHeight={this.state.carouselHeight}
              data={this.props.data}
            />
            <Selector
              warning={this.state.warning}
              data={this.props.data}
              handleSelect={this.props.handleSelect}
              isZeroAmount={this.isZeroAmount}
              selectedSize={this.props.selectedSize}
              selectedColor={this.props.selectedColor}
            />
            <LiffStatus show="off" data={this.props.user} />
          </Header>

          {/* {this.props.data.selectedAmount > 0 ? ( */}
          {this.props.isCompleteSelected(this.props.data) && this.props.isLineUserId(this.props) ? (
            <Footer height={14}>
              <FooterContainer height={14}>
                <FooterText medium>
                  <div style={{ display: 'flex' }}>
                    <div
                      style={{
                        marginRight: '10px',
                      }}>{`จำนวน x ${this.props.data.selectedAmount} `}</div>
                    <div>
                      {' '}
                      {`${(
                        this.props.data.selectedAmount * this.props.data.selectedPrice
                      ).toLocaleString('en')} THB`}
                    </div>
                  </div>
                </FooterText>
                <ButtonContainer>
                  {this.isInternalLink(this.props) && (
                    <Button footer back onClick={() => this.props.handleBack(this.props.history)}>
                      <BackArrow fill={this.props.theme.primary} />
                    </Button>
                  )}
                  {this.isZeroAmount() ? (
                    <Button inactive>ซื้อสินค้า</Button>
                  ) : (
                    <Button
                      primary
                      onClick={async () => {
                        try {
                          let order = await this.props.getCheckOrder(this.props.user)
                          console.log(`Have Order : ${order}`)
                          if (order) {
                            this.props.editOrderByUserId(this.props.user)
                          } else {
                            this.props.postOrder()
                          }
                        } catch(err) {
                          console.log(err)
                          this.props.postOrder()
                        }

                      }}>
                      ซื้อสินค้า
                    </Button>
                  )}
                </ButtonContainer>
              </FooterContainer>
            </Footer>
          ) : this.props.isCompleteSelected(this.props.data) ? (
            <Footer height={14}>
              <FooterContainer height={14}>
                <FooterText>
                  <div style={{ display: 'flex' }}>
                    <div> คุณยังไม่ได้มี line_id </div>
                  </div>
                </FooterText>
                <ButtonContainer>
                  {this.isInternalLink(this.props) && (
                    <Button footer back onClick={() => this.props.handleBack(this.props.history)}>
                      <BackArrow fill={this.props.theme.primary} />
                    </Button>
                  )}
                  <Button inactive>ซื้อสินค้า</Button>
                </ButtonContainer>
              </FooterContainer>
            </Footer>
          ) : (
            <Footer>
              <FooterContainer>
                <ButtonContainer>
                  {this.isInternalLink(this.props) && (
                    <Button footer back onClick={() => this.props.handleBack(this.props.history)}>
                      <BackArrow fill={this.props.theme.primary} />
                    </Button>
                  )}
                  <Button inactive>ซื้อสินค้า</Button>
                </ButtonContainer>
              </FooterContainer>
            </Footer>
          )}
        </SubContainer>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  product: state.product,
  order: state.order,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    setProductData: dispatch.product.setProductData,
    postOrder: dispatch.order.postOrder,
    getCheckOrder: dispatch.order.getCheckOrder,
    editOrderByUserId: dispatch.order.editOrderByUserId,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(ProductPage)))
