import React, { Component } from 'react'
import { connect } from 'react-redux'
// import styled, { css } from 'styled-components'
import styled, { css, withTheme } from 'styled-components'

import LinesEllipsis from 'react-lines-ellipsis'

import ColorTemplate from '../../components/templateColor'
import SizeTemplate from '../../components/templateSize'
import NumberInput from '../../components/numberInputToggle'
import Frame from '../../components/frameLayout'

import {
  TitleArea,
  Title,
  Text,
  Price,
  DescriptionArea,
  MoreBar,
  More,
  Warning,
  SelectedSection,
  SelectedBar,
  Divider,
} from '../../style/contentStyle'

const Container = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
      color: ${props.theme.textPurple};
    `
  }}
  display: flex;
  flex-direction: column;
  flex: 1;

  height: max-content;
  // width: 100vw;
  align-content: space-between;
  justify-content: space-between;

  @media only screen and (min-width: 451px) {
    width: 500px;
  }
`
// const setWarning = (warning, cart) => {
//   let text = []
//   cart.color === '' && text.push('color')
//   cart.size === '' && text.push('size')
//   // console.log(text.length)
//   if (warning) {
//     return (
//       <div>
//         {text.length > 0 ? (
//           <Warning>
//             Please select {text[0] && text[0]}
//             {text.length === 2 ? ' and ' : ''}
//             {text[1] && text[1]}
//             {'.'}
//           </Warning>
//         ) : null}
//       </div>
//     )
//   }
// }
// {setWarning(warning, this.props.cart)}

// Global variable is set isClamped to avoid unlimited loop
// var isClampedGlobal = false;

const Top = (props) => {
  const { product_name, product_minPrice, product_detail } = props.data
  const { isMore, isClampedAtStart } = props.state

  return (
    <div style={{ padding: '0 8vw' }}>
      <TitleArea>
        <Title>
          <span>{product_name}</span>
        </Title>
        <Price>
          <span>{`${product_minPrice.toLocaleString()} THB`}</span>
        </Price>
      </TitleArea>
      <DescriptionArea>
        <LinesEllipsis
          text={product_detail}
          onReflow={props.handleReflow}
          maxLine={!isMore ? 4 : 10}
          trimRight={true}
        />
        <MoreBar>
          {isClampedAtStart && (
            <More onClick={() => props.handleChange('isMore')}>
              {!isMore ? 'ดูเพิ่มเติม' : 'แสดงน้อยลง'}
            </More>
          )}
        </MoreBar>
      </DescriptionArea>
    </div>
  )
}

const Body = (props) => {
  const { colors, sizes, selectedMaxAmount, selectedAmount, isZeroAmount } = props.data

  return (
    <>
      {isZeroAmount ? (
        <Warning>Sorry This product is out of Stock</Warning>
      ) : (
        <SelectedSection>
          {colors && (
            <SelectedBar>
              <Text>สี</Text>
              <ColorTemplate
                colors={colors}
                handleSelect={props.handleSelect}
                selectedColor={props.selectedColor}
              />
            </SelectedBar>
          )}
          {colors && <Divider />}
          {sizes && (
            <SelectedBar>
              <Text>ขนาด</Text>
              <SizeTemplate
                sizes={sizes}
                handleSelect={props.handleSelect}
                selectedSize={props.selectedSize}
              />
            </SelectedBar>
          )}
          {sizes && <Divider />}
          <SelectedBar>
            <Text>จำนวน</Text>
            <NumberInput
              maxAmount={selectedMaxAmount}
              selectedAmount={selectedAmount}
              handleSelect={props.handleSelect}
            />
          </SelectedBar>
        </SelectedSection>
      )}
    </>
  )
}

class ContentSection extends Component {
  state = {
    isClampedAtStart: false, // Logic to decide first clamp
    isClamped: false, // react-lines-ellipsis :: api use for detect first clamp only
    isMore: false, // Logic to expand/less description
  }

  handleChange = (key) => {
    this.setState((prevState) => ({ [key]: !prevState[key] }))
  }

  handleReflow = (rleState) => {
    const { clamped } = rleState
    const { isClamped, isClampedAtStart } = this.state
    // console.log(` text : ${text} `)
    // console.log(` clamped : ${clamped} `)
    isClamped !== clamped && !isClampedAtStart && this.handleChange('isClampedAtStart')
  }

  componentDidMount() {}

  render() {
    // const warning = this.props.warning
    return (
      <Container>
        <Frame
          style={{ position: 'relative' }}
          primaryColor={this.props.theme.tertiary}
          secondaryColor={this.props.theme.white}
          paddingBottom={false}
          overflowHidden={true}
          frameHeight="max-content"
          topHeight="max-content"
          bodyHeight="max-content"
          topContent={
            <Top
              data={this.props.data}
              state={this.state}
              handleReflow={this.handleReflow}
              handleChange={this.handleChange}
            />
          }>
          <Body
            data={this.props.data}
            handleChange={this.handleChange}
            handleSelect={this.props.handleSelect}
            selectedSize={this.props.selectedSize}
            selectedColor={this.props.selectedColor}
          />
        </Frame>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(ContentSection))
