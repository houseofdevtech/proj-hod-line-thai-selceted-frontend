import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import arrBack from '../../images/back_arrow.svg'
import _ from 'lodash'
import {
  BottomColumnContainer,
  BottomLeftBackground,
  BottomLeftFiller,
  BottomLeftSquare,
  BottomRowContainer,
  ButtonContainer,
  Container,
  ContentContainer,
  ContentVerticalContainer,
  SelectedContainer,
  SubContainer,
  ThemesContainer,
  TopBackground,
  TopContainer,
  TopContent,
} from '../../style/generalStyle'
import { menus } from '../../inputData/plContentsPassione'
import Button from '../../components/button'
import ContentSection from '../../components/contentSection'
import NoContentSection from '../../components/noContentSection'
import ContentVerticalSection from '../../components/contentVerticalSection'
import styled from 'styled-components'
import { Title } from '../../style/contentStyle'
export const ContentContainer2 = styled.div`
  position: absolute;
  padding: 0px 0 10px 0;
display: flex;
flex-flow: row wrap;
  top: 65px;
  width: 100%;
  height: 77vh;
  justify-content: center;
  overflow-x: hidden;
  overflow-y: scroll;
  border-radius: 30px 0 20px 0;

  @media only screen and (min-width: 451px) {
    width: inherit;
    // height: inherit;
    overflow-x: scroll;
    &::-webkit-scrollbar {
      display: none;
      width: 0px !important;
      background: transparent;
    }
  }
`;

const Flex = styled.div`
display: flex;
justify-content: space-around;
align-items: center;
`

const H3 = styled.h3`
display: flex;
color: white;

`
const DIV = styled.div`
display: flex;
justify-content: center;
margin: 3px;
`
class ManageState extends Component {
  state = {
    // ... from component did mount
    selectedSize: null,
    selectedColor: null,
    selectedAmount: 0,
    selectedPrice: 0,
    selectedMaxAmount: 1,
    preload: false,
    selected: false,
    type: 'all',
  }

  async componentDidMount() {
    // this.forceUpdate()
    const product = await this.props.product.product
    // console.log(product)
    // await this.props.getProduct(this.props.qsObject)
    // const product = this.props.data
    // make sure that product is get only one shop and one product

    // If product is get into array state
    // if (product && product.length === 1 && this.state.preload === false) {
    if (product && this.state.preload === false) {
      console.log('key:"product" in state and only has 1 product_id')
      // console.log(`product :: ${product[0].product_data[0].product_items}`)
      // Extract Items inside the shop
      // const items = product[0].product_data[0].product_items
      const items = product.product_data[0].product_items
      const images = this.setImage(items)
      const minPrice = this.setMinPrice(items)
      const colorTemplate = this.setColor(items)
      const product_name = product.product_data[0].product_name
      const product_id = product.product_data[0].product_id
      const product_detail = product.product_data[0].detail
      const sizes = this.setSize(items)
      const colorXsize = this.setSizeXColor(items)

      // Start Order

      // this.props.setOrder({ product_id: product_id })
      // console.log(`shop_id :: ${this.props.location.state.shop_id}`)
      this.props.location.state.shop_id &&
      this.props.setOrder({ shop_id: this.props.location.state.shop_id })

      this.setState({
        images: images,
        colors: colorTemplate,
        product_minPrice: minPrice,
        product_name: product_name,
        product_id: product_id,
        product_detail: product_detail,
        sizes: sizes,
        colorXsize: colorXsize,
        preload: true,
      })
    } else {
      console.log('more than one product')
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selectedColor !== this.state.selectedColor &&
      this.state.selectedColor !== null &&
      // selected interlocking prevent loop
      this.state.selected
    ) {
      console.log(`DidUpdate`)
      // console.log(`prevState ::${JSON.stringify(prevState.selectedColor)}`)
      // console.log(`thisState ::${JSON.stringify(this.state.selectedColor)}`)

      // console.log(this.findSizeXColor(this.state.selectedColor))
      const sizes = this.findSizeXColor(this.state.selectedColor)

      // reset Order
      this.props.setOrder({
        items: null,
        type: '',
      })
      // reset Size selected
      this.setState({
        sizes: sizes,
        selectedSize: null,
        selected: false,
      })
    }

    // Find Max Amount and Price when [Color] and [Price] is selected
    if (
      prevState.selectedColor === this.state.selectedColor &&
      this.state.selectedColor !== null &&
      this.state.selectedSize !== null &&
      this.state.selected
    ) {
      const resultProduct = this.findProduct()

      if (resultProduct) {
        console.log(`selectProduct Existed :: ${JSON.stringify(resultProduct)}`)

        this.setState({
          selectedPrice: resultProduct.price,
          // if current Amount more than max amount
          // [then] set to max amount (selectedProduct.qty)
          // [else] set to current Amount (selectedAmount)
          selectedAmount:
            this.state.selectedAmount > resultProduct.qty
              ? resultProduct.qty
              : this.state.selectedAmount,
          selectedMaxAmount: resultProduct.qty,
          // interlocking logic for prevent loop
          selected: false,
        })
        // console.log(sku_id)
      } else {
        console.log(`selectProduct not Existed ::${JSON.stringify(resultProduct)}`)
      }
    }

    if (this.isCompleteSelected(this.state) && this.state.selected) {
      const resultProduct = this.findProduct()
      if (!_.isEmpty(resultProduct)) {
        console.log(`chattyUserId ::${this.props.user.chattyUserId}`)
        this.props.setOrderUserId(this.props.user.chattyUserId)
        this.props.setOrder({
          items: [
            {
              id: resultProduct.sku_id,
              qty:
                this.state.selectedAmount > resultProduct.qty
                  ? resultProduct.qty
                  : this.state.selectedAmount,
            },
          ],
        })
      }

      // interlocking logic for prevent loop
      this.setState({ selected: false })
    }
  }

  findProduct = (props, state) => {
    const { selectedColor, selectedSize } = this.state
    const product = this.props.product.product
    const items = product.product_data[0].product_items
    console.log(`${JSON.stringify(selectedColor)} || ${selectedSize}`)
    return this.findSelectedProduct(items, this.state.selectedColor, this.state.selectedSize)
  }

  isCompleteSelected = (state) => {
    return state.selectedSize !== null && state.selectedColor !== null && state.selectedAmount !== 0
  }

  isLineUserId = (props) => {
    // If have line userId from liff init this will not equal empty string
    return props.user.line.userId !== ''
  }

  setImage = (product_items) => {
    let images = []
    // push 1st image from "main_img_path"
    images.push(product_items[0].main_img_path)
    // push other images from "sub_img_path"
    product_items[0].sub_img_path.map((obj) => images.push(obj.img_path))

    // push other images from "color_img_path"
    // let colorImagePath = []
    //  ........ get all color_img_path to colorImagePath(Array)
    // product_items.map((obj) => {
    //   if (obj.color.color_img_path !== null) {
    //     colorImagePath.push(obj.color.color_img_path)
    //   }
    //   return null
    // })
    // // ......... filter duplicate colorImagePath(Array)
    // const uniqueColorImagePath = _.uniq(colorImagePath)
    // uniqueColorImagePath.map((path) => images.push(path))

    // filter all unique image path
    const result = _.uniq(images)
    // return array of images url
    return result
  }

  setMinPrice = (product_items) => {
    let prices = []
    product_items.map((obj) => prices.push(obj.price))
    // console.log(prices)
    // console.log (`Math :: ${_.min( prices )}`)
    // return min Price from array
    return _.min(prices)
  }

  setColor = (product_items) => {
    const colorTemplate = []
    product_items.map((obj) => {
      // delete 'color_img_path'
      // check equal object will omit 'color_img_path' in setSizeXColor
      let newObj = _.cloneDeep(obj)
      delete newObj.color['color_img_path']
      // console.log(newObj)
      colorTemplate.push(newObj.color)
      return null
    })
    // filter duplicate field
    const keys = ['color_name', 'color_value']
    const filtered = colorTemplate.filter(
      ((s) => (o) => ((k) => !s.has(k) && s.add(k))(keys.map((k) => o[k]).join('|')))(new Set()),
    )

    return filtered
  }

  setSize = (product_items) => {
    const sizeTemplate = []
    product_items.map((obj) => sizeTemplate.push(obj.size))
    // filter duplicate field
    const result = _.uniq(sizeTemplate)

    return result
  }

  setSizeXColor = (product_items) => {
    const colors = this.setColor(product_items)
    const result = []
    colors.map((c) => {
      let resultObj = {}
      let sizes = []
      product_items.map((obj) => {
        // delete 'color_img_path'
        // check equal object will omit 'color_img_path' in this method
        let newObj = _.cloneDeep(obj)
        delete newObj.color['color_img_path']
        // console.log(`${JSON.stringify(c)} x ${JSON.stringify(newObj.color)}`)
        if (_.isEqual(c, newObj.color)) {
          sizes.push(newObj.size)
        }
        return null
      })
      resultObj.sizes = sizes
      resultObj.color = c
      result.push(resultObj)
      return null
    })
    return result
  }

  isEqualColor = (c1, c2) => {
    return _.isEqual(c1.color_value, c2.color_value) && _.isEqual(c1.color_name, c2.color_name)
  }

  findSizeXColor = (color) => {
    // console.log(color)
    let result = null
    this.state.colorXsize.map((obj) => {
      // color && console.log(` ${obj.color.color_value} :: ${color.color}`)
      if (color && this.isEqualColor(obj.color, color)) {
        // console.log(obj.sizes)
        result = obj.sizes
      }
      return null
    })
    return result
  }

  findSelectedProduct = (product_items, color, size) => {
    const result = product_items.filter(
      (obj) => this.isEqualColor(obj.color, color) && obj.size === size,
    )
    // console.log(result[0].sku_id)
    return result[0]
  }

  handleSelect = async (type, i) => {
    this.setState({ type: type })
    console.log('tyype', this.state.type)
  }

  render() {
    console.log('oox', this.props.data)
    console.log(`manage State :: ${JSON.stringify(this.state)}`)
    // console.log(`product State :: ${JSON.stringify(this.props.product)}`)
    return (
      <div>
        <Container>
          <SubContainer>
            <TopBackground>
              <TopContainer>
                <Flex>
                <H3 className="d-flex justify-center">{this.props.name}</H3>
                  <H3> </H3>
                </Flex>
              </TopContainer>
            </TopBackground>
            <BottomColumnContainer>
              <BottomRowContainer>
                <BottomLeftFiller>
                  <BottomLeftBackground>
                    <BottomLeftSquare/>`
                  </BottomLeftBackground>
                </BottomLeftFiller>
                <ThemesContainer>
                  <SelectedContainer/>
                </ThemesContainer>
                {this.props.data?.length > 0 ? (
                  <ContentContainer2>
                    {this.props.data?.map((obj, index) => {
                      console.log('kkl')
                      return ( <DIV><ContentVerticalSection key={index} data={obj}/></DIV>)
                    })}
                  </ContentContainer2>

                ) : (
                  <ContentContainer>
                    <NoContentSection/>
                  </ContentContainer>
                )}
              </BottomRowContainer>
              <div className="footer">
                <button className="footer-button footer_contents" onClick={() =>  this.props.history.goBack()}><img className="img-center" src={arrBack}/></button>
              </div>
            </BottomColumnContainer>
          </SubContainer>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  product: state.product,
  order: state.order,
})

const mapDispatchToProps = (dispatch) => {
  return {
    getProduct: dispatch.product.getProduct,
    setOrder: dispatch.order.setOrder,
    setOrderUserId: dispatch.order.setOrderUserId,
    setOrderID: dispatch.order.setOrderID,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ManageState))
