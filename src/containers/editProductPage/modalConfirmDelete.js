import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { customModalStyle, mobileModalStyle } from './customModalStyle'
import { Desktop, Mobile } from '../../utils/displayResponsive'

import ErrorBoundary from '../../components/errorBoundary'
import CustomModal from '../../components/customModal'
import Button from '../../components/button'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 3vw 4vw;
  @media only screen and (min-width: 451px) {
    padding: 15px 20px;
  }
`

const Text = styled.div`
  padding: 8vw 0;
  font-size: 1rem;
  @media only screen and (min-width: 451px) {
    padding: 40px 0;
  }
`
const ButtonContainer = styled.div`
  display: flex;
  width: 100%;
  height: 12vw;
  @media only screen and (min-width: 451px) {
    height: 60px;
  }
`

class ModalConfirmDelete extends Component {
  state = {}

  componentDidMount() {}

  closeModal = () => {
    console.log('closeModal')
    this.props.closeModal(false)
  }

  onCallBack = () => {
    console.log('onCallBack - Modal')
    this.props.onCallBack()
  }

  renderContent = () => {
    // const { onClose, images } = this.props
    // const { index } = this.state
    // console.log(this.state.index)
    return (
      <ErrorBoundary>
        <Mobile>
          <CustomModal isOpen={this.props.isOpen} customStyle={mobileModalStyle}>
            <Container>
              <Text>คุณต้องการลบรายการสินค้าดังกล่าวหรือไม่ ?</Text>
              <ButtonContainer>
                <Button
                  secondary
                  style={{ fontSize: '1rem', flex: '1', borderRadius: '10px', marginRight: '5vw' }}
                  onClick={() => this.closeModal()}>
                  ยกเลิก
                </Button>
                <Button
                  primaryCard
                  style={{ fontSize: '1rem', flex: '1', borderRadius: '10px' }}
                  onClick={() => this.onCallBack()}>
                  ตกลง
                </Button>
              </ButtonContainer>
            </Container>
          </CustomModal>
        </Mobile>
        <Desktop>
          <CustomModal isOpen={this.props.isOpen} customStyle={customModalStyle}>
            <Container>
              <Text>คุณต้องการลบรายการสินค้าดังกล่าวหรือไม่ ?</Text>
              <ButtonContainer>
                <Button
                  secondary
                  style={{ fontSize: '1rem', flex: '1', borderRadius: '10px', marginRight: '5vw' }}
                  onClick={() => this.closeModal()}>
                  ยกเลิก
                </Button>
                <Button
                  primaryCard
                  style={{ fontSize: '1rem', flex: '1', borderRadius: '10px' }}
                  onClick={() => this.onCallBack()}>
                  ตกลง
                </Button>
              </ButtonContainer>
            </Container>
          </CustomModal>
        </Desktop>
      </ErrorBoundary>
    )
  }

  render() {
    return <ErrorBoundary>{this.renderContent()}</ErrorBoundary>
  }
}

ModalConfirmDelete.propstype = {
  closeModal: PropTypes.func.isRequired,
  onCallBack: PropTypes.func.isRequired,
}

export default ModalConfirmDelete
