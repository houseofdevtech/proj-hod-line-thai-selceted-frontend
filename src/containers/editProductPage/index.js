import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { withTheme } from 'styled-components'
import { withRouter } from 'react-router-dom'

import _ from 'lodash'

import BeatLoader from 'react-spinners/BeatLoader'

import Frame from '../../components/frameLayout'
// import { MockUI } from '../../style/generalStyle'
import EditCard from '../../components/editProductListCard'
import FooterButton from '../../components/footerButton'
import ConfirmDeleteModal from './modalConfirmDelete'

// import { editProductData } from '../../inputData/editProductData'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  // align-items: center;
  // justify-content: center;
  flex: 1;
  padding: 10px 10px;
  hr {
    display: block;
    height: 2px;
    border: 0;
    border-top: 1px solid ${(props) => props.theme.primary};
  }
`

const TextMessage = styled.div`
  display: flex;
  height: 50vh;
  justify-content: center;
  align-items: center;
  text-align: center;
  font-size: 0.8rem;
  font-weight: 600;
  span {
    display: inline-block;
    vertical-align: middle;
  }
  p {
    color: ${(props) => props.theme.textPurple};
    text-decoration: underline;
    font-weight: 600;
    cursor: pointer;
  }
`

class EditProductPage extends Component {
  _isUpdated = false

  state = {
    isShowModal: false,
    isFilterProduct: false,
    isAllProductLoaded: false,
    filterProduct: [
      // {
      //   sku_id: 1783,
      //   sku_code: 'BHU004_2',
      //   color: { color_name: 'YELLOW', color_value: '#e0c948', color_img_path: '' },
      //   size: 'L',
      //   other: [{ name: 'ราคาเดิม', value: '990' }],
      //   main_img_path: 'https://vulcan.houseofdev.tech/wit/img/BHU4_YELLOW.png',
      //   sub_img_path: [],
      //   price: 199,
      //   qty: 6,
      //   status: 0,
      // },
      // {
      //   sku_id: 1782,
      //   sku_code: 'BHU004_1',
      //   color: { color_name: 'YELLOW', color_value: '#e0c948', color_img_path: '' },
      //   size: 'M',
      //   other: [{ name: 'ราคาเดิม', value: '990' }],
      //   main_img_path: 'https://vulcan.houseofdev.tech/wit/img/BHU4_YELLOW.png',
      //   sub_img_path: [],
      //   price: 199,
      //   qty: 6,
      //   status: 0,
      // },
    ],
    selectedItems: null,
    isLineUserId: false,
    allProductId: null,
  }

  isInternalLink = () => {
    return 'from' in this.props.location.state
  }

  componentDidMount() {
    if (this.isInternalLink()) {
      // old
      const { all_product_id, shop_id } = this.props.location.state
      console.log(`ALL PRODUCT :: ${JSON.stringify(this.props.location.state)}`)
      // Distribute all_product_id
      // and getProduct
      all_product_id.map((product_id) => {
        let payload = {
          product_id: product_id,
          shop_id: shop_id,
        }
        this.props.getProduct(payload)
        return null
      })
      this.setState({ allProductId: all_product_id })
      this.props.setOrder({ user_id: this.props.user.chattyUserId })
      this.props.setOrder({ product_id: all_product_id })
      this.props.location.state.shop_id &&
        this.props.setOrder({ shop_id: this.props.location.state.shop_id })
    } else {
      // new getOrder
      //
      // getOrder(userId) >> all_product_id
      // but userId may not be load in componentDidMount() step
      // so this step will trigger at componentDidUpdate() once instead
      // use ... all_product_id (from getOrder),
      //     ... shop_id (qsString),
      // getAllProduct()
      // setOrder({}) .... prefill editOrder api body
    }
  }

  isLineUserIdCheck = () => {
    return (
      !this._isUpdated &&
      !this.state.isLineUserId &&
      this.props.user.line.userId !== '' &&
      this.props.user.chattyUserId !== ''
    )
  }

  componentDidUpdate = async () => {
    if (this.isLineUserIdCheck()) {
      this._isUpdated = true
      this.setState({ isLineUserId: true })

      const isGetOrder = await this.props.getOrder(this.props.user)
      if (isGetOrder) {
        const { allProductId } = this.props.payment

        console.log(`isGetOrder :: ${isGetOrder}`)
        console.log(`allProductId :: ${allProductId}`)

        const all_product_id = allProductId !== null && allProductId
        const { shop_id } = this.props.location.state

        all_product_id.map((product_id) => {
          let payload = {
            product_id: product_id,
            shop_id: shop_id,
          }
          this.props.getProduct(payload)
          return null
        })

        this.setState({ allProductId: all_product_id })
        this.props.setOrder({ user_id: this.props.user.chattyUserId })
        this.props.setOrder({ product_id: all_product_id })
        this.props.location.state.shop_id &&
          this.props.setOrder({ shop_id: this.props.location.state.shop_id })
      } else {
        console.log('isGetOrder false')
      }
    }

    if (
      this.state.isAllProductLoaded &&
      !_.isEmpty(this.props.edit.product) &&
      !this.state.isFilterProduct
    ) {
      console.log('hello didUpdate')
      console.log(this.props.payment.order)
      //filter product from order
      this.getProductfromOrder()
    }
    // Check is all product has been loaded
    if (
      this.state.allProductId !== null &&
      // this.props.location.state.all_product_id.length === this.props.edit.product.length &&
      this.state.allProductId.length === this.props.edit.product.length &&
      !this.state.isAllProductLoaded
    ) {
      this.setState({ isAllProductLoaded: true })
    }
  }

  componentWillUnmount() {
    this.props.clearProduct()
  }

  getProductfromOrder = () => {
    if (!_.isEmpty(this.props.edit.product)) {
      // console.log(`PROP PAYMENT :: ${JSON.stringify(this.props.payment)}`)
      const { order } = this.props.payment
      console.log(`ORDER :: ${JSON.stringify(order)}`)
      if (!_.isEmpty(order.order_items)) {
        // +++++++++  Get 'sku_id' from Order
        let arr_sku_id = []
        let orderItems = []
        // const skuIdFromOrders =
        order.order_items.map((product) => {
          return product.product_items.map((product_items) => {
            const item = { id: product_items.sku_id, qty: product_items.qty }
            orderItems.push(item)
            arr_sku_id.push(product_items.sku_id)
            // return product_items.sku_id
            return null
          })
        })

        console.log(`ORDER ITEM :: ${JSON.stringify(orderItems)}`)

        // console.log(`skuIdFromOrders :: ${arr_sku_id}`)

        // +++++++++  Get Product detail by filter from 'sku_id'
        const { product } = this.props.edit

        let filterProduct = []
        arr_sku_id.map((sku_id) => {
          console.log(sku_id)
          // console.log(product)
          product.map((obj) => {
            obj.product_data.map((product) => {
              // console.log(objItem.product_items.filter((obj) => obj.sku_id === id))
              // const matchedProduct =
              product.product_items.filter((product_item) => {
                if (product_item.sku_id === sku_id) {
                  filterProduct.push(product_item)
                }
                return null
              })
              // console.log(`MATCH :: ${JSON.stringify(matchedProduct)}`)
              return null
            })
            return null
          })
          return null
        })
        console.log(`filterProduct :: ${JSON.stringify(filterProduct)}`)

        this.props.setOrder({ items: orderItems })
        this.setState({
          filterProduct: filterProduct,
          isFilterProduct: true,
          selectedItems: orderItems,
        })
      }
    }
  }

  getFilterProduct = (id) => {
    return this.state.filterProduct.filter((obj) => obj.sku_id === id)
  }

  setStateSelectedItems = (selectedItems) => {
    console.log(`setStateSelectedItems -> selectedItems :: ${JSON.stringify(selectedItems)}`)
    if (!_.isEmpty(selectedItems)) {
      this.props.setOrder({ items: selectedItems })
    } else {
      console.log('empty')
      this.props.deleteOrderItems()
    }
    this.setState({ selectedItems: selectedItems })
  }

  isExistingProduct = (arrayItems, id) => {
    return arrayItems.find((item) => item.id === id)
  }
  findExistingProduct = (arrayItems, id) => {
    return arrayItems.findIndex((item) => item.id === id)
  }

  onChangeSelectedItems = (id, value) => {
    const { selectedItems } = this.state
    console.log(`id :: ${id}`)
    // console.log(`value :: ${value}`)
    const index = this.findExistingProduct(selectedItems, id)
    // console.log(this.isExistingProduct(selectedItems, id))
    // console.log(index)
    if (this.isExistingProduct(selectedItems, id)) {
      if (value !== 0) {
        // if Value is not Zero => adjust cart value
        let selectedItems = this.state.selectedItems
        selectedItems[index].qty = value
        this.setStateSelectedItems(selectedItems)
      } else {
        // if Value is Zero => remove from cart
        let selectedItems = this.state.selectedItems
        const resultItems = selectedItems.filter((o) => o.id !== id)
        // this.setState({ selectedItems: resultItems })
        this.setStateSelectedItems(resultItems)
      }
    } else {
      let selectedItems = this.state.selectedItems
      // this.setState({ selectedItems: [...selectedItems, { id: id, qty: 1 }] })
      selectedItems.push({ id: id, qty: 1 })
      this.setStateSelectedItems(selectedItems)
    }
  }

  showValue = (selectedItems, key) => {
    // console.log(`showValue -> selectedItems :: ${JSON.stringify(selectedItems)}`)
    if (selectedItems === null) {
      return 0
    } else {
      const value = selectedItems.filter((o) => {
        if ('id' in o) {
          // console.log(`showValue o.id == key :: ${o.id} == ${key}`)
          return o.id === key
        } else {
          return null
        }
      })
      const result = _.isEmpty(value) ? 0 : value[0].qty
      // console.log(`showValue :: ${result}`)
      return result
    }
    // console.log(`this showValue :: ${JSON.stringify(value)}`)
  }

  handleConfirmDelete = (id) => {
    console.log(`handleConfirmDelete`)
    this.setState({ isShowModal: true, willDeletedId: id })
  }

  closeModal = () => {
    this.setState({ isShowModal: false })
  }

  onCallBack = () => {
    console.log('onCallBack - Edit Page')
    this.onChangeSelectedItems(this.state.willDeletedId, 0)
    this.setState({ isShowModal: false })
  }

  handleSubmitEdit = () => {
    console.log(`handleSubmitEdit`)
  }

  handlePrimaryButton = async () => {
    const res = await this.props.editOrder()
    const liff = window.liff
    if (res) {
      if (this.isInternalLink()) {
        this.props.history.goBack()
      } else {
        liff.closeWindow()
      }
    }
  }

  getProductDetailBySkuId = (skuId) => {
    const product = this.props.edit.product
    let result = null

    product.map((p) => {
      p.product_data.map((pData) => {
        pData.product_items.map((pItem) => {
          // console.log('inner')
          // console.log(`${pItem.sku_id} == ${skuId}`)

          if (pItem.sku_id === skuId) {
            // console.log('found')
            result = {
              product_id: pData.product_id,
              product_name: pData.product_name,
            }
          }
          return null
        })
        return null
      })
      return null
    })
    return result
  }

  renderCards = (selectedItems) => {
    let Cards = []

    selectedItems.map((item, index) => {
      const size = selectedItems.length

      const filterProduct =
        this.state.isAllProductLoaded && this.state.filterProduct && this.getFilterProduct(item.id)
      const product = filterProduct[0]

      //Get product_id and product_name from this.props.edit.product
      const productDetail = this.getProductDetailBySkuId(item.id)
      let orderItem = { product_id: 'ไม่มีข้อมูล', product_name: 'ไม่มีข้อมูล' }
      if (productDetail !== null) {
        orderItem = productDetail
      } else {
        orderItem = { product_id: 'ไม่มีข้อมูล', product_name: 'ไม่มีข้อมูล' }
      }

      console.log(`product :: ${JSON.stringify(product)}`)
      if (this.state.filterProduct && this.showValue(this.state.selectedItems, item.id) > 0) {
        Cards.push(
          <div key={index}>
            <EditCard
              productId={orderItem.product_id}
              name={orderItem.product_name}
              shopId={this.props.location.state.shop_id}
              locationState={this.props.location.state}
              image={product.main_img_path}
              maxQty={product.qty}
              size={product.size}
              color={product.color}
              price={product.price}
              id={product.sku_id}
              qty={this.showValue(this.state.selectedItems, product.sku_id)}
              onChangeSelectedItems={this.onChangeSelectedItems}
              handleConfirmDelete={this.handleConfirmDelete}
            />
            {index < size - 1 && <hr />}
          </div>
        )
      } else {
      }
      return null
    })
    return Cards
  }

  Footer = () => {
    return (
      <FooterButton
        primaryColor={this.props.theme.tertiary}
        backIconColor={this.props.theme.primary}
        handleBack={() => this.props.handleBack(this.props.history)}
        handlePrimaryButton={() => this.handlePrimaryButton()}
        isActive={true}
        labelPrimary="บันทึกการแก้ไข"
      />
    )
  }

  render() {
    // console.log(`handleback :: ${this.props.handleBack}`)
    console.log(`edit location state  :: ${JSON.stringify(this.props.location.state)}`)
    console.log(`edit (self state) :: ${JSON.stringify(this.state)}`)
    console.log(`edit (filterProduct) :: ${JSON.stringify(this.state.filterProduct)}`)
    console.log(`edit (redux order)   :: ${JSON.stringify(this.props.order)}`)
    console.log(`edit (redux edit)    :: ${JSON.stringify(this.props.edit)}`)
    // const { order } = this.props.payment
    const { selectedItems } = this.state
    return (
      <div>
        <Frame
          bodyHeight="max-content"
          primaryColor={this.props.theme.tertiary}
          bottomContent={_.isEmpty(selectedItems) ? <div /> : <this.Footer />}
          paddingBottom={true}
          secondaryColor={'#fff'}>
          <Container>
            {selectedItems === null ? (
              <TextMessage>
                <BeatLoader size={15} color={this.props.theme.primary} loading={true} />
              </TextMessage>
            ) : _.isEmpty(selectedItems) ? (
              <TextMessage>
                <span>
                  <div>รายการสั่งซื้อนี้ไม่สามารถชำระเงินได้</div>
                  <div>เนื่องจากคุณมีการอัปเดตสินค้า</div>
                  <br />
                  <p onClick={() => this.props.history.push('/address')}>
                    โปรดคลิกที่นี่เพื่อชำระสินค้าที่คุณอัพเดต
                  </p>
                </span>
              </TextMessage>
            ) : this.state.isAllProductLoaded ? (
              this.renderCards(selectedItems)
            ) : (
              <div> Loading </div>
            )}
          </Container>
        </Frame>

        <ConfirmDeleteModal
          isOpen={this.state.isShowModal}
          onCallBack={this.onCallBack}
          closeModal={(data) => {
            this.setState({ isShowModal: data })
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  edit: state.edit,
  payment: state.payment,
  order: state.order,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    clearProduct: dispatch.edit.clearProduct,
    getProduct: dispatch.edit.getProduct,
    deleteOrderItems: dispatch.order.deleteOrderItems,
    setOrder: dispatch.order.setOrder,
    editOrder: dispatch.order.editOrder,
    getOrder: dispatch.payment.getOrder,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(EditProductPage)))
