import React, { Component } from 'react'
import { connect } from 'react-redux'

import { withRouter } from 'react-router-dom'

// import CarouselSection from '../../components/carousel'
// import { picIndex } from '../../components/carousel'

import Page from './page'
import LoadingScreen from '../../views/loadingPage'

import { isValidProductQS } from '../../utils/assist'

class LandingAskPage extends Component {
  state = {
    //isExternalLink : false
  }

  componentDidMount = async () => {
    // **** Preload Product before landing Target Page ****
    // this.props.setProductData(ProductData)
    // this.props.setCartNamePrice(ProductData)

    // State with pass by appControl.js
    const qsObject = this.props.location.state
    // console.log(`qsObject : ${JSON.stringify(qsObject)}`)
    // Check is Valid QueryString (which has user_id and product_id to initiate api call)
    if (isValidProductQS(qsObject)) {
      // if all queryString have been validated, then send to api method to call
      // const data = await this.props.getProduct(qsObject)
      await this.props.getProduct(qsObject)
      // this.setState({qsObject : qsObject})
    } else {
      console.log(
        `incomplete queryString :: This page may entry from internal link or Invalid QueryString`
      )
    }

    // Check this qsObject that there is External Link or not
  }

  // componentWillUnmount() {
  //   console.log('product UnMount')
  //   this.props.location.state = {}
  //   this.props.setProductData({})
  // }
  handleSubmit = () => {
    this.props.handleSubmitAskSeller(this.state.comment)
    // write code for animation that
    // "Question have been submit"
    // and
    // liff.sendMessage to chat with comment
    // then close
  }

  isRecievedData = (props) => {
    return 'product' in props.product
  }

  isInternalLink = (props) => {
    console.log('isInternalLink')
    return 'from' in props.location.state && props.location.state.from === '/product'
  }

  render() {
    // data && console.log(`data :: ${JSON.stringify(data)}`)

    // +++++++++++++++++++++++++++++++++++++
    // The data is passed from Product Page throught Askbutton then Link to this Ask Page
    // by using this.props.location.state from withRouter to connect the data
    // +++++++++++++++++++++++++++++++++++++
    const data = this.props.location.state
    // const data = ('data' in this.props.location.state)
    //   ? this.props.location.state
    //   : { data: this.props.product.product_data }

    // console.log(`product :: ${JSON.stringify(this.props.product.product_data)}`)
    // console.log(`data :: ${'data' in this.props.location.state}`)

    return (
      <div>
       {this.isRecievedData(this.props) || this.isInternalLink(this.props) ?
          <Page data={data} {...this.props} />
        :
          <LoadingScreen/>
       }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  product: state.product,
  ask: state.ask,
  checkPayment: state.checkPayment,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    setProductData: dispatch.product.setProductData,
    getProduct: dispatch.product.getProduct,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LandingAskPage))
