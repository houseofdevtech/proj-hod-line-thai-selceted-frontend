import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled, { withTheme } from 'styled-components'
import { withRouter } from 'react-router-dom'
import _ from 'lodash'

// import CarouselSection from '../../components/carousel'
// import { picIndex } from '../../components/carousel'

// import ButtonPane from '../../components/buttonPaneTest'
import FooterButton from '../../components/footerButton'

import { Container, SubContainer, Header } from '../../style/productStyle'
import AskSection from './sectionAsk'

const PicturePane = styled.div`
  height: 100%;

  width: 100%;
  align-self: center;
  background: ${(props) => props.theme.secondary};

  img {
    /* height: 30vh; */
    width: 100%;
    object-fit: cover;
  }

  @media only screen and (min-width: 451px) {
    height: 200px;
  }
`

class AskPage extends Component {
  state = {
    comment: '',
  }

  async componentDidMount() {
    const productsData = await this.props.product.product
    // If load from query String & Product and
    // images will be first picture refer from [this.props.ask.pictureIndex]
    // if (productsData && productsData.length === 1 && !this.props.location.state.data) {
    if (productsData && !this.props.location.state.data) {
      const items = productsData.product_data[0].product_items
      const product_name = productsData.product_data[0].product_name
      const images = this.setImage(items)
      const minPrice = this.setMinPrice(items)
      this.setState({
        images: images,
        product_minPrice: minPrice,
        product_name: product_name,
        selectedPrice: null,
      })
    }
    // If load from Internal this.props.location.state.data
    // which pass value from with router
    if (this.props.location.state.data) {
      const {
        product_name,
        selectedPrice,
        product_minPrice,
        images,
      } = this.props.location.state.data
      this.setState({
        images: images,
        product_minPrice: product_minPrice,
        product_name: product_name,
        selectedPrice: selectedPrice,
      })
    }
  }

  setImage = (product_items) => {
    let images = []
    // push 1st image from "main_img_path"
    images.push(product_items[0].main_img_path)
    // push other images fron "sub_img_path"
    product_items[0].sub_img_path.map((obj) => images.push(obj.img_path))
    // return array of images url
    return images
  }

  setMinPrice = (product_items) => {
    let prices = []
    product_items.map((obj) => prices.push(obj.price))
    console.log(prices)
    // console.log (`Math :: ${_.min( prices )}`)
    // return min Price from array
    return _.min(prices)
  }

  handleSubmit = () => {
    if (this.props.data.data === undefined) {
      const image = this.state.images[this.props.ask.pictureIndex]
      const product_name = this.state.product_name
      const showPrice = this.state.product_minPrice
      this.props.handleSubmitAskSeller(this.state.comment, image, product_name, showPrice)
    } else {
      const image = this.state.images[this.props.ask.pictureIndex]
      const product_name = this.props.data.data.product_name
      const showPrice = this.props.data.data.selectedPrice
        ? this.props.data.data.selectedPrice
        : this.state.product_minPrice
      this.props.handleSubmitAskSeller(this.state.comment, image, product_name, showPrice)
    }
    // write code for animation that
    // "Question have been submit"
    // and
    // liff.sendMessage to chat with comment
    // then close
  }

  handleChange = (e) => {
    this.setState({
      comment: e.target.value,
    })
  }

  isInternalLink = (props) => {
    return 'from' in props.location.state
  }

  // isUserNotExist = () => {
  //   console.log(`isUserNotExist :: ${this.props.user.line.userId === ''}`)
  //   return this.props.user.line.userId === ''
  // }

  isUserNotExist = () => {
    console.log(`isUserNotExist :: ${this.props.user.line.userId === ''}`)
    return this.props.user.line.userId === ''
  }
  isUserExist = () => {
    return this.props.user.line.userId !== '' && this.state.comment !== ''
  }

  render() {
    // const { images } = this.props.product

    // The data is passed from Product Page throught Askbutton then Link to this Ask Page
    // by using this.props.location.state from withRouter to connect the data
    // const { images } = this.props.location.state.data
    // const data = this.props.location.state
    // const { images } = this.props.data.data
    const { images } = this.state
    const data = this.props.data
    data && console.log(`ask ('data' props) :: ${JSON.stringify(data)}`)
    console.log(`ask (self state): ${JSON.stringify(this.state)}`)

    // const image = this.state.images ? this.state.images[this.props.ask.pictureIndex] : null
    // const product_name = this.props.data.data.product_name
    // const showPrice = this.props.data.data.selectedPrice
    //   ? this.props.data.data.selectedPrice
    //   : this.state.product_minPrice
    return (
      <Container>
        <SubContainer>
          <Header>
            <PicturePane>
              {images && <img alt="askPic" src={images[this.props.ask.pictureIndex]} />}
            </PicturePane>
            {/* <ButtonPane
              clickOne={() => this.props.handleAsk1(this.state.comment, image)}
              clickTwo={() =>
                this.props.handleAsk2(this.state.comment, image, product_name, showPrice)
              }
              clickThree={() =>
                this.props.handleAsk3(this.state.comment, image, product_name, showPrice)
              }
            /> */}
            <AskSection onChange={this.handleChange} value={this.state.comment} data={this.state} />
          </Header>
          {/* {
            isExternalLink (this.props.ext === true) ? 
            <a></a>:
            <FooterButton/>
          } */}
          <FooterButton
            labelPrimary="ส่งข้อความถึงผู้ขาย"
            primaryColor={this.props.theme.tertiary}
            backIconColor={this.props.theme.primary}
            handleBack={() => this.props.handleBack(this.props.history)}
            handlePrimaryButton={() => this.handleSubmit()}
            handleInactiveButton={() => null}
            isActive={this.isUserExist()}
            isMessage={this.isUserNotExist()}
            message={'คุณยังไม่ได้มี line_id'}
          />
        </SubContainer>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  product: state.product,
  ask: state.ask,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(AskPage)))
