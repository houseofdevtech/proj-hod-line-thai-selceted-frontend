import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled, { withTheme } from 'styled-components'

import {
  // Container,
  WhiteContainer,
  ThemesContainer,
  FrameContainer,
  TitleArea,
  Title,
  //Text,
  Price,
  SelectedContainer,
  BottomRowContainer,
  BottomColumnContainer,
  BottomLeftBackground,
  BottomLeftSquare,
  BottomLeftFiller,
  Container,
} from '../../style/contentStyle'

const TextAreaContainer = styled.div`
  padding: 30px 30px 10px 20px;
`

const SelectedContainerModify = styled.div`
  top: '-7px';
`

const TextArea = styled.textarea`
  // position:absolute;
  flex: 10 1 auto;
  background:${(props) => {
    return `${props.theme.tertiary}`
  }}
  border: 2px solid ${(props) => {
    return `${props.theme.primary}`
  }}
  color: ${(props) => {
    return `${props.theme.text}`
  }}
  overflow-y: auto;
  padding: 15px 10px;
  outline: none;
  resize: none;
  width: 100%;
  height: 40vh;
  border-radius: 10px;
  
  box-sizing: border-box;
  margin-bottom: 10px;
  font-size: 18px;

`
const TitleAreaModify = styled.div`
  padding: 20px 10px 0px;
`

class SectionAsk extends Component {
  render() {
    // const { title, price } = this.props.product
    const { product_name, selectedPrice, product_minPrice } = this.props.data
    // console.log(`selectedPrice :: ${selectedPrice}`)
    // console.log(`product_minPrice :: ${product_minPrice}`)
    const { value, onChange } = this.props
    return (
      <Container>
        <WhiteContainer>
          <FrameContainer>
            <TitleAreaModify>
              <TitleArea>
                <Title> {product_name}</Title>
                {/* if no selectedPrice show min price instead, if have show its price */}
                <Price>
                  {`${
                    selectedPrice === 0 || !selectedPrice
                      ? product_minPrice
                        ? `${product_minPrice.toLocaleString('th')}`
                        : null
                      : selectedPrice
                      ? `${selectedPrice.toLocaleString('th')}`
                      : null
                  } THB`}
                </Price>
              </TitleArea>
            </TitleAreaModify>
          </FrameContainer>
          <BottomColumnContainer>
            <BottomRowContainer>
              <BottomLeftFiller>
                <BottomLeftBackground>
                  <BottomLeftSquare />
                </BottomLeftBackground>
              </BottomLeftFiller>
              <ThemesContainer>
                <SelectedContainerModify>
                  <SelectedContainer>
                    <TextAreaContainer>
                      <TextArea
                        placeholder="พิมพ์ข้อความที่คุณต้องการสอบถาม..."
                        onChange={onChange}
                        value={value}
                      />
                    </TextAreaContainer>
                  </SelectedContainer>
                </SelectedContainerModify>
              </ThemesContainer>
            </BottomRowContainer>
          </BottomColumnContainer>
        </WhiteContainer>
      </Container>
    )
  }
}

const mapStateToProps = (state) => ({
  cart: state.cart,
  product: state.product,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = () => {
  return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(withTheme(SectionAsk))
