import React, { Component } from 'react'
import { connect } from 'react-redux'

import Page from './page'
import LoadingScreen from '../../views/loadingPage'

class MyStatusLanding extends Component {
  _isUpdated = false

  state = {
    isLineUserId: false,
  }

  componentDidMount = async () => {
    // const checkUser = await this.props.checkUser(this.props.user.line.userId)
    // if (checkUser) {
    //   this.props.getTracking(this.props.user.chattyUserId)
    // }
  }

  componentDidUpdate = async () => {
    if (!this._isUpdated && !this.state.isLineUserId && this.props.user.line.userId !== '') {
      if (this.props.user.chattyUserId && this.props.user.chattyUserId !== '') {
        this._isUpdated = true
        this.setState({ isLineUserId: true })
        await this.props.getTracking(this.props.user.chattyUserId)
        await this.props.getAddress(this.props.user)
      }
    }
  }

  isTracking = (props) => {
    return 'tracking' in this.props.status
  }

  render() {
    return <div>{!this.isTracking(this.props) ? <LoadingScreen /> : <Page />}</div>
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  status: state.status,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    checkUser: dispatch.user.checkUser,
    getTracking: dispatch.status.getTracking,
    getAddress: dispatch.address.getAddress,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyStatusLanding)

// import { MockUI } from '../../style/generalStyle'
// <div>
//         <MockUI>
//           <img alt="mockUI" src={`${process.env.PUBLIC_URL}/asset/img/mock_page_mystatus.jpg`} />
//         </MockUI>
//       </div>
