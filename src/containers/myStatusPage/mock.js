export const MOCK_TRACKING = [
  { _id: 1, type: "waitpaying" },
  { _id: 2, type: "tracking" },
  { _id: 3, type: "waitpaying" },
  // { _id: 4, type: "notified" },
  { _id: 4, type: "tracking" },
  { _id: 5, type: "waitpaying" },
  { _id: 6, type: "tracking" },
  // { _id: 7, type: "notified" },
  { _id: 7, type: "waitpaying" },
  { _id: 8, type: "tracking" },
];

export const MOCK_HISTORY = [
  { _id: 1, type: "done" },
  { _id: 2, type: "done" },
];