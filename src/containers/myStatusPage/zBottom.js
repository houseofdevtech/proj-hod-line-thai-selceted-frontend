import React from 'react'
import styled from 'styled-components'
import BottomBackground from '../../images/topPayment.png'

const BottomContent = styled.div`
  position: relative;
  display: block;
  margin: 10px 0 10px
  width: inherit;
  text-align: center;
  font-size: 0.9rem;
  z-index: 20;
  color: ${(props) => props.theme.textPurple};
  img {
    position: relative;
    object-fit: scale-down;
    width: 100%;
    height: 30vw;
  }
  div {
    height: 35vw;
  }

  @media only screen and (min-width: 451px) {
    width: inherit;
    img {
      position: relative;
      object-fit: cover;
      width: 100%;
      height: 100%;
    }
  }
`

export const Bottom = (props) => {
  return (
    <BottomContent>
      <img alt="top-background" src={BottomBackground} />
    </BottomContent>
  )
}
