import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import styled, { withTheme } from 'styled-components'
// import { withTheme } from 'styled-components'
import _ from 'lodash'

import FramemyStatus from '../../components/frameLayout/FrameMyStatus'

import { Bottom } from './zBottom'
import { Top } from './zTop'

// import TrackingCard from '../../components/statusTrackingCard'
import TrackingCard from '../../components/statusTrackingCard'
// import HistoryCard from '../../components/statusHistoryCard'
// import { MOCK_HISTORY } from './mock.js'

import PackageBox from '../../images/packing.svg'

const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 0 10px 5px;
  width: inherit;
`

const EmptyBoxContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  flex-direction: column;
  color: ${(props) => props.theme.textPurple};
  font-size: 0.85rem;
  height: 20vh;
  div {
    margin-left: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    div {
      margin-top: 10px;
      margin-left: 0px;
    }
  }
  img {
    align-self: center;
    justify-self: center;
  }
`
const EmptyBox = (props) => {
  return (
    <EmptyBoxContainer>
      <div>
        <img src={PackageBox} alt="box" />
        {props.displayMode === 'tracking' ? (
          <div>ยังไม่มีรายการสั่งซื้อ</div>
        ) : (
          <div>ยังไม่มีประวัติการสั่งซื้อ</div>
        )}
      </div>
    </EmptyBoxContainer>
  )
}

const tabData = [
  {
    label: 'สถานะการจัดส่ง',
    mode: 'tracking',
    key: '0',
  },
  {
    label: 'ประวัติการสั่งซื้อ',
    mode: 'history',
    key: '1',
  },
]

class MyStatusPage extends Component {
  state = {
    displayMode: 'tracking',
    tabData: tabData,
  }

  onChangeTab = (index) => {
    let resultMode = null
    const tabData = this.state.tabData.map((obj) => {
      // Modify displayMode
      if (obj.key === index) {
        resultMode = obj.mode
      }
      // Modify tabData
      return {
        ...obj,
        active: obj.key === index ? true : false,
      }
    })
    this.setState({
      selectedTabIndex: index,
      tabData: tabData,
      displayMode: resultMode,
    })
  }

  renderTrackingCards = () => {
    // if (filterBy === 'all') {
    if (this.state.displayMode === 'tracking') {
      return _.chain(this.props.status.tracking)
        .sortBy((item) => item.date_order)
        .reverse()
        .map((item, index) => <TrackingCard key={index} type={item.type} item={item} />)
        .value()
    } else {
      return <EmptyBox displayMode={this.state.displayMode} />
      // return _.chain(MOCK_HISTORY)
      //   .sortBy((item) => item.type)
      //   .reverse()
      //   .map((item, index) => <HistoryCard key={index} type={item.type} />)
      //   .value()
    }

    // }
    // const filtered = _.filter(MOCK_ORDERS, (order) => order.type === filterBy)
    // return _.map(filtered, (item, index) => <OrderCard key={index} type={item.type} />)
  }
  // {
  //   this.props.status.tracking.map((obj,index) =>
  //   <div key={index} >
  //     <TrackingCard data={obj}/>
  //     {
  //       index < this.props.status.tracking.length-1 &&
  //       <hr/>
  //     }
  //   </div>
  //   )
  // }
  render() {
    console.log(`myStatus :: ${JSON.stringify(this.state)}`)
    // console.log(`myStatus (address) :: ${JSON.stringify(this.props.address)}`)
    return (
      <div>
        <FramemyStatus
          primaryColor={this.props.theme.tertiary}
          secondaryColor={'#fff'}
          paddingBottom={false}
          topContent={
            <Top
              tabData={this.state.tabData}
              onChangeTab={this.onChangeTab}
              user={this.props.address}
              history={this.props}
            />
          }
          topHeight="max-content"
          bottomContent={<Bottom />}
          bodyHeight="max-content"
          marginRight="0px"
          marginLeft="0px">
          <CardContainer>{this.renderTrackingCards()}</CardContainer>
        </FramemyStatus>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  status: state.status,
  address: state.address,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    getTracking: dispatch.status.getTracking,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(MyStatusPage)))
