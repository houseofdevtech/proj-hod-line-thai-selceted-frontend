import React from 'react'
import styled from 'styled-components'
import TabComponent from '../../components/tabGeneralComponent'

import EditIcon from '../../images/edit.svg'

const TopContent = styled.div`
  position: relative;
  display: block;
  margin: 10px 0 10px
  width: inherit;
  text-align: center;
  font-size: 0.9rem;
  z-index: 20;
  color: ${(props) => props.theme.textPurple};
  img {
    position: relative;
    object-fit: scale-down;
    width: 100%;
    height: 30vw;
  }
  div {
    height: 35vw;
  }

  @media only screen and (min-width: 451px) {
    width: inherit;
    img {
      position: relative;
      object-fit: cover;
      width: 100%;
      height: 100%;
    }
  }
`

const TopUserContent = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1 100%;
  align-self: flex-start;
  z-index: 20;
  color: ${(props) => props.theme.textPurpleLight};
  padding: 0 8vw 4vw;
  div {
    z-index: 30;
    postion: absolute;
    font-size: 0.8rem;
    // margin: 0 2vw 0 2vw;
  }
  @media only screen and (min-width: 451px) {
    padding: 0 40px 20px;
  }
`

const TopUserName = styled.div`
  align-self: flex-start;
  color: ${(props) => props.theme.textPurple};
`

const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 1;
`
const toAddress = (props) => {
  props.history.push({ pathname: '/address', state: { from: props.location.pathname } })
}

const EditPane = styled.div`
  disply: inline-block;
  color: ${(props) => props.theme.textPurple};
  // justify-content: center;
  // align-items: flex-end;
  vertical-align: middle;
  margin-right: ${(props) => props.mr};
  cursor: pointer;
  img {
    vertical-align: middle;
    margin-right: 3px;
    margin-bottom: 5px;
    width: 15px;
    height: 15px;
    align-self: center;
  }
  span {
    display: inline-block;
    vertical-align: middle;
  }
`

const EditButton = (props) => {
  return (
    <EditPane mr={props.marginRight} onClick={() => props.onClick()}>
      <img alt="edit-icon" src={EditIcon} />
      <span>แก้ไข</span>
    </EditPane>
  )
}



// <TopContent>
//         <img alt="top-background" src={TopBackground} />
//       </TopContent>
// ${props.user.address.name}
//{props.user.address.full_address}

export const Top = (props) => {
  
  return (
    <div>
      <TopContent>สถานะของฉัน</TopContent>
      <TopUserContent>
        <TopContainer>
          <TopUserName>{`คุณ ${props.user.name} `}</TopUserName>
          <EditButton
            marginRight="5px"
            style={{ position: 'absolute' }}
            onClick={() => toAddress(props.history)}
          />
        </TopContainer>

        <div>ที่อยู่จัดส่ง :</div>
        {/* <div>123/456 คอนโดทวีสุข ตึก เอ แขวงบางซื้อ เขตบางซื่อ กรุงเทพฯ 10800</div> */}
        <div>{props.user.full_address}</div>
      </TopUserContent>
      <TabComponent transparent style={{}} tabData={props.tabData} onChangeTab={props.onChangeTab} />
    </div>
  )
}
