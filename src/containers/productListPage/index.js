// import styled from "styled-components";
import React, { Component } from 'react'
import { connect } from 'react-redux'
// import queryString from 'query-string'
import { withRouter } from 'react-router-dom'
import { withTheme } from 'styled-components'
import {
  Container,
  SubContainer,
  TopBackground,
  TopContainer,
  TopContent,
  ButtonContainer,
  ThemesContainer,
  BottomRowContainer,
  BottomColumnContainer,
  BottomLeftFiller,
  BottomLeftBackground,
  BottomLeftSquare,
  SelectedContainer,
  ContentContainer,
  ContentVerticalContainer,
  EmptyContentContainer,
} from '../../style/generalStyle'
import axios from 'axios'
import _ from 'lodash'
import Button from '../../components/button'
import ContentSection from '../../components/contentSection'
import ContentVerticalSection from '../../components/contentVerticalSection'
import NoContentSection from '../../components/noContentSection'
// import { menus } from '../../inputData/plMenus'
// import { contents } from '../../inputData/plContents'
import { contents, menus } from '../../inputData/plContentsPassione'
import { fluidRange } from 'polished'
// outside react state
// for store product type index
var id = null
class ProductPage extends Component {
  state = {
    // type: null,
    // type: 'new',
    type: 'all',
    index: null,
    province_show: [],
    reg: [],
    regions: [],
  }
  componentDidMount() {
    this.getProvince()
  }
  componentDidUpdate() {
    let array = []
    // {product: [],tenStore: []}
  }
  handleSelect = async (type, i) => {
    this.setState({ type: type })
    let ggwp = []
    let pro = []
    this.setState({ reg: this.props.regions })
    const temp = await axios
      .get(
        `https://dev.houseofdev.tech/thai-selected/admin/pages/api/selectByRegion.php?region=${type}`
      )
      .then((res) => {
        if (this.props.regions?.provinceName === res.data.provinceName) {
          ggwp.push(Object.assign(this.state.reg, res.data.provinceName))
        }
        console.log('ggwp', this.state.reg)
        this.props.setRegions(res.data)
        return res.data
      })
      .catch((error) => console.log('error', error))
    console.log('resxx1', _.uniqBy(temp, 'provinceName'))
    const result = await Promise.all(
      temp.map(async (e) => {
        const x = await axios
          .get(
            `https://dev.houseofdev.tech/thai-selected/admin/pages/api/select10list.php?provinceID=${e.provinceID}`
          )
          .then((result) => {
            if (result.data.length > 0) {
              Object.assign(e, [result.data])
              console.log('eee', e)
              return result.data
            }
          })
          .catch((error) => console.log('error', error))
        return x
      })
    )
    this.props.setRegions(_.uniqBy(temp, 'provinceName'))
    this.setState({ regions: this.props.regions })
    console.log('oop', this.state.regions)
  }
  getProvince = async () => {
    const Province = await axios
      .get('https://dev.houseofdev.tech/thai-selected/admin/pages/api/listAllProvince.php')
      .then((result) => {
        return result.data
      })
      .catch((error) => console.log('error', error))
    const result = await Promise.all(
      Province.map(async (e) => {
        const x = await axios
          .get(
            `https://dev.houseofdev.tech/thai-selected/admin/pages/api/select10list.php?provinceID=${e.provinceID}`
          )
          .then((result) => {
            if (result.data.length > 0) {
              this.state.province_show.push(e)
              Object.assign(e, [result.data])
              console.log('eee', e)
              return result.data
            }
          })
          .catch((error) => console.log('error', error))
        return x
      })
    )
    this.props.setProvince(Province)
    const undefinedFilter = _.filter(result, (e) => {
      return e !== undefined
    })
    this.props.setTenStore(undefinedFilter)
    // console.log('y', ...this.props.tenStore)
  }
  isValidType(arrayObj, text) {
    let result = false
    arrayObj.map((obj, index) => {
      if (Object.values(obj).indexOf(text) > -1) {
        console.log(`has ${text}`)
        result = true
        id = index
      }
      return null // Fix warning issue
    })
    return result
  }

  scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)
  render() {
    // console.log(this.state)
    console.log(this.props.product)
    return (
      <Container>
        <SubContainer>
          <TopBackground>
            <TopContainer>
              <TopContent>
                {menus.map((obj, i) => (
                  <ButtonContainer key={obj.title}>
                    <Button
                      transparent
                      width="max"
                      style={{ marginRight: '15px' }}
                      onClick={() => this.handleSelect(obj.type)}>
                      {obj.title}
                    </Button>
                  </ButtonContainer>
                ))}
              </TopContent>
            </TopContainer>
          </TopBackground>
          <BottomColumnContainer>
            <BottomRowContainer>
              <BottomLeftFiller>
                <BottomLeftBackground>
                  <BottomLeftSquare />`
                </BottomLeftBackground>
              </BottomLeftFiller>
              <ThemesContainer>
                <SelectedContainer secondaryColor={this.props.theme.white} />
              </ThemesContainer>
              {this.state.type === 'all' ? (
                <ContentContainer>
                  {this.state.province_show.map((obj, index) => {
                    if (obj.provinceName) {
                      return <ContentSection key={index} data={obj} />
                    }
                  })}
                  <EmptyContentContainer />
                </ContentContainer>
              ) : this.props.regions.length > 0 ? (
                <ContentVerticalContainer>
                  {this.state.regions.map((obj, index) => {
                    return <ContentSection reg key={index} data={obj} />
                  })}
                </ContentVerticalContainer>
              ) : (
                <ContentContainer>
                  <NoContentSection />
                </ContentContainer>
              )}
            </BottomRowContainer>
          </BottomColumnContainer>
        </SubContainer>
      </Container>
    )
  }
}
const mapStateToProps = (state) => ({
  provinces: state.product.provinces,
  tenStore: state.product.tenStore,
  regions: state.product.regions,
})
// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    setProvince: dispatch.product.setProductData,
    setTenStore: dispatch.product.setTenStore,
    setRegions: dispatch.product.setRegions,
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(ProductPage)))
