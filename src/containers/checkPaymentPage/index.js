import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTheme } from 'styled-components'
import { withRouter } from 'react-router-dom'
// import LoadingScreen from 'react-loading-screen'
// import Frame from '../../components/frameLayout'
import Condition from './condition'

class CheckPaymentPage extends Component {
  state = {
    text: 'Loading',
  }

  render() {
    return <Condition />
  }
}
const mapStateToProps = (state) => ({

})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = () => {
  return {
    
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(CheckPaymentPage)))
