import React, { Component } from 'react'
import { connect } from 'react-redux'
import ChangePayment from './changePayment'
import GetQrcode from './getQrcode'
import LoadingRedirectPage from '../loadingRedirectPage'

class Condition extends Component {
  _isUpdated = false

  state = {
    isLineUserId: false,
    isQRCode: false,
  }
  componentDidMount = async () => {
    return
  }

  componentDidUpdate = async () => {
    if (!this._isUpdated && !this.state.isLineUserId && this.props.user.line.userId !== '') {
      if (this.props.user.chattyUserId && this.props.user.chattyUserId !== '') {
        // console.log('Address : check chattyUserId')
        // console.log(`chattyUserId:  ${this.props.user.chattyUserId} !== ''`)
        this._isUpdated = true
        this.setState({ isLineUserId: true })
        await this.props.getOrder(this.props.user)
        console.log(this.props.order)
        console.log(this.props.checkPayment.check)

        if (this.props.checkPayment.check === 'qr' || this.props.checkPayment.check === 'bank') {
          this.setState({ isQRCode: true })
          console.log(this.state)
        } else if (this.props.checkPayment.check === 'credit_card') {
          this.setState({ isQRCode: true })
          console.log(this.state)
        } else {
          this.setState({ isQRCode: false })
          console.log(this.state.isQRCode)
        }

        // if (this.checkPayment.output === true){
        //this.setState({ isQRCode: true })
        // } else {
        //this.setState({ isQRCode: false })
        // }
      }
    }
  }

  render() {
    return (
      <div>
        {this.state.isQRCode ? (
          this.props.checkPayment.check === 'qr' ? (
            <GetQrcode />
          ) : (
            <ChangePayment />
          )
        ) : (
          <LoadingRedirectPage />
        )}
        {/* {this.props.checkPayment.check === 'bank' ? <ChangePayment /> : this.props.checkPayment.check === 'qr' ?<GetQrcode />:null} */}
        ,{/* {this.props.checkPayment.check === 'qr' ? <GetQrcode /> : <LoadingRedirectPage />} */}
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  user: state.user,
  checkPayment: state.checkPayment,
  output: state.checkPayment.output,
})

const mapDispatchToProps = (dispatch) => {
  return {
    getOrder: dispatch.checkPayment.getOrder,
    setOutputState: dispatch.checkPayment.setOutputState,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Condition)
