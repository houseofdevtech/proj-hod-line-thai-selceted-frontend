import { withRouter } from 'react-router-dom'
import React, { Component } from 'react'
import styled, { withTheme } from 'styled-components'
// import { connect } from 'react-redux'
// import { Link } from 'react-router-dom'
import Frame from '../../components/frameLayout'
// import Button from '../../components/button'

// const StyleButton = styled.button`
//   border-radius: 15px;
//   border: none;
//   background-color: #f0218b;

//   text-align: center;
//   margin-top: 50%;
// `
// const StyleInButton = styled.button`
//   border: none;
//   background-color: #f0218b;
// `

const StyleLink = styled.div`
  
    color: #ec0980;
  
`
const StyleFrame = styled.div`
                    display: flex;
                    text-align: center;
                    width: 100%;
                    color:#464546

                    flex-direction: column;
//                   `
// const liff = window.liff

// function handleClick() {
//   liff.closeWindow()
// }
// const Link = styled.div`

// `

class ChangePayment extends Component {
  gotoPayment() {
    console.log('gotoPayment')
    // console.log(this.props.location.state)
    this.props.history.push('/address', { 
      ...this.props.location.state,
      from: this.props.location.pathname, 
      to: '/payment',
      shop_id: this.props.location.state.shop_id ,  
    })
  }

  render() {
    return (
      <Frame
        primaryColor={this.props.theme.secondary}
        secondaryColor={'#fff'}
        topHeight="20vh"
        bodyHeight="50vh">
        {' '}
        <StyleFrame>
          ขออภัย ท่านไม่สามารถชำระเงินด้วยวิธีนี้ได้ <br></br>
          เนื่องจากมีการเปลี่ยนแปลงวิธีการชำระเงิน <br></br>
          กรุณาคลิกที่ลิงค์ด้านล่าง เพื่อทำการชำระเงินใหม่อีกครั้ง <br></br>
          <br></br> <br></br>
          <StyleLink>
            {/* <Link to="/address">
              <u>ชำระเงิน</u>
            </Link> */}
            <u onClick={() => this.gotoPayment()}>ชำระเงิน</u>
          </StyleLink>
        </StyleFrame>
      </Frame>
    )
  }
}
export default withRouter(withTheme(ChangePayment))
