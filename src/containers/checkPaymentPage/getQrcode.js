import React, { Component } from 'react'
import  { withTheme } from 'styled-components'
import { connect } from 'react-redux'
// import Frame from '../../components/frameLayout'
import LoadingRedirectPage from '../loadingRedirectPage'
// const Container = styled.div`
//   width: 100%;
//   margin-top: 50%;
//   text-align: center;
// `
// const Container2 = styled.div`
//   width: 100%;
//   text-align: center;
// `

class GetQrcode extends Component {
  _isUpdate = false
  state = {
    isLoad: false,
  }

  componentDidMount = async () => {
    // this._isUpdate = true
    // this.props.sendQrcode(this.props.user)
    
    // get and send qrcode ... respectively 
    this.props.getQrcode(this.props.user)
  }

  render() {
    return (
      <LoadingRedirectPage />
      // <Frame
      //   primaryColor={this.props.theme.secondary}
      //   secondaryColor={'#fff'}
      //   topHeight="20vh"
      //   bodyHeight="50vh">
      //   {/* <Container2>ระบบกำลังดาว์นโหลด QR Code ของคุณ */}
      //    <LoadingRedirectPage/>
      //   {/* <Container>Loading…</Container> */}
      //   {/* </Container2> */}
      // </Frame>
    )
  }
}
const mapStateToProps = (state) => ({
  check: state.checkPayment,
  user: state.user,
})
const mapDispatchToProps = (dispatch) => {
  return {
    getQrcode: dispatch.qrcode.getQrcode,
    sendQrcode: dispatch.qrcode.sendQrcode,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(GetQrcode))
