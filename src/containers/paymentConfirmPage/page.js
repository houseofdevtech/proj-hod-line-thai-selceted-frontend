import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import styled, { css, withTheme } from 'styled-components'
import { lighten } from 'polished'

import Frame from '../../components/frameLayout'
import ListCard from '../../components/paymentListCard'
import Button from '../../components/button'
import FooterButton from '../../components/footerButton'

// import TopBackground from '../../images/topPayment.png'
import TopBackground from '../../images/passioneBanner.png'
import EditIcon from '../../images/edit.svg'

import CreditCardModal from './modal/modalCreditCard'

const TopContent = styled.div`
  position: relative;
  display: block;
  // width: 100vw;
  width: inherit;
  flex: 1 100%;
  align-self: flex-start;
  z-index: 20;
  color: ${(props) => props.theme.textPurple};
  img {
    position: relative;
    object-fit: cover;
    width: 100%;
    height: 20vw;
  }
  div {
    height: 35vw;
  }

  @media only screen and (min-width: 451px) {
    width: inherit;
    img {
      position: relative;
      object-fit: cover;
      width: 100%;
      height: 100%;
    }
  }
`
const ButtonContainerPosition = styled.div`
  ${(props) => {
    if (props.hasNext) {
      return css`
        margin-right: 3vw;
        flex: 1;
        @media only screen and (min-width: 451px) {
          margin-right: 5px;
        }
      `
    }
    return css`
      flex: 1;
    `
  }}
`
const TopUserContent = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1 100%;
  align-self: flex-start;
  z-index: 20;
  color: ${(props) => props.theme.textPurpleLight};
  padding: 0 4vw 4vw;
  div {
    z-index: 30;
    postion: absolute;
    font-size: 0.8rem;
    // margin: 0 2vw 0 2vw;
  }
`

const TopUserName = styled.div`
  align-self: center;
  padding-left: 40px;
  color: ${(props) => props.theme.textPurple};
`

const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 1;
`

const toAddress = (props) => {
  props.history.push({ pathname: '/address', state: { from: props.location.pathname } })
}

const Top = (props) => {
  // console.log(`Top :: ${JSON.stringify(props.user)}`)
  if (props.user !== null) {
    return (
      <div>
        <TopContent>
          <img alt="top-background" src={TopBackground} />
        </TopContent>
        <TopUserContent>
          <TopContainer>
            <div />
            <TopUserName>{`คุณ ${props.user.name} `}</TopUserName>
            <EditButton
              marginRight="5px"
              style={{ position: 'absolute' }}
              onClick={() => toAddress(props.history)}
            />
          </TopContainer>

          <div>ที่อยู่จัดส่ง :</div>
          <div>{props.user.full_address}</div>
        </TopUserContent>
      </div>
    )
  } else {
    return <div></div>
  }
}

const Section = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  flex: 1;
  width: inherit;
  padding: 0 2vw;

  @media only screen and (min-width: 451px) {
    padding: 0 10px;
  }
`
const SendContainer = styled.div`
  display: flex;
  flex-direction: column;
  // justify-content: flex-start;
  // align-items: flex-start;
  // position: relative;
  width: 100%;
  flex: 1;
  // padding-left: 2vw;
  margin-bottom: 2vw;

  @media only screen and (min-width: 451px) {
    margin-bottom: 5px;
  }
`

const ButtonContainer = styled.div`
  display: flex;
  flex: 1;
  width: 90vw;

  @media only screen and (min-width: 451px) {
    width: 100%;
  }
`

const Topic = styled.div`
  font-size: 0.85rem;
  font-weight: 100;
  color: ${(props) => props.theme.textPurple};
  margin-bottom: 2vw;

  @media only screen and (min-width: 451px) {
    margin-bottom: 5px;
  }
`

const Remark = styled.div`
  align-self: flex-end;
  font-size: 0.7rem;
  color: ${(props) => lighten(0.4, props.theme.textPurple)};
  margin-top: 2vw;
  @media only screen and (min-width: 451px) {
    margin-top: 5px;
  }
`
const RemarkEmpty = styled.div`
  align-self: flex-end;
  font-size: 0.7rem;
  color: ${(props) => lighten(0.4, props.theme.textPurple)};
  margin: 3vw 0px;
  @media only screen and (min-width: 451px) {
    margin: 12px 0px;
  }
`

const SendingMethodButtons = (props) => {
  return (
    <SendContainer>
      <Topic>วิธีการจัดส่ง</Topic>
      {/* {console.log(`shipping method :: ${JSON.stringify(props.data)}/`)} */}
      {props.data !== null && props.data.shipping_price ? (
        <ButtonContainer>
          {'std' in props.data.shipping_price && (
            <ButtonContainerPosition hasNext={'ems' in props.data.shipping_price}>
              <Button
                payment
                // Change background color when selected and refer by state (pass on props.state)
                selected={props.state.shipping_type === 'std' ? true : undefined}
                // If another field button make "Spacing" among buttons

                height={40}
                onClick={() => props.onChange('shipping_type', 'std')}>
                ลงทะเบียน
              </Button>
            </ButtonContainerPosition>
          )}
          {'ems' in props.data.shipping_price && (
            <ButtonContainerPosition hasNext={'kerry' in props.data.shipping_price}>
              <Button
                payment
                selected={props.state.shipping_type === 'ems' ? true : undefined}
                height={40}
                onClick={() => props.onChange('shipping_type', 'ems')}>
                EMS
              </Button>
            </ButtonContainerPosition>
          )}
          {'kerry' in props.data.shipping_price && (
            <ButtonContainerPosition hasNext={'messenger' in props.data.shipping_price}>
              <Button
                payment
                selected={props.state.shipping_type === 'kerry' ? true : undefined}
                height={40}
                onClick={() => props.onChange('shipping_type', 'kerry')}>
                Kerry
              </Button>
            </ButtonContainerPosition>
          )}
          {'messenger' in props.data.shipping_price && (
            <ButtonContainerPosition hasNext={false}>
              <Button
                payment
                selected={props.state.shipping_type === 'messenger' ? true : undefined}
                height={40}
                onClick={() => props.onChange('shipping_type', 'messenger')}>
                ส่งด่วนทันที
              </Button>
            </ButtonContainerPosition>
          )}
        </ButtonContainer>
      ) : null}
      {/* Switch case for Shipping remark */}
      {(() => {
        switch (props.state.shipping_type) {
          case 'std':
            return <Remark>{props.remark.std}</Remark>
          case 'ems':
            return <Remark>{props.remark.ems}</Remark>
          case 'kerry':
            return <Remark>{props.remark.kerry}</Remark>
          case 'messenger':
            return <Remark>{props.remark.messenger}</Remark>
          default:
            return <RemarkEmpty />
        }
      })()}
    </SendContainer>
  )
}
// ***** Template of Payment List
// const PaymentMethodList = [
//   { label: 'QR PromptPay', match: 'qr_code', active: false },
//   { label: 'โอนเงิน', match: 'bank', active: false },
//   { label: 'CreditCard', match: 'credit_card', active: false },
// ]

const ButtonContainerPayment = styled.div`
  display: flex;
  flex: 1;
  width: 90vw;
  margin-bottom: 2vw;

  @media only screen and (min-width: 451px) {
    width: 100%;
  }
`

const PaymentMethodButtons = (props) => {
  const paymentList = []
  if (props.data !== null) {
    // console.log(`paybutton :: ${JSON.stringify(props.data.payment_options)}`)
    props.data.payment_options.qr_code &&
      paymentList.push({ label: 'QR PromptPay', type: 'qr', active: false })
    props.data.payment_options.bank &&
      paymentList.push({ label: 'โอนเงิน', type: 'bank', active: false })
    props.data.payment_options.credit_card &&
      paymentList.push({ label: 'Credit', type: 'credit_card', active: false })
    // paymentList.push({ label: 'QR PromptPay', type: "qr", active: false })
    // paymentList.push({ label: 'โอนเงิน', type: "bank", active: false })
  }
  return (
    <SendContainer>
      <Topic>วิธีการชำระเงิน</Topic>
      {/* {console.log(`shipping method :: ${JSON.stringify(props.data)}/`)} */}
      {props.data !== null ? (
        <SendContainer>
          {(() => {
            if (props.data.length === 4) {
              const firstTwo = props.data.slice(0, 2)
              const lastTwo = props.data.slice(2, 4)
              console.log(`first three : ${JSON.stringify(firstTwo)}`)
              console.log(`last three : ${JSON.stringify(lastTwo)}`)

              return (
                <div>
                  {showButtons(firstTwo, props.state, props.onChange)}
                  {showButtons(lastTwo, props.state, props.onChange)}
                </div>
              )
            } else {
              console.log(`render buttons`)
              //const render = renderButtons(paymentList, [], props)
              const render = renderButtons(paymentList, [], props)
              //console.log(render)
              return render
            }
          })()}
        </SendContainer>
      ) : null}
    </SendContainer>
  )
}

const renderButtons = (array, result, props) => {
  // const first = array.slice(0, 3)
  // const last = array.slice(3, array.length)

  // return (
  //   <div>
  //     {showButtons(first, state, onChange)}
  //     <div/>
  //     {showButtons(last, state, onChange)}
  //   </div>
  // )

  return (
    <div>
      {(() => {
        if (array.length <= 3) {
          result.push(showButtons(array, props.state, props.onChange))
          //console.log(result)
          return result
        } else {
          const first = array.slice(0, 3)
          const last = array.slice(3, array.length)
          result.push(showButtons(first, props.state, props.onChange))
          return renderButtons(last, result, props)
        }
      })()}
    </div>
  )
}

const showButtons = (array, state, onChange) => {
  // console.log(`showButtons`)
  console.log(`showButtons array : ${JSON.stringify(array)}`)

  return (
    <ButtonContainerPayment key={array.length}>
      {array.map((obj, index) => {
        return (
          <ButtonContainerPosition hasNext={index < array.length - 1}>
            <Button
              key={index}
              payment
              selected={state.payment_type === obj.type ? true : undefined}
              fontSize={obj.label.length > 10 ? '0.65' : '0.75'}
              onClick={() => onChange('payment_type', obj.type)}
              height={40}>
              {obj.label}
            </Button>
          </ButtonContainerPosition>
        )
      })}
    </ButtonContainerPayment>
  )
  // {console.log(`length :: ${obj.label.length}`)}
  // : obj.label.length > 10
  //               ? { fontSize: '0.6rem' }
  // selected={state.payment_type === obj.bank_name ? true : undefined}
  // onClick={() => onChange('payment_type', obj.bank_name)}
  // {obj.bank_name}
}

const EditPane = styled.div`
  disply: inline-block;
  color: ${(props) => props.theme.textPurple};
  // justify-content: center;
  // align-items: flex-end;
  vertical-align: middle;
  margin-right: ${(props) => props.mr};
  cursor: pointer;
  img {
    vertical-align: middle;
    margin-right: 3px;
    margin-bottom: 5px;
    width: 15px;
    height: 15px;
    align-self: center;
  }
  span {
    display: inline-block;
    vertical-align: middle;
  }
`

const EditButton = (props) => {
  return (
    <EditPane mr={props.marginRight} onClick={() => props.onClick()}>
      <img alt="edit-icon" src={EditIcon} />
      <span>แก้ไข</span>
    </EditPane>
  )
}

const ItemListFrame = styled.div`
  margin: 0 0 2vw 0;
  padding: 0 2vw 1vw;
  // width: 86vw;
  border: ${(props) => props.theme.primary} solid 2px;
  border-radius: 5px;
  flex: 1;
  // height: 50vh;
  div {
    color: ${(props) => props.theme.textGrey} 
    font-size: 0.75rem;
  }
  p {
    color: ${(props) => props.theme.textPurple};
    margin:0;
  }
  @media only screen and (min-width: 451px) {
    margin: 0 0 10px 0;
    padding: 0 10px 10px;
    div{
      font-size: 0.85rem;
    }
  }
`

const FlexSBSpace = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 2vw 1vw 0vw;

  @media only screen and (min-width: 451px) {
    margin: 10px 10px 0;
  }
`
const FlexSBBottom = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0vw 1vw 2vw;
`
const FlexSBItemPay = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 1.5vw 1vw 0vw;

  @media only screen and (min-width: 451px) {
    margin: 10px 10px 10px;
  }
`

const DividerTop = styled.hr`
  border: none;
  border-top: 1.5px solid ${(props) => props.theme.primary};
  box-shadow: 0 0px 2px 0px rgba(0, 0, 0, 0.1);

  margin: 0px 0px 2vw;

  @media only screen and (min-width: 451px) {
    margin: 0 0 10px;
  }
`
const DividerBottom = styled.hr`
  border: none;
  border-top: 1.5px solid ${(props) => props.theme.primary};
  box-shadow: 0 0px 2px 0px rgba(0, 0, 0, 0.1);

  margin: 2vw 0px 0vw;

  @media only screen and (min-width: 451px) {
    margin: 10px 0 0;
  }
`

const ItemDivider = styled.hr`
  border: none;
  border-top: 1px solid ${(props) => props.theme.textGrey};
  margin: 6px 0px;
  // box-shadow: 0 0px 2px 0px rgba(0, 0, 0, 0.1);
`

// const toEdit = (props) => {
//   props.history.push({
//     pathname: '/edit',
//     state: {
//       from: props.location.pathname,
//       all_product_id: props.payment.allProductId,
//       // this is get shop_id
//       ...props.location.state,
//     },
//   })
// }

const Order = (props) => {
  let shipping_price = 0
  return (
    <ItemListFrame>
      <FlexSBItemPay>
        <p>รายการที่ต้องชำระเงิน</p>
        {/* <EditButton onClick={() => toEdit(props.editData)} /> */}
      </FlexSBItemPay>
      <DividerTop />

      {props.data !== null && props.data.data_order.order_items !== null ? (
        <Section>
          {props.data.data_order.order_items.map((obj) => {
            let Cards = []
            if (obj.product_items) {
              obj.product_items.map((item, index) => {
                //console.log(`List card ${obj.product_name}& ${item.color}`)
                Cards.push(
                  <div key={index}>
                    <ListCard
                      key={index}
                      name={obj.product_name}
                      size={item.size}
                      color={item.color}
                      quantity={item.qty}
                      price={item.price.toLocaleString('th')}
                    />
                    {index < obj.product_items.length - 1 ? <ItemDivider /> : null}
                  </div>
                )
                return null
              })
              return Cards
            }
            return null
          })}
        </Section>
      ) : (
        <div></div>
      )}
      <DividerBottom />
      <FlexSBSpace>
        <div>ค่าจัดส่ง</div>
        {/* <div> ฿ {props.data ? props.data : null}</div> */}
        {(() => {
          switch (props.state.shipping_type) {
            case 'std': {
              shipping_price = props.data.data_order.shipping_price.std
              return <div>{shipping_price} THB</div>
            }
            case 'ems': {
              shipping_price = props.data.data_order.shipping_price.ems
              return <div>{shipping_price} THB</div>
            }
            case 'kerry': {
              shipping_price = props.data.data_order.shipping_price.kerry
              return <div>{shipping_price} THB</div>
            }
            case 'messenger': {
              shipping_price = props.data.data_order.shipping_price.messenger
              return <div>{shipping_price} THB</div>
            }
            default:
              return <div>- THB</div>
          }
        })()}
      </FlexSBSpace>
      <FlexSBBottom>
        <div>ยอดชำระ</div>
        <div>
          {props.data ? (props.data.sum_price + shipping_price).toLocaleString('th') : null} THB
        </div>
      </FlexSBBottom>
    </ItemListFrame>
  )
}
// {console.log(`index: length :: ${index} - ${obj.product_items.length} `)}
// {console.log(`sum :: ${JSON.stringify(props.data.sum)}`)}

// const MockData = {
//   name: 'นางสาวสวย ช้อปปิ้งเก่ง',
//   address: '123/456 คอนโดทวีสุข ตึกเอ แขวงบางซื่อ เขตบางซื่อ กรุงเทพฯ 10800',
// }
// const MockShipping = {
//   shipping_price: { std: 0, ems: 0 },
// }
// const MockBanking = [
//   { bank_name: 'bank1' },
//   { bank_name: 'bank2' },
//   { bank_name: 'bank3' },
//   { bank_name: 'bank4' },
//   { bank_name: 'bank5' },
//   { bank_name: 'bank6' },
//   { bank_name: 'bank6' },
// ]

const ShippingRemark = {
  std: 'ระยะเวลาในการจัดส่ง 4-7 วัน',
  ems: 'ระยะเวลาในการจัดส่ง 3-5 วัน',
  kerry: 'ระยะเวลาในการจัดส่ง 3-5 วัน',
  messenger: 'เมื่อซื้อตั้งแต่ 500 บาท ส่งฟรีภายในสองชั่วโมง เฉพาะในเขตเมืองระยองเท่านั้น',
}

class PaymentConfirmPage extends Component {
  _isUpdated = false
  _isBillLoaded = false
  total = null

  state = {
    isActiveButton: false,
    shipping_type: null,
    payment_type: null,
    total_payment: null,

    isEmptyStock: false,
    reRenderTextMsg: null,

    isShowModal: false,
    isCreditCard: false,
  }

  componentDidMount() {
    // console.log(`paymentDidMount user_id :: ${this.props.user.chattyUserId}`)
    // console.log(`paymentDidMount order_id :: ${JSON.stringify(this.props.location.state.shop_id)}`)
    // console.log(`paymentDidMount order :: ${this.props.payment.order}`)
    this.props.setBills({ user_id: this.props.user.chattyUserId })

    if (this.props.location.state && 'shop_id' in this.props.location.state) {
      this.props.setBills({ shop_id: Number(this.props.location.state.shop_id) })
    }

    // this.props.setBills({ payment_type: 'bank' })
  }

  componentDidUpdate() {
    if (this._isUpdated) {
      // console.log(this.props.location.state)
      // If payment_type is credit_card
      this.state.payment_type === 'credit_card' && this.setState({ isCreditCard: true })
      // If payment_type is not null then set Body pay_by_type : [payment_type]
      this.state.payment_type !== null &&
        this.props.setBills({ pay_by_type: this.state.payment_type }) &&
        this.props.setBills({ total: this.props.payment.order.sum_price })

      // If shipping_type is not null then set Body shipping_type_id : [shipping_type(code)]
      this.state.shipping_type !== null &&
        this.props.setBills({ shipping_type_id: this.translateShipping(this.state) })

      // If there is order data from api then set Body order_id: [order_id]
      this.props.payment.order !== undefined &&
        this.props.setBills({ order_id: this.props.payment.order.data_order.order_id }) &&
        this.props.setBills({ bill_id: this.props.location.state.bill_id })

      // Set Sum + Shipping price => this.state.total_payment
      const shipping_type = this.state.shipping_type
      let shipping_price
      switch (shipping_type) {
        case 'std': {
          shipping_price = this.props.payment.order.data_order.shipping_price.std
          break
        }
        case 'ems': {
          shipping_price = this.props.payment.order.data_order.shipping_price.ems
          break
        }
        case 'kerry': {
          shipping_price = this.props.payment.order.data_order.shipping_price.kerry
          break
        }
        case 'messenger': {
          shipping_price = this.props.payment.order.data_order.shipping_price.messenger
          break
        }
        default:
          shipping_price = 0
      }
      if (this.props.payment !== null) {
        const total_price = this.props.payment.sum_price + shipping_price
        this.handleSetTotal(total_price)
      }
    }
    if (
      this._isUpdated &&
      this.state.payment_type !== null &&
      this.state.shipping_type !== null &&
      !this.state.isEmptyStock
    ) {
      this.setState({ isActiveButton: true })
    }
    this._isUpdated = false

    // isEmptyStock is checked when getOrder in payment(Redux)
    // when getOrder show zero stock : 'qty === 0' it will triggers reRender for Empty stock
    if (!this._isBillLoaded && 'order' in this.props.payment) {
      this._isBillLoaded = true
      const { order } = this.props.payment
      // console.log(order.shipping_type_id)
      // console.log(order.pay_by_type)
      this._isUpdated = true
      this.setState({
        shipping_type: this.translateShippingNum2String(order.shipping_type_id),
        payment_type: order.pay_by_type,
        // isActiveButton: true,
      })
    }

    if ('isEmptyStockTrigger' in this.props.payment && this.props.payment.isEmptyStockTrigger) {
      this.props.setPayment({ isEmptyStockTrigger: false })
      this.props.setBills({ reRender: true })
    }

    // reRender is checked when
    if (this.props.bills.reRender) {
      this.props.setBills({ reRender: false })
      this.setState({
        reRenderTextMsg: 'สินค้าที่คุณเลือกไม่มีในสต๊อคแล้ว ... กรุณาเลือกสินค้าใหม่',
        isActiveButton: false,
        isEmptyStock: true,
      })
      this.forceUpdate()
    }
  }

  translateShipping = (state) => {
    switch (state.shipping_type) {
      case 'ems':
        return 1
      case 'kerry':
        return 2
      case 'messenger':
        return 3
      default:
        return 0
    }
  }

  translateShippingNum2String = (num) => {
    switch (num) {
      case 1:
        return 'ems'
      case 2:
        return 'kerry'
      case 3:
        return 'messenger'
      default:
        return 'std'
    }
  }

  translatePayment = (state, props) => {
    // console.log(`payment (payment):: ${JSON.stringify(props.payment.bank)}`)
    // console.log(`bank_name :: ${state.payment_type}`)
    // const result = this.props.payment.bank.filter((obj) => obj.bank_name === state.payment_type)
    // console.log(result[0].bank_id)
    // return result[0].bank_id
    return 0
    // return (state)
  }

  handleSetTotal = (value) => {
    console.log(value)
    // this.total = value
    this.setState({ total_payment: value })
  }

  handleChange = (field, value) => {
    this._isUpdated = true

    const isPaymentCreditCard = field === 'payment_type' && value === 'credit_card'
    if (isPaymentCreditCard) {
      this.setState({ isCreditCard: true, [field]: value })
    } else {
      this.setState({ isCreditCard: false, [field]: value })
    }
    // this.setState({ [field]: value })
  }

  handleSetActive = (booleanValue) => {
    this.setState({ isActiveButton: booleanValue })
  }

  handleShowInActiveButton = () => {
    this.setState({ isShowModal: true })
  }

  handlePrimaryButton = () => {
    if (this.state.isCreditCard) {
      // Redirect Information Need
      // amount : sum + shipping price
      // product_name : [product id]-[product item id]
      // tracking_id : *** wait tracking_id from api ***
      const product_id = this.props.payment.order.data_order.order_items[0].product_id
      const product_item_id = this.props.payment.order.data_order.order_items[0].product_items.map(
        (obj) => {
          return obj.sku_id
        }
      )
      const product_name = `${product_id}-${product_item_id}`
      // const summary =
      //   parseInt(
      //     this.props.payment.order.order_items[0].product_items.map((obj) => {
      //       return obj.sum
      //     })
      //   ) + parseInt(this.props.payment.order.shipping_price.kerry)

      const redirectPayload = {
        amount: this.state.total_payment,
        product_name: product_name,
      }
      console.log(JSON.stringify(redirectPayload))
      this.props.putBillsRedirect(redirectPayload)
    } else {
      this.props.putBills()
    }
  }

  Footer = () => {
    return (
      <FooterButton
        primaryColor={this.props.theme.tertiary}
        backIconColor={this.props.theme.primary}
        handleBack={() => this.props.handleBack(this.props.history)}
        handlePrimaryButton={() => this.handlePrimaryButton()}
        handleInactiveButton={() => this.handleShowInActiveButton()}
        isActive={this.state.isActiveButton}
        isMessage={this.state.isEmptyStock}
        message={this.state.reRenderTextMsg}
        labelPrimary="ชำระเงิน"
      />
    )
  }

  render() {
    console.log(`payment (payment):: ${JSON.stringify(this.props.payment)}`)
    console.log(`payment (bills):: ${JSON.stringify(this.props.bills)}`)
    console.log(`payment (state):: ${JSON.stringify(this.state)}`)
    return (
      <div>
        <Frame
          primaryColor={this.props.theme.tertiary}
          secondaryColor={this.props.theme.white}
          topContent={
            <Top
              user={this.props.payment.order ? this.props.payment.order.data_user : null}
              history={this.props}
            />
          }
          bottomContent={<this.Footer />}
          paddingBottom={true}
          topHeight="max-content"
          bodyHeight="max-content">
          <Section>
            <SendingMethodButtons
              data={this.props.payment.order ? this.props.payment.order.data_order : null}
              state={this.state}
              remark={ShippingRemark}
              onChange={this.handleChange}
            />
            <Order
              data={this.props.payment.order ? this.props.payment.order : null}
              state={this.state}
              editData={this.props}
              handleSetTotal={this.handleSetTotal}
            />
            <PaymentMethodButtons
              data={this.props.payment.order ? this.props.payment.order.data_order : null}
              state={this.state}
              onChange={this.handleChange}
            />
          </Section>
        </Frame>

        <CreditCardModal
          isOpen={this.state.isShowModal}
          onCallBack={() => {
            console.log('onCallBack')
          }}
          onCloseModal={(data) => {
            this.setState({ isShowModal: data })
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  payment: state.paymentConfirm,
  bills: state.bills,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    getAddress: dispatch.address.getAddress,
    setPayment: dispatch.paymentConfirm.setPayment,

    setBills: dispatch.bills.setBills,
    putBills: dispatch.bills.putBills,
    putBillsRedirect: dispatch.bills.putBillsRedirect,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(withRouter(PaymentConfirmPage)))
