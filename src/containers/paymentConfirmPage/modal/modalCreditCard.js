import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { customModalStyle, mobileModalStyle } from './customModalStyle'
import { Desktop, Mobile } from '../../../utils/displayResponsive'

// import axios from 'axios'

import ErrorBoundary from '../../../components/errorBoundary'
import CustomModal from '../../../components/customModal'

import CloseIcon from '../../../images/ic-cancelbtn-gray-24-px.svg'

// var __html = require(`./payment.html`)
// var template = { __html: __html }

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  padding: 3vw 4vw;
  @media only screen and (min-width: 451px) {
    padding: 15px 20px;
  }
`

// const FormKbank = () => {
//   return (
//     <form method="POST" action="/checkout">
//       <script
//         type="text/javascript"
//         src="https://dev-kpaymentgateway.kasikornbank.com/ui/v2/kpayment.min.js"
//         data-mid="451001080988001"
//         data-apikey="pkey_test_20352Xj5Kmq3cn4mHtk55XXtUviVWzxDnb6PR"
//         data-amount="74.00"
//         data-payment-methods="card"></script>
//         <input type="submit" value="Pay now" />
//     </form>
//   )
// }

class ModalCreditCardKbank extends Component {
  state = {
    goRedirect: false,
  }

  componentDidMount() {}

  onCloseModal = () => {
    console.log('closeModal')
    this.props.onCloseModal(false)
  }

  onCallBack = () => {
    console.log('onCallBack - Modal')
    this.props.onCallBack()
  }

  // onSubmit = (e) => {
  //   e.preventDefault()
  //   const data = {
  //     mid: "451001080988001",
  //     apikey: "pkey_test_20352Xj5Kmq3cn4mHtk55XXtUviVWzxDnb6PR",
  //     amount:"74.00",
  //     'payment-methods':"card",
  //   }

  //   axios({
  //     method: 'POST',
  //     url: 'https://dev-kpaymentgateway.kasikornbank.com/',
  //     data: data,
  //   }).then((response) => {
  //     if (response.data.status === 'success') {
  //       alert('Message Sent.')
  //       this.resetForm()
  //     } else if (response.data.status === 'fail') {
  //       alert('Message failed to send.')
  //     }
  //   })

  //   this.setState({ goRedirect: true })
  // }

  renderContent = () => {
    // const { onClose, images } = this.props
    // const { index } = this.state
    // console.log(this.state.index)
    // {/* <form method="POST" action={() => this.props.history.push('/checkout')}> */}
    return (
      <ErrorBoundary>
        <Mobile>
          <CustomModal isOpen={this.props.isOpen} customStyle={mobileModalStyle}>
            <Container>
              <img
                style={{ position: 'absolute', right: '10px', top: '10px' }}
                src={CloseIcon}
                alt="close-icon"
                onClick={this.onCloseModal}
              />
              <div
                style={{
                  width: '100%',
                  height: '100%',
                  textAlign: 'center',
                  marginTop: '16%',
                }}>
                กรุณาเลือกวิธีการชำระเงิน
              </div>
            </Container>
          </CustomModal>
        </Mobile>
        <Desktop>
          <CustomModal isOpen={this.props.isOpen} customStyle={customModalStyle}>
            <Container>
              <img
                style={{ position: 'absolute', right: '10px', top: '10px' }}
                src={CloseIcon}
                alt="close-icon"
                onClick={this.onCloseModal}
              />
              <div style={{ width: '100%', height: '100%', textAlign: 'center' }}>
                กรุณาเลือกวิธีการชำระเงิน
              </div>
            </Container>
          </CustomModal>
        </Desktop>
      </ErrorBoundary>
    )
  }

  render() {
    return <ErrorBoundary>{this.renderContent()}</ErrorBoundary>
  }
}

ModalCreditCardKbank.propstype = {
  onCloseModal: PropTypes.func.isRequired,
  onCallBack: PropTypes.func.isRequired,
}

export default withRouter(ModalCreditCardKbank)
