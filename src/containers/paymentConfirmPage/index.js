import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import PaymentPage from './page'
// import AddressPage from '../addressPage'
import LoadingScreen from '../../views/loadingPage'

import { isValidPaymentQS } from '../../utils/assist'

class PaymentConfirmLanding extends Component {
  // _isUpdated = false

  state = {}

  componentDidMount = async () => {
    // 3 is cartId must pass from address Page or QueryString
    // preloadCart(Cart Id)
    // const cart = await preloadCart(3)
    // console.log(cart)
    // this.setState(cart[0])
    const qsObject = this.props.location.state
    console.log(qsObject)
    if (isValidPaymentQS(qsObject)) {
      console.log('Payment landing :: valid query String')
      const resAddress = await this.props.getAddress(this.props.user)
      const resOrder = await this.props.getBills(qsObject)
      // const resBank = await this.props.getBank(qsObject.shop_id)
      // console.log(`res.data :: ${JSON.stringify(res)}`)
      let res = false
      let log = []
      resAddress === false && log.push('Failed get Address')
      resOrder === false && log.push('Failed get Order or No Order')
      // resBank === false && log.push('Failed get Bank')
      // console.log(`resAddress :: ${resAddress}`)
      // console.log(`resOrder :: ${resOrder}`)
      // if (!resAddress || !resOrder || !resBank) {
      if (!resAddress || !resOrder) {
        console.log(log)
      }
      if (resAddress && resOrder ) {
        res = true
      }
      console.log('payment all get done')
      this.setState({ dataLoaded: res, errorMsg: log })
    } else {
      console.log(`incomplete queryString Do Something `)
    }
  }

  componentDidUpdate = async () => {
    if (this.props.payment.isUpdated) {
      console.log('paymentConfirm update :: set state dataloaded = false')
      this.setState({ dataLoaded: false })
      console.log('paymentConfirm update :: set redux isUpdated = false')
      this.props.setUpdated(false)
      const res = await this.props.getAddress(this.props.user)
      if (res) {
        console.log('paymentConfirm update :: set dataloaded = true (if have response)')
        this.setState({ dataLoaded: res })
      }
    }
  }

  // componentWillUnmount() {
  //   this.props.setAddress({})
  // }

  handleMockupFlow = () => {
    this.props.handleMockupFlow2Msg(this.props.qrcode, this.props.receipt)
  }

  isAddress = (props) => {
    return 'address' in props
  }

  render() {
    console.log(this.state)
    // console.log(`user State :: ${JSON.stringify(this.props.user)}`)
    console.log(`paymentConfirm State :: ${JSON.stringify(this.props.payment)}`)
    // isCompleteAddress is this redux state (paymentConfirm)
    // {this.props.payment.isCompleteAddress ? (
    return (
      <div>
        {this.state.dataLoaded === false ? (
          <LoadingScreen label={this.state.errorMsg} />
        ) : this.isAddress(this.props.payment) ? (
          <PaymentPage {...this.props} handleMockupFlow={this.handleMockupFlow} />
        ) : (
          <LoadingScreen label="Waiting User Information" />
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  payment: state.paymentConfirm,
  // receipt: state.receipt,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    getAddress: dispatch.paymentConfirm.getAddress,
    getBills: dispatch.paymentConfirm.getBills,
    getBank: dispatch.paymentConfirm.getBank,

    setAddress: dispatch.paymentConfirm.setAddress,
    setUpdated: dispatch.paymentConfirm.setUpdated,
    // isCompleteAddress: dispatch.payment.isCompleteAddress,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PaymentConfirmLanding))
