import React, { Component } from 'react'
import styled, { withTheme } from 'styled-components'
import Frame from '../../components/frameLayout'
import Button from '../../components/button'

const Body = styled.div`
  display:flex 
  flex-direction:column;
  font-size: 1.1rem;
  justify-content: center;
  align-items: center;
  text-align:center;
  height: 50vh;
  width: 100%;

`

const ButtonContainer = styled.div`
  width: 150px;
  height: 50px;
  margin-top: '10px';
`

const Spacing = styled.div`
  padding: 10px 10px;
  height: 100px;
`

class LandingEmptyOrder extends Component {
  render() {
    return (
      <Frame
        primaryColor={this.props.theme.tertiary}
        secondaryColor={this.props.theme.white}
        topContent={<Spacing />}
        paddingBottom={true}
        topHeight="max-content"
        bodyHeight="max-content">
        <Body>
          <div>กรุณาซื้อสินค้าใหม่</div>
          <ButtonContainer>
            <Button primary onClick={() => this.props.handleCloseApp()}>
              ปิดหน้าต่าง
            </Button>
          </ButtonContainer>
        </Body>
      </Frame>
    )
  }
}

export default withTheme(LandingEmptyOrder)
