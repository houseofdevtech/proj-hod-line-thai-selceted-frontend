import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import styled, { withTheme } from 'styled-components'
// import { isAndroid } from 'react-device-detect'

import Frame from '../../components/frameLayout'

import FooterButton from '../../components/footerButton'
import Section from './section'

// import TopBackground from '../../images/topPayment.png'
import TopBackground from '../../images/passioneBanner.png'
// import ButtonPane from '../../components/buttonPaneTest'

const TopContent = styled.div`
  position: relative;
  display: block;
  // width: 100vw;
  width: inherit;
  // height: 20vh;
  flex: 1 100%;
  align-self: flex-start;

  z-index: 20;
  color: ${(props) => props.theme.textPurple};
  img {
    position: relative;
    object-fit: cover;
    width: 100%;
    height: 20vw;
  }
  div {
    position: relative;
    display: flex;
    justify-content: center;
    padding: 1vh 0px;
    // top: 6.5em;
    // right: 4.5em;
    color: ${(props) => props.theme.textPurple};
    font-size: 0.8rem;
    font-weight: 500;
  }

  @media only screen and (min-width: 451px) {
    img {
      position: relative;
      object-fit: scale-down;
      width: 100%;
      height: 100%;
    }
  }
`
const Top = () => {
  return (
    <div>
      <TopContent>
        <img alt="top-background" src={TopBackground} />

        <div>โปรดกรอกชื่อและที่อยู่สำหรับจัดส่งสินค้า</div>
      </TopContent>
    </div>
  )
}

class LayoutAddressPage extends Component {
  state = {
    isActiveButton: false,
    isFocus: false,
    offsetFocus: 0,
  }

  handleSetFocus = (booleanValue) => {
    this.setState({ isFocus: booleanValue })
  }

  handleSetActive = (booleanValue) => {
    this.setState({ isActiveButton: booleanValue })
  }

  handlePrimaryButton = async () => {
    const res = await this.props.editAddress(this.props.address)
    if (res) {
      if (this.props.location.state && 'to' in this.props.location.state) {
        console.log(`state :: ${JSON.stringify(this.props.location.state)}`)
        this.props.setUpdated(true)
        this.props.history.goBack()
        // this.props.history.push({
        //   pathname: this.props.location.state.to,
        //   state: { ...this.props.location.state, from: this.props.location.pathname },
        // })
      } else if (this.props.history.location.state.from === '/mystatus') {
        this.props.setUpdated(true)
        this.props.history.push({
          pathname: '/mystatus',
          state: { ...this.props.location.state, from: this.props.location.pathname },
        })
      } else {
        console.log('push /payment')
        this.props.setUpdated(true)
        this.props.history.push({
          pathname: '/payment',
          state: { ...this.props.location.state, from: this.props.location.pathname },
        })
        // state: { from: this.props.location.pathname ,data: this.props.data}}
      }
    }
  }

  Footer = () => {
    return (
      <FooterButton
        primaryColor={this.props.theme.tertiary}
        backIconColor={this.props.theme.primary}
        handleBack={() => this.props.handleBack(this.props.history)}
        handlePrimaryButton={() => this.handlePrimaryButton()}
        isActive={this.state.isActiveButton}
        labelPrimary="บันทึกข้อมูล"
      />
    )
  }

  handleFocus = (e) => {
    // if (isAndroid) {
    const { top } = e.target.getBoundingClientRect()
    // const d = e.target.getBoundingClientRect()
    // console.log(`top:: ${top}` )
    const h = window.innerHeight
    console.log(`top: ${top} || window/2 : ${h / 2}`)
    //
    if (top > h / 2) {
      console.log('set margin ')
      this.setState({ isFocus: true, offsetFocus: top - h / 2 + 100 })
    }
    // }
  }

  handleBlur = (e) => {
    // console.log('Blur')
    if (this.state.isFocus) {
      this.setState({ isFocus: false })
    }
  }

  // ButtonTest = () => {
  //   return (
  //     <ButtonPane
  //       clickOne={() => this.handleSetFocus(false)}
  //       clickTwo={() => this.handleSetFocus(true)}
  //     />
  //   )
  // }

  render() {
    console.log(`addres page state :: ${JSON.stringify(this.state)}`)
    return (
      <div>
        <Frame
          primaryColor={this.props.theme.tertiary}
          secondaryColor={'#fff'}
          topContent={<Top />}
          bottomContent={<this.Footer />}
          topHeight="max-content"
          bodyHeight="70vh"
          paddingBottom={true}
          justifyContent="flex-end"
          heightDesktop="350px">
          <Section
            {...this.props}
            focus={this.state.isFocus}
            offset={this.state.offsetFocus}
            handleSetActive={this.handleSetActive}
            handleFocus={this.handleFocus}
            handleBlur={this.handleBlur}
          />
        </Frame>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  address: state.address,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    editAddress: dispatch.address.editAddress,
    setUpdated: dispatch.payment.setUpdated,
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(withRouter(LayoutAddressPage)))
