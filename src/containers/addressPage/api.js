import axios from 'axios'
import request from '../../utils/urlRequest'
// import qs from 'qs'

const CancelToken = axios.CancelToken
const source = CancelToken.source()

// import {get}  from '../service'
// import { post } from '../service'
// preloadAddress (route)
// getAddressfromZipcode (zipcode)


// API 05 - address_province (get)
export const preloadProvinces = async () => {
  console.log(`preload :: /address_province`)
  let result = null
  try {
    const res = await request
      .get('/api/address_province', {
        cancelToken: source.token,
      })
      .catch(function(thrown) {
        if (axios.isCancel(thrown)) {
          console.log('Request canceled', thrown.message)
        } else {
          // handle error
          console.log('ERROR Request canceled ', thrown.message)
        }
      })
    if (res) {
      // console.log(res.data)
      const cleanData = res.data.map((obj) => {
        // console.log(obj)
        return {
          ...obj,
          value: obj.province_id,
          label: obj.name_in_thai,
        }
      })
      // console.log(cleanData)
      result = cleanData
      // return cleanData
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
  return result
}

// API 06 - address_districts (get)
export const preloadDistricts = async (id) => {
  console.log(`preload /address_districts`)

  request.interceptors.request.use((request) => {
    console.log('Starting Request', request)
    return request
  })
  request.interceptors.response.use((response) => {
    console.log('Response:', response)
    return response
  })
  let result = null
  try {
    const res = await request
      .get(`/api/address_districts`, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8' },
        params: { province_id: id },
        // data: ({ 'id': id }),
        cancelToken: source.token,
      })
      .catch(function(thrown) {
        if (axios.isCancel(thrown)) {
          console.log('Request canceled', thrown.message)
        } else {
          // handle error
          console.log('ERROR Request canceled ', thrown.message)
        }
      })
    if (res) {
      // console.log(res.data)
      const cleanData = res.data.map((obj) => {
        // console.log(obj)
        return {
          ...obj,
          value: obj.district_id,
          label: obj.name_in_thai,
        }
      })
      // console.log(cleanData)
      result = cleanData
      // return cleanData
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
  return result
}

// API 07 - address_subdistricts (get)
export const preloadSubDistricts = async (id) => {
  console.log(`preload /address_subdistricts`)

  let result = null
  try {
    const res = await request
      .get('/api/address_subdistricts', {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8' },
        params: { districts_id: id },
        // data: ({ 'id': id }),
        cancelToken: source.token,
      })
      .catch(function(thrown) {
        if (axios.isCancel(thrown)) {
          console.log('Request canceled', thrown.message)
        } else {
          // handle error
          console.log('ERROR Request canceled ', thrown.message)
        }
      })
    if (res) {
      // console.log(res.data)
      const cleanData = res.data.map((obj) => {
        // console.log(obj)
        return {
          ...obj,
          value: obj.sub_district_id,
          label: obj.name_in_thai,
        }
      })
      // console.log(cleanData)
      result = cleanData
      // return cleanData
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
  return result
}

// API 08 - get_zipcode (get)
export const getAddressFromZipcode = async (zipcode) => {
  console.log('get Address from Zip Code')
  // console.log(zipcode)
  // const zip = String(zipcode)
  // const msg = JSON.stringify({ zipcode: zip })
  let result = null
  try {
    // request.interceptors.request.use((request) => {
    //   console.log('Starting Request', request)
    //   return request
    // })
    // request.interceptors.response.use((response) => {
    //   console.log('Response:', response)
    //   return response
    // })
    const res = await request.get(`/api/get_zipcode`, { params: { zipcode: zipcode } }, function(
      req,
      res
    ) {
      console.log(req.params)
    })

    if (res) {
      if ('message' in res.data) {
        // If there is meessage, It's was an error
        result = res.data
      } else {
        // console.log(res.data)
        // cleanData = res.data
        const cleanData = {}
        const cleanDataProvince = {
          ...res.data.province,
          id: res.data.province.province_id,
          value: res.data.province.province_id,
          label: res.data.province.name_in_thai,
        }

        const cleanDataDistrict = {
          ...res.data.district,
          id: res.data.province.district_id,

          value: res.data.district.district_id,
          label: res.data.district.name_in_thai,
        }

        const cleanDataSubDistrict = res.data.sub_district.map((obj) => {
          // console.log(obj)
          return {
            ...obj,
            id: obj.sub_district_id,
            value: obj.sub_district_id,
            label: obj.name_in_thai,
            // district_id: obj.province_id,
          }
        })

        cleanData.province = cleanDataProvince
        cleanData.district = cleanDataDistrict
        cleanData.sub_district = cleanDataSubDistrict

        console.log(cleanData)
        result = cleanData
      }
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
  return result
}

// API 03 - get_address_user (get)
export const getAddressFromBuyerId = async (buyerId) => {
  console.log(`getAddressBuyer ${buyerId}`)
  let result = null
  try {
    const res = await request
      .get(`/user/${buyerId}`, { cancelToken: source.token })
      .catch(function(thrown) {
        if (axios.isCancel(thrown)) {
          console.log('Request canceled', thrown.message)
        } else {
          // handle error
          console.log('ERROR Request canceled ', thrown.message)
        }
      })

    if (res) {
      // console.log(res.data)
      const cleanData = res.data
      const cleanDataProvince = {
        ...res.data.province,
        id: res.data.province.province_id,
        value: res.data.province.province_id,
        label: res.data.province.name_in_thai,
      }

      const cleanDataDistrict = {
        ...res.data.district,
        id: res.data.province.province_id,

        value: res.data.district.province_id,
        label: res.data.district.name_in_thai,
      }

      const cleanDataSubDistrict = {
        ...res.data.sub_district,
        id: res.data.sub_district.sub_district_id,
        district_id: res.data.sub_district.sub_district_id,
        value: res.data.sub_district.sub_district_id,
        label: res.data.sub_district.name_in_thai,
      }

      cleanData.province = cleanDataProvince
      cleanData.district = cleanDataDistrict
      cleanData.sub_district = cleanDataSubDistrict

      result = cleanData

      // console.log(cleanData)
      // result = res.data
      // return cleanData
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
  return result
}

export const postAddress = async (body) => {
  const msg = JSON.stringify(body)
  console.log(body)
  try {
    // request.interceptors.request.use((request) => {
    //   console.log('Starting Request', request)
    //   return request
    // })
    // request.interceptors.response.use((response) => {
    //   console.log('Response:', response)
    //   return response
    // })
    const res = await request
      .post(`/user`, { body: msg, cancelToken: source.token })
      .catch(function(thrown) {
        if (axios.isCancel(thrown)) {
          console.log('Request canceled', thrown.message)
        } else {
          // handle error
          console.log('ERROR Request canceled ', thrown.message)
        }
      })
    // const res = await post(body,'/user')
    if (res) {
      // console.log(res)
      console.log(res.data)
      if (res.data.message === 'Completed.') {
        console.log('complete')
        return true
      } else {
        console.log('uncomplete')
        return false
      }
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
}

export const cancelAxios = () => {
  source.cancel('Operation canceled from ComponentWillUnmount()')
}
