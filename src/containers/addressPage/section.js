import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled, { withTheme, css } from 'styled-components'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import {
  preloadProvinces,
  preloadDistricts,
  preloadSubDistricts,
  getAddressFromZipcode,
  postAddress,
  // getAddressFromBuyerId,
  // cancelAxios,
} from './api'
// import _ from 'lodash'
import Selector from '../../components/selector'
// import NumberInput from '../../components/numberInput'




const Content = styled.div`
  ${(props) => {
    console.log(`focus:: ${props.focus}`)
    if (props.focus) {
      return css`
        margin-top: -${props.offset}px;
      `
    }
  }}

  transition: margin-top 0.5s;
  -moz-transition: margin-top 0.5s;
  -webkit-transition: margin-top 0.5s;

  // position: absolute;
  // top: 24vh;
  // right: 5vw;
  display: flex;
  flex-direction: column;
  width: 85vw;
  // padding: 0px 10px;
  // align-self:flex-end;
  // justify-content: center;
  align-items: flex-end;
  color: ${(props) => props.theme.textPurple};
  font-size: 0.8rem;
  z-index: 100;

  @media only screen and (min-width: 451px) {
    top: 160px;
    right: calc((100% / 2) - 250px + 25px);
    width: 400px;
  }
`

const Field = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex: 1;
  margin-bottom: 1vh;
`

const NumberInput = styled.input`
  width: 57vw;
  height: 10vw;
  padding: 12px;
  border-radius: 4px;
  box-sizing: border-box;
  border: 1px solid ${(props) => props.theme.primary} ;
  color: ${(props) => props.theme.textPurple};
  background: ${(props) => props.theme.tertiary};

  @media only screen and (min-width: 451px) {
    width: calc(500px * 0.57);
    height: 38px;
  }
`
const TextInput = styled.input`
  width: 57vw;
  height: 10vw;
  padding: 12px;
  border-radius: 4px;
  box-sizing: border-box;
  border: 1px solid ${(props) => props.theme.primary} ;
  color: ${(props) => props.theme.textPurple};
  background: ${(props) => props.theme.tertiary};
  font-size: 0.8rem;

  @media only screen and (min-width: 451px) {
    width: calc(500px * 0.57);
    height: 38px;
  }
`

// const noOption = [
//   {
//     value: 0,
//     label: 'ไม่มีตัวเลือก',
//   },
// ]

class AddressPage extends Component {
  _isMounted = false
  _isUpdated = false

  state = {
    buyerLineId: '',
    buyerName: '',
    buyerTel: '',
    buyerAddress: '',
    provinces: [],
    districts: [],
    subdistricts: [],
    zipcode: '',
    selectedProvince: null,
    selectedDistrict: null,
    selectedSubdistrict: null,
    zipcodeLoad: false,

    preload: false,
  }

  componentDidMount = async () => {
    this._isMounted = true

    window.addEventListener('resize', this.resize.bind(this))
    this.resize()
    //get query string
    const qsValues = queryString.parse(this.props.location.search)
    // (Query String) Check is Empty Query String or not
    if (Object.entries(qsValues).length === 0) {
      console.log(`no query string`)
      // (Query String) If no query string or (querystring have no information)
      // then Start preload
      if (!this.state.preload) {
        // If not preload ( preload = false )
        const provinces = await preloadProvinces()
        // const districts = await preloadAddress('/address_districts')
        // const subdistricts = await preloadAddress('/address_subdistricts')

        if (this._isMounted) {
          const { name, telephone, address, province, district, sub_district } = this.props.address

          // modify add some key to match with Selector options
          // If object choice is same key with Selector options
          // It will show preload result in selectors
          // if not empty address
          // if (!_.isEmpty(this.props.address)) {
          if (province !== null) {
            province.value = province.province_id
            province.label = province.name_in_thai
          }
          if (district !== null) {
            district.value = district.district_id
            district.label = district.name_in_thai
          }
          if (sub_district !== 0) {
            sub_district.value = sub_district.sub_district_id
            sub_district.label = sub_district.name_in_thai
          }

          // Start Update to check is completed form
          this._isUpdated = true

          this.setState({
            ...this.state,
            buyerLineId: this.props.address.user_id,
            buyerName: name ? name : '',
            buyerTel: telephone ? telephone : '',
            buyerAddress: address ? address : '',
            selectedProvince: province ? province : null,
            selectedDistrict: district ? district : null,
            selectedSubdistrict: sub_district ? sub_district : null,
            zipcode: sub_district.zip_code ? sub_district.zip_code : '',
            provinces: provinces,
            zipcodeLoad: sub_district.zip_code ? true : false,
            // districts: districts,
            // subdistricts: subdistricts,
            preload: true,
          })
          // } else {
          //   console.log(`this.props.address is empty`)
          // }
        }
      } else {
        // If preload ( preload = true )
        console.log('trigger did mount (preload = true) ')
      }
    }
  }

  componentDidUpdate = async (prevProps, prevState) => {
    if (prevState !== this.state && this._isUpdated) {
      this.props.setAddress({
        name: this.state.buyerName,
        telephone: this.state.buyerTel,
        address: this.state.buyerAddress,
        sub_district: this.state.selectedSubdistrict,
      })
      if (this.isCompleteForm(this.state)) {
        this.props.handleSetActive(true)
      } else {
        this.props.handleSetActive(false)
      }
    }
    if (
      prevState.selectedDistrict !== this.state.selectedDistrict &&
      this.state.selectedDistrict !== null &&
      this.state.selectedSubdistrict !== null
    ) {
      // This is for update options for subdistricts
      // when choose then update according to
      // district_id (for subdistricts options)
      if (this._isUpdated) {
        console.log(`update District && SubDistricts Option !!!!!!`)
        // this._isUpdated = false
        console.log(this.state.selectedDistrict)
        const districts = await preloadDistricts(this.state.selectedProvince.value)
        const sub_districts = await preloadSubDistricts(this.state.selectedDistrict.value)
        // // console.log(districts)
        // // console.log(sub_districts)
        this.setState({
          districts: districts,
          subdistricts: sub_districts,
          // selectedSubdistrict: null,
        })
      }
    }
    if (
      prevState.selectedProvince !== this.state.selectedProvince &&
      this.state.selectedProvince !== null
    ) {
      // This is for update options for districts
      // when choose then update according to
      // province_id (for districts options) and
      if (this._isUpdated) {
        console.log(`update districts Option !!!!!!`)
        this._isUpdated = false
        console.log(this.state.selectedProvince)
        const districts = await preloadDistricts(this.state.selectedProvince.value)
        this.setState({
          districts: districts,
        })
      }
    }

    if (
      prevState.selectedDistrict !== this.state.selectedDistrict &&
      this.state.selectedDistrict !== null
    ) {
      // This is for update options for subdistricts
      // when choose then update according to
      // district_id (for subdistricts options)
      if (this._isUpdated) {
        console.log(`update SubDistricts Option !!!!!!`)
        // this._isUpdated = false
        console.log(this.state.selectedDistrict)
        const sub_districts = await preloadSubDistricts(this.state.selectedDistrict.value)
        // // console.log(districts)
        // // console.log(sub_districts)
        this.setState({
          subdistricts: sub_districts,
          // selectedSubdistrict: null,
        })
      }
    }
    if (
      prevState.selectedSubdistrict !== this.state.selectedSubdistrict &&
      this.state.selectedSubdistrict !== null
    ) {
      // This is for update options for districts and subdistricts
      // when choose then update according to
      // province_id (for districts options) and
      // district_id (for subdistricts options)
      if (this._isUpdated) {
        console.log(`update SubDistricts  !!!!!!`)
        // this._isUpdated = false
        // console.log(this.state.selectedSubdistrict)
        // // console.log(sub_districts)
        this.setState({
          zipcode: this.state.selectedSubdistrict.zip_code,
          // selectedSubdistrict: null,
        })
      }
    }
    if (
      prevState.zipcode !== this.state.zipcode && // Detect State Change
      this.state.zipcode > 9999 && // Detect zip code more than once
      !this.state.zipcodeLoad // Boolean for detect once
      // !this.state.buyerIdLoad // Boolean check if not input from buyerId
    ) {
      // this.setState({ ...this.state, zipcode: this.state.zipcode })
      this.handleGetAddress(this.state.zipcode)
    }
    // When buyerIdLoad is TRUE  but user want to adjust zip code
    // Then buyerIdLoad is set FALSE
    if (
      prevState.zipcode !== this.state.zipcode &&
      this.state.zipcode < 9999
      // this.state.buyerIdLoad
    ) {
      this.props.setAddress({})
      // this.setState({
      //   ...this.state,
      //   buyerIdLoad: false,
      // })
    }
    if (this.state.zipcodeLoad && this.state.zipcode < 10000 && this._isUpdated) {
      this._isUpdated = false
      const provinces = await preloadProvinces()
      this.props.setAddress({ province: null, district: null, sub_district: 0 })
      this.setState({
        ...this.state,
        provinces: provinces,
        districts: null,
        subdistricts: null,
        selectedProvince: null,
        selectedDistrict: null,
        selectedSubdistrict: null,
        zipcodeLoad: false,
      })
    }
    // Stop Infinite Loop in componentDidUpdate
    this._isUpdated = false
  }

  isCompleteForm = (state) => {
    return (
      state.buyerName !== '' &&
      state.buyerTel !== '' &&
      state.buyerAddress !== '' &&
      state.zipcode !== '' &&
      state.selectedProvince !== null &&
      state.selectedDistrict !== null &&
      state.selectedSubdistrict !== null
    )
  }

  componentWillUnmount() {
    this._isMounted = false
    // cancelAxios()
    this.setState({zipcodeLoad: false,preload: false})
  }

  handleChange = (e) => {
    this._isUpdated = true
    this.setState({ [e.target.name]: e.target.value })
  }

  handleOptionChangeSubDistrict = (selectedOption, key) => {
    this._isUpdated = true
    this.setState({ [key]: selectedOption, zipcode: selectedOption.zip_code })
  }

  handleOptionChange = (selectedOption, key) => {
    this._isUpdated = true
    this.setState({ [key]: selectedOption })
    // console.log(`[key]:`, key)
    // console.log(`Option selected:`, selectedOption)
  }

  handlePostAddress = async () => {
    // {
    //   "line_id": "line_0006",
    //   "name": "สวย สวยสวย",
    //   "telephone": "077777777",
    //   "address": "บริษัทมายองจำกัด 123 หมู่ 4 ต.ในเมือง อ.เมือง จ.ลำพูน",
    //   "sub_district_id": 216
    // }
    const { buyerLineId, buyerName, buyerTel, buyerAddress, selectedSubdistrict } = this.state

    const postMessage = {
      line_id: buyerLineId,
      name: buyerName,
      telephone: buyerTel,
      address: buyerAddress,
      sub_district_id: selectedSubdistrict.sub_district_id
        ? selectedSubdistrict.sub_district_id
        : selectedSubdistrict.id,
    }

    const res = await postAddress(postMessage)
    // Post OK May change to Redirect to Payment page
    res ? console.log('Post OK') : console.log('Post Not OK')
  }

  handleGetAddress = async (zipcode) => {
    console.log('send trigger : passive address')
    const res = await getAddressFromZipcode(zipcode)
    // res.message !== null
    res &&
      ('message' in res
        ? console.log(res.message)
        : this.setState({
            ...this.state,
            provinces: [res.province],
            districts: [res.district],
            subdistricts: res.sub_district,
            selectedProvince: res.province,
            selectedDistrict: res.district,
            selectedSubdistrict: null,
            zipcodeLoad: true,
          }))
    // this.setState({
    //   ...this.state,
    //   selectedProvince: res.province,
    //   selectedDistrict: res.district,
    //   selectedSubdistrict: res.sub_district[0],
    //   subdistricts: res.sub_district,
    // })
  }

  isSelectedProvince = () => {
    return this.state.selectedProvince !== null ? true : false
  }

  handleNumberInput = (e) => {
    this._isUpdated = true
    // const val = e.target.value
    // If the current value passes the validity test then apply that to state
    // console.log(e.target.validity.valid)
    // e.target.validity.valid -- Use Pattern to check regex
    if (e.target.validity.valid) this.setState({ zipcode: e.target.value })
    // else if (val === '' || val === '-') this.setState({ zipcode: val })
  }

  resize() {
    let isWebView = window.innerWidth >= 451
    if (isWebView !== this.state.isWebView) {
      this.setState({ isWebView: isWebView })
    }
  }

  render() {
    console.log(this.state)
    console.log(`this.state.isWebView :: ${this.state.isWebView}`)
    // console.log(`user State :: ${JSON.stringify(this.props.user)}`)
    console.log(`address State :: ${JSON.stringify(this.props.address)}`)

    // SelectorStyles
    // Because inside render this can be constructed with theme (styled.component)
    const SelectorStyles = {
      // control: (styles) => ({ ...styles, backgroundColor: 'white',  }),
      container: (styles) => ({
        ...styles,
        width: `${this.state.isWebView ? 'calc( 500px * 0.57 )' : '57vw'}`,
        
      }),
      control: (styles) => ({
        ...styles,
        backgroundColor: this.props.theme.tertiary,
        border: `1px solid ${this.props.theme.primary}`,
      }),
      option: (styles) => ({
        ...styles,
        backgroundColor: 'white',
        color: this.props.theme.textPurple,
      }),
      singleValue: (styles) => ({
        ...styles,
        color: this.props.theme.textPurple,
      }),
      input: (styles) => ({
        ...styles,
        // 'readonly': 'readonly',
        // 'disabled': 'disabled',
        // ':focus':{
        // }
      }),
      dropdownIndicator: (provided, state) => ({
        ...provided,
        transform: state.selectProps.menuIsOpen && 'rotate(180deg)',
        color: this.props.theme.primary,
      }),
    }

    return (
      <div>
        <Content focus={this.props.focus} offset={this.props.offset}>
          <Field>
            ชื่อ-นามสกุล
            <TextInput
              name="buyerName"
              type="text"
              value={this.state.buyerName}
              onChange={(e) => this.handleChange(e)}
            />
          </Field>
          <Field>
            เบอร์โทรศัพท์
            <TextInput
              name="buyerTel"
              type="tel"
              value={this.state.buyerTel}
              onChange={(e) => this.handleChange(e)}
            />
          </Field>
          <Field>
            ที่อยู่
            <TextInput
              name="buyerAddress"
              type="text"
              value={this.state.buyerAddress}
              placeholder="เลขที่อยู่ / ชื่ออาคาร / ถนน / ซอย"
              onChange={(e) => this.handleChange(e)}
            />
          </Field>
          <Field>
            รหัสไปรษณีย์
            <NumberInput
              name="zipcode"
              type="tel"
              maxLength={5}
              value={this.state.zipcode}
              onKeyPress={this.maxLengthCheck}
              onChange={(e) => this.handleNumberInput(e)}
              pattern="\d+"
            />
          </Field>

          <Field>
            จังหวัด{' '}
            <Selector
              name="selectedProvince"
              placeholder="เลือก..."
              maxMenuHeight={125}
              blurInputOnSelect={true}
              captureMenuScroll={false}
              autoFocus={false}
              isFocused={false}
              styles={SelectorStyles}
              value={this.state.selectedProvince}
              options={this.state.provinces}
              onChange={(e) => this.handleOptionChange(e, 'selectedProvince')}
              onFocus={(e) => this.props.handleFocus(e)}
              onBlur={(e) => this.props.handleBlur(e)}
              ref={this.ref1}
            />
          </Field>
          <Field>
            อำเภอ{' '}
            <Selector
              name="selectedDistrict"
              placeholder="เลือก..."
              maxMenuHeight={200}
              blurInputOnSelect={true}
              captureMenuScroll={false}
              autoFocus={false}
              isFocused={true}
              //isSearchable={false}
              isDisabled={this.state.selectedProvince !== null ? false : true}
              //isDisabled={false}
              styles={SelectorStyles}
              value={this.state.selectedDistrict}
              options={this.state.districts}
              onChange={(e) => this.handleOptionChange(e, 'selectedDistrict')}
              onFocus={(e) => this.props.handleFocus(e)}
              onBlur={(e) => this.props.handleBlur(e)}
            />
          </Field>
          <Field>
            ตำบล{' '}
            <Selector
              name="selectedSubdistrict"
              placeholder="เลือก..."
              maxMenuHeight={150}
              blurInputOnSelect={true}
              captureMenuScroll={false}
              autoFocus={false}
              isFocused={true}
              // *** Need isSearchable but need to lift page for keyboard views
              //isSearchable={false}
              isDisabled={this.state.selectedDistrict !== null ? false : true}
              //isDisabled={false}
              styles={SelectorStyles}
              value={this.state.selectedSubdistrict}
              options={this.state.subdistricts}
              onChange={(e) => this.handleOptionChangeSubDistrict(e, 'selectedSubdistrict')}
              onFocus={(e) => this.props.handleFocus(e)}
              onBlur={(e) => this.props.handleBlur(e)}
            />
          </Field>
        </Content>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  address: state.address,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = (dispatch) => {
  return {
    setAddress: dispatch.address.setAddress,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(AddressPage)))

// options={this.state.selectedProvince !== null ? this.state.districts.filter(obj => obj.province_id === this.state.selectedProvince.id) : null }
// onKeyDown={(e) => this.maxLengthCheck(e)}

// district old options
// options={
//   this.state.selectedProvince !== null &&
//   !this.state.zipcodeLoad &&
//   !this.state.buyerIdLoad
//     ? this.state.districts.filter(
//         (obj) => obj.province_id === this.state.selectedProvince.id
//       )
//     : this.state.zipcodeLoad || this.state.buyerIdLoad
//     ? this.state.districts
//     : noOption
// }

// options={this.state.subdistricts? this.state.subdistricts : noOption}
// sub_district old options
// options={
//   this.state.selectedDistrict !== null &&
//   !this.state.zipcodeLoad &&
//   !this.state.buyerIdLoad
//     ? this.state.subdistricts.filter(
//         (obj) => obj.district_id === this.state.selectedDistrict.id
//       )
//     : this.state.zipcodeLoad || this.state.buyerIdLoad
//     ? this.state.subdistricts
//     : noOption
// }
