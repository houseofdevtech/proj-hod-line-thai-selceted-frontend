import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import _ from 'lodash'

import Page from './page'
import LoadingScreen from '../../views/loadingPage'

// get user api03 from userId

class LandingAddressPage extends Component {
  _isUpdated = false

  state = {
    isLineUserId: false,
  }

  isInternalLink = (props) => {
    return 'from' in this.props.state
  }
  isLineUserIdCheck = () => {
    return (
      !this._isUpdated &&
      !this.state.isLineUserId &&
      this.props.user.line.userId !== '' &&
      this.props.user.chattyUserId !== ''
    )
  }

  componentDidMount = async () => {
    console.log('didmount')
    if (this.isLineUserIdCheck()) {
      this._isUpdated = true
      this.setState({ isLineUserId: true })

      console.log('loadata didmount')
      this.loadData()
    }

  }

  componentDidUpdate = async () => {
    if (this.isLineUserIdCheck()) {
      this._isUpdated = true
      this.setState({ isLineUserId: true })
      
      console.log('loadata didupdated')
      this.loadData()
    }
  }

  loadData = async () => {
    await this.props.getOrder(this.props.user)

    const isOrderExist = !_.isEmpty(this.props.payment.order)
    if (isOrderExist) {
      await this.props.getAddress(this.props.user)
    } else {
      // redirect to empty order Page
      console.log(`isOrderExist(false) :: ${JSON.stringify(this.props.payment.order)}`)
      console.log('redirect')
      this.props.history.push('/emptyorder')
    }
  }

  // componentWillUnmount(){
  //   this._isUpdated = false
  //   this.setState({isLineUserId: false})
  // }

  isGetAddress = (address) => {
    return 'user_id' in address
  }

  // isGoToPayment = (props) => {
  //   // console.log(props.location.state)
  //   return props.address.isCompleteAddress && ( props.location.state && !('from' in props.location.state))
  // }

  render() {
    console.log(`address state :: ${JSON.stringify(this.state)}`)
    console.log(`address :: ${JSON.stringify(this.props.address)}`)

    // ***** Full Check Address Before Payment *****
    return (
      <div>
        {this.isGetAddress(this.props.address) ? (
          <Page {...this.props} />
        ) : (
          <LoadingScreen label="Waiting User Information" />
        )}
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  user: state.user,
  address: state.address,

  payment: state.payment,
})

const mapDispatchToProps = (dispatch) => {
  return {
    checkUser: dispatch.user.checkUser,
    getAddress: dispatch.address.getAddress,
    setAddress: dispatch.address.setAddress,

    getOrder: dispatch.payment.getOrder,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LandingAddressPage))
