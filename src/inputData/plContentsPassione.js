export const menus = [
  {
    type: 'all',
    title: 'ทั้ังหมด',
  },
  {
    type: 'ภาคกลาง',
    title: 'ภาคกลาง',
  },
  {
    type: 'ภาคเหนือ',
    title: 'ภาคเหนือ',
  },
  {
    type: 'ภาคตะวันออก',
    title: 'ภาคตะวันออก',
  },

  {
    type: 'ภาคตะวันออกเฉียงเหนือ',
    title: 'ภาคตะวันออกเฉียงเหนือ',
  },
  {
    type: 'ภาคใต้',
    title: 'ภาคใต้',
  },
]

export const contents = [
  // shopLink: 'h'ttps://vulcan.houseofdev.tech/proj-hod-line-shop-buyer-frontend/?page=product',
  // askLink: 'h'ttps://vulcan.houseofdev.tech/proj-hod-line-shop-buyer-frontend/?page=ask',
  {
    type: 'est',
    title: 'Estee Lauder',
    products: [
      {
        name: ' Recovery Complex II 50 ML',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/4e324ab85f5f2ee09fa630a5f90e7db98886beb9883b9cdf80e0be9413d776e2`,
        originalPrice: 4230,
        salePrice: 4230,
        shopLink: '/product',
        shopId: 42,
        productId: 327,
        askLink: '/ask',
      },
      {
        name: ' Recovery Complex II 75 ML',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/649022a1e1aa4c67e49441a95d453661e87e321f119a624d94056a9ddda723ac`,
        originalPrice: 5220,
        salePrice: 5220,
        shopLink: '/product',
        shopId: 42,
        productId: 328,
        askLink: '/ask',
      },
      {
        name: 'Intense Reset Concentrate',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/618413e232d40df1132f137280fcc918ecc4408dabd931d65761fb1807bc3fbc`,
        originalPrice: 3510,
        salePrice: 3510,
        shopLink: '/product',
        shopId: 42,
        productId: 329,
        askLink: '/ask',
      },
      {
        name: 'Eye Concentrate Matrix',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/dd9136fd785cb242c4ebedfd144d21b2890781536ee5694f055d8bcc90c63e5e`,
        originalPrice: 2790,
        salePrice: 2790,
        shopLink: '/product',
        shopId: 42,
        productId: 330,
        askLink: '/ask',
      },
      {
        name: 'Supercharged Complex ',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/86619ddec32816566953a2fa14cb43b5893bd345a4c93c07e00ff242ff76f171`,
        originalPrice: 2430,
        salePrice: 2430,
        shopLink: '/product',
        shopId: 42,
        productId: 331,
        askLink: '/ask',
      },
      {
        name: 'Double Wear Stay-in-Place ',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/1a60bcd8c2c3d9218b1c01a1563a24ed9b34e6832b170e4e68e3d4e01305cafd`,
        originalPrice: 1755,
        salePrice: 1755,
        shopLink: '/product',
        shopId: 42,
        productId: 334,
        askLink: '/ask',
      },
      {
        name: 'Double Wear Stay-in-Place ',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/1a60bcd8c2c3d9218b1c01a1563a24ed9b34e6832b170e4e68e3d4e01305cafd`,
        originalPrice: 1755,
        salePrice: 1755,
        shopLink: '/product',
        shopId: 42,
        productId: 333,
        askLink: '/ask',
      },
      {
        name: 'Double Wear Stay-in-Place ',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/1a60bcd8c2c3d9218b1c01a1563a24ed9b34e6832b170e4e68e3d4e01305cafd`,
        originalPrice: 1755,
        salePrice: 1755,
        shopLink: '/product',
        shopId: 42,
        productId: 335,
        askLink: '/ask',
      },
    ],
  },
  {
    type: 'bgw',
    title: 'BODY GLOVE (WOMEN)',
    products: [
      {
        name: 'BODY GLOVE (WOMEN) BLACK',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/2e37409dd76160b44c573db9f88f82fbc906752540c680b27933112606206d72`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 344,
        askLink: '/ask',
      },
      {
        name: 'BODY GLOVE (WOMEN) D-SLATE',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/ac015325aa6ef2cfef530ca7fce4e56117d31ed9f59e4ce711c195e30321124a`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 343,
        askLink: '/ask',
      },
      {
        name: 'BODY GLOVE (WOMEN) DK.GREY',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/618413e232d40df1132f137280fcc918ecc4408dabd931d65761fb1807bc3fbc`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 345,
        askLink: '/ask',
      },
      {
        name: 'BODY GLOVE (WOMEN) YELLOW',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/e844fc6402080baa5fe3f04d4a097c7e40935f5cb84bbb6b329b110099c23441`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 341,
        askLink: '/ask',
      },
      {
        name: 'BG (WOMEN) TROPICAL GREEN',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/d7f9c6ca1729ca1088f3279209d995c668073d77515b460410a970ccfc823ea6`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 342,
        askLink: '/ask',
      },
    ],
  },
  {
    type: 'bgm',
    title: 'BODY GLOVE (MEN)',
    products: [
      {
        name: 'BODY GLOVE (MEN) BLACK',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/e031c133276ac50776d919c078b51c83ad773c096df35479110d35991f0ae922`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 355,
        askLink: '/ask',
      },
      {
        name: 'BODY GLOVE (MEN) D-SLATE',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/0413334fceb51df18905a8ac82bdd5ac28eef14eef30b7d34a2b571f5833fe1a`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 356,
        askLink: '/ask',
      },
      {
        name: 'BODY GLOVE (MEN) DARK GREY',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/c8c8b6c186ef6b122d3a99342502c72752347c25a3a990fdc753e3a1c5f39a08`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 357,
        askLink: '/ask',
      },
      {
        name: 'BODY GLOVE (MEN) YELLOW',
        image: `https://storage.googleapis.com/passione-chatty-asset-image/products/shop_42/59ac14522976b063522e4921e6badf8e586833851a86112f2668e72cc5694370`,
        originalPrice: 690,
        salePrice: 395,
        shopLink: '/product',
        shopId: 42,
        productId: 358,
        askLink: '/ask',
      },
    ],
  },
  {
    type: 'bhu',
    title: 'BEVERY HILLS',
    products: [
      {
        name: 'BLUE - UNISEX',
        image: `https://vulcan.houseofdev.tech/wit/img/BHU1_BLUE.png`,
        originalPrice: 990,
        salePrice: 199,
        shopLink: '/product',
        shopId: 33,
        productId: 206,
        askLink: '/ask',
      },
      {
        name: 'DARK BLUE - UNISEX',
        image: `https://vulcan.houseofdev.tech/wit/img/BHU2_DK-BLUE.png`,
        originalPrice: 990,
        salePrice: 199,
        shopLink: '/product',
        shopId: 33,
        productId: 207,
        askLink: '/ask',
      },

      {
        name: 'ORANGE - UNISEX',
        image: `https://vulcan.houseofdev.tech/wit/img/BHU3_ORANGE.png`,
        originalPrice: 990,
        salePrice: 199,
        shopLink: '/product',
        shopId: 33,
        productId: 208,
        askLink: '/ask',
      },

      {
        name: 'YELLOW - UNISEX',
        image: `https://vulcan.houseofdev.tech/wit/img/BHU4_YELLOW.png`,
        originalPrice: 990,
        salePrice: 199,
        shopLink: '/product',
        shopId: 33,
        productId: 209,
        askLink: '/ask',
      },

      {
        name: 'RED - UNISEX',
        image: `https://vulcan.houseofdev.tech/wit/img/BHU5_RED.png`,
        originalPrice: 990,
        salePrice: 199,
        shopLink: '/product',
        shopId: 33,
        productId: 210,
        askLink: '/ask',
      },
    ],
  },
]
