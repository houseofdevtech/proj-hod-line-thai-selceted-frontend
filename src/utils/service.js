// export const ip = 'https://vulcan.houseofdev.tech/blue/api/v1.0/'
// export const ip = 'https://vulcan.houseofdev.tech/hod-line-shop/backend/api'
export const ip = 'https://chattyshop.houseofdev.tech/backend/api'
export const post = (object, path, token) =>
  new Promise((resolve, reject) => {
    fetch(ip + path, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(object),
    })
      .then((res) => {
        setTimeout(() => null, 0)
        return res.json()
      })
      .then((json) => {
        resolve(json)
      })
      .catch((err) => reject(err))
  })
export const get = (path, token) =>
  new Promise((resolve, reject) => {
    fetch(ip + path, {
      method: 'GET',
      headers: {
        Accept: '*/*',
        // 'Content-Type': 'application/json',
        // 'Content-Type': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
      .then((res) => {
        setTimeout(() => null, 0)
        return res.json()
      })
      .then((json) => {
        resolve(json)
      })
      .catch((err) => reject(err))
  })
export const get_other = (path, token) =>
  new Promise((resolve, reject) => {
    fetch(path, {
      method: 'GET',
      mode: 'no-cors',
    })
      .then((res) => {
        setTimeout(() => null, 0)
        return res.blob()
      })
      .then((blob) => {
        console.log(blob)
        resolve(blob)
      })
      .catch((err) => reject(err))
  })
