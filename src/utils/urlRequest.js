import axios from 'axios'

let instance = null 

switch (process.env.REACT_APP_DEV) {
  case 'passione':
    instance = axios.create({
      baseURL: 'https://apichattyshop.passione.co.th',
    })

    break
  case 'dev':
    instance = axios.create({
      baseURL: 'https://chattyshop.houseofdev.tech/backend',
    })
    break
  case 'uat':
    instance = axios.create({
      baseURL: 'https://uat.chatty.shop/backend',
    })
    break
  default:
    // NOTED: local
    instance = axios.create({
      // baseURL: 'https://vulcan.houseofdev.tech/hod-line-shop/backend/api',
      // baseURL: 'https://uat.chatty.shop/backend/api',
      // baseURL: 'https://chattyshop.houseofdev.tech/backend',
      baseURL: 'https://apichattyshop.passione.co.th'
    })
}

export default instance
