import React from 'react'
import MediaQuery from 'react-responsive'

export const Mobile = (props) => <MediaQuery {...props} maxWidth={450} />
export const MobileAndTablet = (props) => <MediaQuery {...props} maxWidth={1024} />
export const Desktop = (props) => <MediaQuery {...props} minWidth={451} />
export const LargeDesktop = (props) => <MediaQuery {...props} minWidth={1025} />
