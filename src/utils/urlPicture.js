let picBaseURL = null 

switch (process.env.REACT_APP_DEV) {
  case 'passione':
    picBaseURL = ''
    break
  case 'dev':
    picBaseURL = 'https://chattyshop.houseofdev.tech/backend/'
    break
  case 'uat':
    picBaseURL = 'https://uat.chatty.shop/backend/'
    break
  default:
    // NOTED: local
    picBaseURL = 'https://chattyshop.houseofdev.tech/backend/'
}

export default picBaseURL
