import _ from 'lodash'

export const isValidProductQS = (obj) => {
  if (!_.isEmpty(obj) && 'shop_id' in obj && 'product_id' in obj) {
    console.log(`Go with \n shop_id \t : \t ${obj.shop_id} \n product_id  : \t ${obj.product_id}`)
    return true
  } else {
    console.log(`incomplete shop_id or product_id field`)
    console.log(`qsObject : ${JSON.stringify(obj)}`)
    return false
  }
}

export const isValidPaymentQS = (obj) => {
  if (!_.isEmpty(obj) && 'shop_id' in obj ) {
    console.log(`Go with \n shop_id \t : \t ${obj.shop_id} `)
    return true
  } else {
    console.log(`incomplete shop_id or relate fields`)
    console.log(`qsObject : ${JSON.stringify(obj)}`)
    return false
  }
}

export const isValidSlipQS = (obj) => {
  if (!_.isEmpty(obj) && 'shop_id' in obj && 'bill_id' in obj && 'bank_id' in obj) {
    console.log(`Go with \n shop_id \t : \t ${obj.shop_id} \n bill_id  : \t ${obj.bill_id} \n bank_id  : \t ${obj.bank_id}`)
    return true
  } else {
    console.log(`incomplete shop_id or bill_id or bank_id field`)
    console.log(`qsObject : ${JSON.stringify(obj)}`)
    return false
  }
}
