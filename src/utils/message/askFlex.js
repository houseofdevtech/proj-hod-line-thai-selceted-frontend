export const flexMessage = (picUrl,name,price) => {
  return {
    type: 'flex',
    altText: 'Flex Message',
    contents: {
      type: 'bubble',
      hero: {
        type: 'image',
        url: picUrl,
        size: 'full',
        aspectRatio: '1:1',
        aspectMode: 'cover',
      },
      body: {
        type: 'box',
        layout: 'vertical',
        spacing: 'sm',
        contents: [
          {
            type: 'box',
            layout: 'baseline',
            contents: [
              {
                type: 'text',
                text: name,
                size: 'sm',
                align: 'start',
                weight: 'bold',
                wrap: true,
              },
              {
                type: 'text',
                text: `฿ ${price}`,
                flex: 0,
                size: 'xl',
                weight: 'bold',
                wrap: true,
              },
            ],
          },
        ],
      },
    },
  }
}
