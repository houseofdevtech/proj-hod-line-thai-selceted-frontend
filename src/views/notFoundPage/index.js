import React, { Component } from 'react'
import { connect } from 'react-redux'
import Frame from '../../components/frameLayout'
import _ from 'lodash'
// import './style.css'
import { FlexCol } from '../../style/generalStyle'

class NotFoundPage extends Component {
  render() {
    const { deviceOS, browserLanguage, sdkVersion, isInClient, isLoggedIn } = this.props.appState
    return (
      <Frame justifyContent="center">
        <FlexCol>
          404 Not Found
          {!_.isEmpty(this.props.appState) && (
            <table>
              <tbody>
                <tr>
                  <th>OS</th>
                  <td style={{ textlign: 'left' }}>{deviceOS}</td>
                </tr>
                <tr>
                  <th>Language</th>
                  <td style={{ textlign: 'left' }}>{browserLanguage}</td>
                </tr>
                <tr>
                  <th>LIFF SDK Version</th>
                  <td style={{ textlign: 'left' }}>{sdkVersion}</td>
                </tr>
                <tr>
                  <th>isInClient</th>
                  <td style={{ textlign: 'left' }}>{isInClient}</td>
                </tr>
                <tr>
                  <th>isLoggedIn</th>
                  <td style={{ textlign: 'left' }}>{isLoggedIn}</td>
                </tr>
              </tbody>
            </table>
          )}
        </FlexCol>
      </Frame>
    )
  }
}

const mapStateToProps = (state) => ({
  appState: state.appState,
})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(NotFoundPage)
