import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoadingScreen from 'react-loading-screen'

import Frame from '../../components/frameLayout'
import { withTheme } from 'styled-components'

class LoadingPage extends Component {
  render() {
    // console.log(this.props.theme)
    // console.log(this.props.location)
    return (
      <Frame primaryColor={this.props.theme.secondary} secondaryColor={'#fff'}>
        <LoadingScreen
          loading={true}
          bgColor="#ffffff"
          spinnerColor={this.props.theme.primary}
          textColor={this.props.theme.primary}
          text={this.props.label ? this.props.label : 'Loading'}
          children={''}
        />
      </Frame>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
})

// Add return {} (empty object) to fix react warning
const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(LoadingPage))
