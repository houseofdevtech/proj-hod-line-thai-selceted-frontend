import request from './utils/urlRequest'
import { flexMessage } from './utils/message/askFlex'

const liff = window.liff

export const handleCloseApp = () => {
  // event.preventDefault();
  console.log('close')
  liff.closeWindow()
}

export const handleBack = (history) => {
  console.log('Back!!!')
  // this.props.history.push('/')
  // this.props.history.goBack()
  history.goBack()
}

export const handleUrlBack = (url) => {
  window.location.href = url
}

export const handleSubmitOrder = async () => {
  console.log('handleSubmitOrder')
}

export const handleAsk1 = (comment, picUrl) => {
  console.log('AppCommand: handleAsk1')
  liff
    .sendMessages([
      {
        type: 'image',
        originalContentUrl: picUrl,
        previewImageUrl: picUrl,
      },
    ])
    .then(() => {
      console.log('message sent')
      liff.closeWindow()
    })
    .catch((err) => {
      liff.sendMessages([
        {
          type: 'text',
          text: `${err}`,
        },
      ])
      console.log('error', err)
    })
}

export const handleAsk2 = (comment, picUrl, name, price) => {
  console.log('AppCommand: handleAsk2')
  liff
    .sendMessages([
      {
        type: 'text',
        text: comment,
      },
    ])
    .then(() => {
      console.log('message sent')
      liff.closeWindow()
    })
    .catch((err) => {
      liff.sendMessages([
        {
          type: 'text',
          text: `${err}`,
        },
      ])
      console.log('error', err)
    })
}

export const handleAsk3 = (comment, picUrl, name, price) => {
  console.log('AppCommand: handleAsk3')
  liff
    .sendMessages([
      flexMessage(picUrl, name, price),
      {
        type: 'text',
        text: comment,
      },
    ])
    .then(() => {
      console.log('message sent')
      liff.closeWindow()
    })
    .catch((err) => {
      liff.sendMessages([
        {
          type: 'text',
          text: `${err}`,
        },
      ])
      console.log('error', err)
    })
}

export const handleSubmitAskSeller = (comment, picUrl, name, price) => {
  console.log('AppCommand: handleSubmitAskSeller')
  liff
    .sendMessages([
      {
        type: 'image',
        originalContentUrl: picUrl,
        previewImageUrl: picUrl,
      },
      {
        type: 'text',
        text: comment,
      },
    ])
    .then(() => {
      console.log('message sent')
      liff.closeWindow()
    })
    .catch((err) => {
      liff.sendMessages([
        {
          type: 'text',
          text: `${err}`,
        },
      ])
    })
}

// =============================================================
// triggerQRcode = event => {
//   // event.preventDefault();
//   console.log("QRcode");
//   liff
//     .sendMessages([
//       {
//         type: "postback",
//         data: "qrcode"
//       }
//     ])
//     .then(() => {
//       liff.closeWindow();
//     });
// };

export const handleRequestQRcode = async (cart) => {
  console.log('QRcode')
  // const info = this.state.product
  // const userId = this.props.user.userId
  // this.props.setCartUserId(userId)

  // const info = this.props.cart
  const info = cart
  // const message = JSON.stringify({ key: [info] })
  const message = JSON.stringify({ key: [info] })
  // const userId = this.state.userId
  console.log(message)
  // const accessToken = this.state.accessToken
  try {
    const res = await request.post(`/qrcode`, message)
    // const res = await request.post(`/qrcode/${userId}`, info)
    // const res = await request.post(`/qrcode/12345645`, this.state.product)
    if (res) {
      liff.closeWindow()
      console.log(res)
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
    // liff
    //   .sendMessages([
    //     {
    //       type: 'text',
    //       // text: `response fail : ${e} => /qrcode/${userId}`,
    //       text: 'no response from backend',
    //     },
    //   ])
    //   .then(() => {
    //     liff.closeWindow()
    //   })
  }
}

export const handleMockupFlow = async (info) => {
  const msg = info
  const message = JSON.stringify({ key: [msg] })
  console.log(message)
  try {
    const res = await request.post(`/loading`, message)
    if (res) {
      console.log(res)
      if (res.data.success === 1) {
        console.log('success')
        liff.closeWindow()
      }
      // if (res.error === 1) {
      //   console.log('alert')
      //   liff
      //     .sendMessages([
      //       {
      //         type: 'text',
      //         text: 'no response from backend',
      //       },
      //     ])
      //     .then(() => {
      //       liff.closeWindow()
      //     })
      // }
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
}

export const handleMockupFlow2Msg = async (info, info2) => {
  const msg = info
  const message = JSON.stringify({ key: [msg] })
  const msg2 = info2
  const message2 = JSON.stringify({ key: [msg2] })

  console.log(message)
  try {
    const res = await request.post(`/loading`, message)

    if (res) {
      console.log(res)
      if (res.data.success === 1) {
        console.log('success')
        setTimeout(async () => {
          const res = await request.post(`/loading`, message2)
          if (res) {
            if (res.data.success === 1) {
              console.log('success2')
              liff.closeWindow()
            }
          }
        }, 1000)
      }
    }
  } catch (e) {
    console.log(e)
    console.log('response fail')
  }
}

export const exampleSubmitOrderMessage = {
  body: {
    events: [
      {
        source: {
          display: '', // from LIFF api
          userId: '', // from LIFF api
          pictureUrl: '', // from LIFF api
        },
        data: {
          // *** from Buyer Frontend [Start] ***
          type: '',
          date: '',
          time: '',
        },
        products: [
          {
            id: '',
            name: '',
            productImgUrl: '',
            details: [
              {
                color: '',
                size: '',
                amount: '',
                price: '',
              },
            ],
          },
        ],
        total: {
          amount: '',
          price: '',
        },
      }, // *** from Buyer Frontend [End] ***
    ],
  },
}

export const exampleSubmitShippingMessage = {
  body: {
    events: [
      {
        source: {
          display: '', // from LIFF api
          userId: '', // from LIFF api
          pictureUrl: '', // from LIFF api
        },
        data: {
          // *** from Buyer Frontend [Start] ***
          type: '', // shipping
          date: '', // actual press date
          time: '', // actual press date
        }, // *** from Buyer Frontend [End] ***
        products: [
          // from Backend
          {
            id: '',
            name: '',
            productImgUrl: '',
            details: [
              {
                color: '',
                size: '',
                amount: '',
                price: '',
              },
            ],
          },
        ],
        total: {
          amount: '',
          price: '',
        }, // from Backend
        checkout: {
          // *** from Buyer Frontend [Start] ***
          address: {
            customerName: '',
            tel: '',
            zipcode: '',
            province: '',
            district: '',
            subDistrict: '',
            location: '',
          },
        }, // *** from Buyer Frontend [End] ***
      },
    ],
  },
}
