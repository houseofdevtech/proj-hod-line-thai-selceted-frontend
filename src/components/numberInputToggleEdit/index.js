import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTheme } from 'styled-components'

// import { Link } from 'react-router-dom'
import NumericInput from 'react-numeric-input'

import { Desktop, Mobile } from '../../utils/displayResponsive'

class NumberInputToggleSmall extends Component {
  state = {}

  handleChange(e) {
    // e.preventDefault()
    // e.stopPropagation()
    let value = Number(e)
    // console.log(this.props.id)
    this.props.onChangeSelectedItems(this.props.id, value)
    // this.setState({ amount: value });
    // this.props.setCartAmount(value)
  }
  render() {
    const { maxQty, value } = this.props
    console.log(`value:: ${value} `)
    // const { amount } = this.props.cart
    // console.log(this.state)
    // console.log(this.props.cart)
    // background: 'rgba(255,255,255,1)',
    return (
      <div>
        <Mobile>
          <NumericInput
            style={{
              input: { width: '29vw', height: '7vw', pointerEvents: 'none' },
              plus: {
                background: this.props.theme.secondary,
                width: 2,
                height: 13,
                margin: '-6px 0 0 -1px',
                borderRadius: 1,
              },
              minus: {
                background: this.props.theme.secondary,
                width: 13,
                height: 2,
                margin: '-1px 0 0 -6px',
                borderRadius: 1,
              },
              'btnUp.mobile': {
                width: '6vw',
                height: '6vw',
                border: 'none',
                borderRadius: '5vw',
                background: this.props.theme.white,
                margin: '0px 0px 0px 0px',
              },
              'btnDown.mobile': {
                width: '6vw',
                height: '6vw',
                border: 'none',
                borderRadius: '5vw',
                background: this.props.theme.white,
                margin: '0px 0px 0px 0px',
              },
              'input.mobile': {
                background: '#ffefed',
                borderRadius: 30,
                border: 'none',
              },
            }}
            mobile={true}
            min={0}
            max={maxQty}
            value={value}
            inputMode="none"
            strict
            onFocus={() => false}
            onKeyDown={() => false}
            onSelect={() => false}
            onClick={() => false}
            onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
          />
        </Mobile>
        <Desktop>
          <NumericInput
            style={{
              input: { width: '150px', height: '34px' },
              plus: {
                background: this.props.theme.secondary,
                width: 2,
                height: 14,
                margin: '-7px 0 0 -1px',
                borderRadius: 1,
              },
              minus: {
                background: this.props.theme.secondary,
                width: 14,
                height: 2,
                margin: '-1px 0 0 -7px',
                borderRadius: 1,
              },
              'btnUp.mobile': {
                width: '30px',
                height: '30px',
                border: 'none',
                borderRadius: '30px',
                background: this.props.theme.white,
                margin: '0.5px 2px 0px 0px',
              },
              'btnDown.mobile': {
                width: '30px',
                height: '30px',
                border: 'none',
                borderRadius: '30px',
                background: this.props.theme.white,
                margin: '0.5px 0px 0px 2px',
              },
              'input.mobile': {
                background: '#ffefed',
                borderRadius: 30,
                border: 'none',
              },
            }}
            mobile={true}
            min={0}
            max={maxQty}
            value={value}
            inputMode="none"
            strict
            onFocus={() => false}
            onKeyDown={() => false}
            onSelect={() => false}
            onClick={() => false}
            onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
          />
        </Desktop>
      </div>
    )
  }
}

// onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
const mapStateToProps = () => ({})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(NumberInputToggleSmall))

// onChange={(valueAsNumber) => this.props.handleChange(valueAsNumber)}
