import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import styled, { withTheme } from 'styled-components'

import Button from '../../components/button'
import NumInputToggle from '../numberInputToggleEdit'

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex: 1;
  margin: 2vh 0 3vh;
  font-size: 0.75rem;

  @media only screen and (min-width: 451px) {
    margin: 10px 0 15px;
  }
`

const Image = styled.img`
  border: 1px solid ${(props) => props.theme.greyBorder}
  width: 10vw;
  height: 22vw;
  object-fit: cover;
  flex:1;
  @media only screen and (min-width: 451px) {
    width: 50px;
    height: 100px;
  }
`

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex: 3;
  justify-content: space-between;
  margin: 0 0 0 2vw;
  @media only screen and (min-width: 451px) {
    margin: 0 0 0 10px;
  }
`
const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`

const Row = styled.div`
  display: flex;
  align-items: center;
`

const RowInner = styled.div`
  marginright: '2vw';
`

const Size = styled.div`
  display: flex;
`
const Circle = styled.div`
  margin-right: 1vw;
  background: ${(props) => props.color};
  width: 3vw;
  height: 3vw;
  border: 1px solid ${(props) => props.color};
  border-radius: 3vw;
  @media only screen and (min-width: 451px) {
    width: 15px;
    height: 15px;
    border-radius: 15px;
  }
`

const Price = styled.div`
  margin-left: 20px;
`

// const DeleteButton = () => {
//   return (
//     <SmallButton>

//     </SmallButton>
//   )
// }

class EditProductListCard extends Component {
  state = {
    maxQty: this.props.maxQty,
    qty: 0,
  }

  componentDidMount() {
    this.setState({ qty: this.props.qty })
  }
  handleChange = (e) => {
    let value = Number(e)
    // console.log(value)
    this.setState({ ...this.state, qty: value })
  }

  handleLink = () => {
    const { productId, shopId } = this.props
    this.props.history.push({
      pathname: '/product',
      state: {
        from: this.props.location.pathname,
        shop_id: shopId,
        product_id: productId,
        all_product_id: this.props.locationState.all_product_id
      },
    })
  }

  render() {
    const { id, image, name, size, price, color, qty } = this.props
    const { maxQty } = this.state
    // console.log(`color (edit card) ::${JSON.stringify(color)}`)
    console.log(qty)
    return (
      <Container>
        <Image alt="product-image" src={image} />
        <Content>
          <div>{name}</div>
          <Size>
            <div>{`ขนาด : ${size}`}</div>
            {/* <Price>{`${price.toLocaleString('th')} THB`}</Price> */}
            <Price>{price}</Price>
          </Size>
          <Row>
            <RowInner>
              <Row>
                <Circle color={color.color_value} />
                <div>{color.color_name}</div>
              </Row>
            </RowInner>
          </Row>
          <NumInputToggle
            id={id}
            value={qty}
            max={maxQty}
            onChangeSelectedItems={this.props.onChangeSelectedItems}
            handleChange={this.handleChange}
          />
        </Content>
        <ButtonContainer>
          <Button
            delete
            style={{ margin: '0px 0px 2vw', flex: '1' }}
            onClick={() => this.props.handleConfirmDelete(id)}
          />

          <Button
            primaryCard
            color={this.props.theme.primary}
            style={{ flex: '2', fontSize: '0.70rem' }}
            onClick={() => this.handleLink()}>
            แก้ไขสินค้า
          </Button>
        </ButtonContainer>
      </Container>
    )
  }
}

export default withRouter(withTheme(EditProductListCard))
