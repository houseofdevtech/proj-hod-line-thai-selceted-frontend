import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { darken, lighten } from 'polished'

import { connect } from 'react-redux'
// import Grid from "../../components/grid";

const Container = styled.div`
  display: grid;
  // grid-template-columns: repeat(auto-fill, minmax(14vw, 5vw));
  grid-template-columns: repeat(auto-fill, minmax(max-content, 20vw));
  grid-auto-rows: 13vw;
  grid-gap: 4vw;
  // justify-content: center;

  width: 80vw;
  margin: 10px 0px 10px 10px;
  @media only screen and (min-width: 451px) {
    width: 60%;
    grid-template-columns: repeat(auto-fill, minmax(max-content, 50px));
    grid-auto-rows: 55px;
    grid-gap: 30px;
    margin: 10px 0px 10px 20px;
  }
`

const Circle = styled.div`
  background: ${(props) => `#fff`};
  ${(props) => {
    if (props.selected) {
      // console.log(`selected: ${props.selected}`)
      return css`
        background : ${(props) => `${props.theme.primary}`}
        color: #fff
        // border: 3px solid ${darken(0.1, `${props.theme.primary}`)};
        // border: 5px solid ${lighten(0.2, `${props.theme.primary}`)};
      `
    }
    // console.log(`selected: ${props.selected}`)
    return css`
      // border: 5px solid #${props.color};
      // border: 5px solid #fff;
    `
  }}
  ${(props) => {
    if (isSizeMoreThan3Char(props.name)) {
      return css`
        border-radius: 12vw;
        // width: 12vw;
        height: 12vw;
        line-height: 12vw;
        text-align: center;
        font-size: 5vw;
        padding: 0 10px;
        div:focus {
          border: 2px solid ${(props) => `${darken(0.1, `${props.theme.primary}`)}`};
        }
        @media only screen and (min-width: 401px) {
          // width: 46px;
          height: 46px;
          border-radius: 46px;
          font-size: 20px;
          text-align: center;
          line-height: 47px;
          padding: 0 10px 0;
        }
      `
    } else {
      return css`
        // border: none;
        // border-shadow:none;
        // border-radius: 40px;
        // width: 40px;
        // height: 40px;
        // line-height: 40px;
        border-radius: 12vw;
        width: 12vw;
        height: 12vw;
        line-height: 12vw;
        text-align: center;
        font-size: 5vw;

        div:focus {
          border: 2px solid ${(props) => `${darken(0.1, `${props.theme.primary}`)}`};
        }
        @media only screen and (min-width: 401px) {
          width: 46px;
          height: 46px;
          border-radius: 46px;
          font-size: 30px;
          text-align: center;
          line-height: 47px;
          // padding: 0px 0 0 0 ;
          // display: inline-block;
          // vertical-align: middle;
        }
      `
    }
  }}
`

const isSizeMoreThan3Char = (size) => {
  console.log(`isSizeMoreThan3Char :: ${size}`)
  console.log(`length :: ${size.length}`)
  return size.length > 3
}

// <Grid gap="5px" width="60px">
// </Grid>

class TemplateSize extends Component {
  _isUpdated = false

  state = {
    sizes: [],
    // sizes : [
    //   {
    //     key : [number],
    //     size: [size],
    //     selected: #false,
    //   }
    // ]
  }

  InitialState() {
    const { sizes } = this.props
    let result = []
    if (sizes) {
      if (sizes.length === 1) {
        const sizeDistribute = sizes.map((size, index) => {
          // remove cart for now, ==> when refresh there is no state to memory choice
          // if (size === this.props.cart.size) {
          //   return {
          //     key: index,
          //     size: size,
          //     selected: true,
          //   }
          // }
          return {
            key: index,
            size: size,
            selected: true,
          }
        })
        return sizeDistribute
      } else {
        const sizeDistribute = sizes.map((size, index) => {
          // remove cart for now, ==> when refresh there is no state to memory choice
          // if (size === this.props.cart.size) {
          //   return {
          //     key: index,
          //     size: size,
          //     selected: true,
          //   }
          // }
          return {
            key: index,
            size: size,
            selected: false,
          }
        })

        return sizeDistribute
      }
    }
    return result
  }

  componentDidMount() {
    this.setState({
      sizes: this.InitialState(),
    })
  }
  componentDidUpdate(prevProps) {
    //pre select size for one size
    if (!this._isUpdated && this.state.sizes.length === 1 && this.props.selectedSize === null) {
      // this._isUpdated = true
      // this.props.handleSelect('selectedSize', this.state.sizes[0].size)
    }
    // restate in size when color is changed
    if (prevProps.sizes !== this.props.sizes) {
      // console.log('size change :: true')
      this.setState({
        sizes: this.InitialState(),
      })
      if (this.InitialState().length === 1) {
        this.props.handleSelect('selectedSize', this.state.sizes[0].size)
      }
    }
    if (prevProps.selectedSize !== this.props.selectedSize && this.props.selectedSize === null) {
      // console.log('SelectedSize Change')
      this.setState({
        sizes: this.InitialState(),
      })
      if (this.InitialState().length === 1) {
        this.props.handleSelect('selectedSize', this.state.sizes[0].size)
      }
    }
  }

  handleChange = (e) => {
    // console.log(`id : ${e.target.id}`)
    // console.log(this.state.colors[0])
    const size = e.target.getAttribute('name')
    // const id = e.target.id
    // this.props.setCartSize(size)
    // console.log(this.props.cart)
    // console.log(id)

    // // Set Main Redux State
    // if (this.state.sizes[id].selected === true){
    //   const payload = {
    //     id : 1,         // single product add
    //     size: ''
    //   }
    //   this.props.setCartSize(payload)
    // } else {
    //   const payload = {
    //     id : 1,         // single product add
    //     size: size
    //   }
    //   this.props.setCartSize(payload)
    // }
    if (this.isSelectedSize(size)) {
      this.props.handleSelect('selectedSize', null)
    } else {
      this.props.handleSelect('selectedSize', size)
    }
    // Set Local State if the Item is selected or not
    this.setState((prevState) => ({
      sizes: prevState.sizes.map((obj) =>
        // make same data type and compare
        obj.size === size ? { ...obj, selected: !obj.selected } : { ...obj, selected: false }
      ),
    }))
  }

  isSelectedSize = (size) => {
    const filterSelectedSize = this.state.sizes.filter((obj) => obj.size === size)
    return filterSelectedSize[0].selected
  }

  render() {
    const { sizes } = this.state
    // console.log(`templateSize State :: ${JSON.stringify(this.state)}`)
    // console.log(`templateSize props selectedSize  :: ${this.props.selectedSize}}`)
    // console.log(this.props.cart)
    return (
      <Container>
        {sizes.map((obj, index) => (
          <Circle
            onClick={this.handleChange}
            key={index}
            id={index}
            name={obj.size}
            selected={obj.selected}>
            {obj.size}
          </Circle>
        ))}
      </Container>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplateSize)
