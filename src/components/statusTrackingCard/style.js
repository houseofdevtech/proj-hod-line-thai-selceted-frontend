import styled, { css } from 'styled-components'

export const TrackingCardContainer = styled.div`
  margin-top: 20px;
  padding: 10px 20px;
  padding-bottom: 10px;
  width: inherit;
  /* flex: 1; */
  background: #ffefed;
  border-radius: 10px;
  box-shadow: 3px 8px 15px rgba(0, 0, 0, 0.2);

  ${(props) => {
    if(props.status === 'waitpaying' ){
      return css`
        //red
        border-left: 10px solid #e75336;
      `
    }
    if(props.status === 'tracking' ){
      return css`
        //green
        border-left: 10px solid #55a18d;
      `
    }
  }}

`


export const Container = styled.div`
  display: flex;
  justify-content: space-between;
`
export const ContainerData = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  font-size: 0.9rem;
  font-weight: 500;
  flex: 1.5;
  color: ${(props) => props.theme.textPurple};
  span {
    font-size: 0.8rem;
    font-weight: 100;
    color: ${(props) => props.theme.textGrey};
  }

  // Adjust for iphone 5
  @media only screen and (max-width: 320px) {
    font-size: 0.8rem;
    font-weight: 500;
  }
`

export const ContainerButton = styled.div`
  display: flex;
  flex-direction: column;
  justify-content:center;
  align-items: center;
  // flex: 1;
  margin-left: 5vw;
  span {
    text-align: center;
    align-self: center;
    font-size: 0.7rem;
    color: ${(props) => props.theme.textGrey};
  }
`
export const ButtonFrame = styled.div`
  width: 30vw;
  height: 10vw;

  @media only screen and (min-width: 451px) {
    width: 110px;
    height: 35px;
  }
`

export const Divider = styled.div`
  border-top: 1px solid #d1ceda;
  margin: 5px 0;
  // padding: 10px 0px;
  // box-shadow: 2px 2px 5px grey;
`
export const HiddenBar = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

export const HiddenBarText = styled.div`
  color: ${(props) => props.theme.textPurple};
  font-size: 0.7rem;
  font-weight: 600;
`
export const HiddenBarArrow = styled.div`
  position: relative;
  left: 5px;
  border: solid ${(props) => props.theme.textPurple};
  border-width: 0 1.5px 1.5px 0;
  display: inline-block;
  padding: 3px;
  // transform: rotate(45deg);
  ${(props) => {
    if (props.visible) {
      return css`
        top: 0;
        transform: rotate(-135deg);
      `
    }
    return css`
      top: -1px;
      transform: rotate(45deg);
    `
  }}
`

export const DetailContainer = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: column;
  font-size: 0.9rem;
`

export const DetailAddress = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  font-size: 0.9rem;
  flex: 1.5;
  color: ${(props) => props.theme.textPurple};
  span {
    font-size: 0.8rem;
    color: ${(props) => props.theme.textGrey};
  }
`
export const DetailItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-size: 0.8rem;
  img {
    width: 60px;
    height: 60px;
    object-fit:cover;
  }
`

export const DetailItemPrice = styled.div`
  display: flex;
  align-items: flex-start;
  width: 100%;
  justify-content: space-between;
`

export const DetailItemPriceLabel = styled.div`
  position: relative;
  width: 70%;
  left: 10px;
`
export const DetailItemPricePrice = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`

export const DetailSummary = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const DetailSummaryItem = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  span {
    color: ${(props) => props.theme.textPurple};
  }
`
