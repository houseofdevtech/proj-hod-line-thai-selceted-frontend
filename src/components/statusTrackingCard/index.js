import React, { useState } from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'
// import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import moment from 'moment'

// import Woman1 from '../../images/woman1.png'

import Button from '../button'
import {
  TrackingCardContainer,
  Container,
  ContainerData,
  ContainerButton,
  ButtonFrame,
  Divider,
  HiddenBar,
  HiddenBarText,
  HiddenBarArrow,
  DetailContainer,
  DetailAddress,
  DetailItem,
  DetailItemPrice,
  DetailItemPriceLabel,
  DetailItemPricePrice,
  DetailSummary,
  DetailSummaryItem,
} from './style.js'

moment.updateLocale('th', {
  months: [
    'ม.ค.',
    'ก.พ.',
    'มี.ค.',
    'เม.ย.',
    'พ.ค.',
    'มิ.ย.',
    'ก.ค.',
    'ส.ค.',
    'ก.ย.',
    'ต.ค.',
    'พ.ย.',
    'ธ.ค.',
  ],
})

const PriceAndTotalContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 10px;

  @media only screen and (max-width: 375px) {
    flex-direction: column;
  }
`
const PriceContainer = styled.div`
  margin: 0px;

  @media only screen and (max-width: 375px) {
    margin: 5px;
  }
`

const TotalContainer = styled.div`
  margin-left: 20px;

  @media only screen and (max-width: 375px) {
    margin: 5px;
  }
`

const TrackingCard = (props) => {
  const [expandVisible, setExpandVisible] = useState(false)
  // **** Buyer tracking card type
  // **** from props.type can be
  // **** 1. waitpaying (red) no paid
  // **** 2. tracking (green) paid (pass responsible to seller to change status)
  const status = props.type

  // const { date_order, tracking_history, shop_id, bill_id, bank_items } = props.item
  const { date_order, tracking_history, shop_id, bill_id } = props.item
  const {
    bill_code,
    total,
    data_order,
    sum_qty,
    sum_price,
    tracking_price,
    shipping_type,
  } = props.item.tracking_items

  const { name, full_address } = props.item.tracking_items.data_user

  const date = moment.utc(date_order).locale('th')
  const formattedOrderDate = moment(date)
    .local()
    .format('DD MMMM YY  HH.mm น.')

  const handleClick = () => {
    if (status !== 'tracking') {
      // props.history.push('/slip',{shop_id:shop_id, bank_id: bank_items[0].id, bill_id: bill_id })
      props.history.push({
        pathname: '/paymentConfirm',
        state: {
          from: props.location.pathname,
          shop_id: shop_id,
          order_id: data_order.order_id,
          bill_id: bill_id,
        },
      })
      // this.setRedirect(url)
    } else {
      console.log('link to other tracking site (thaipost / kerry)')
      window.location.href = tracking_history.url
    }
  }
  // const isOrder = 'data_order' in props.item.tracking_items
  // console.log( `data_order :: ${isOrder}`)
  // console.log(props.item)
  return (
    <TrackingCardContainer status={status}>
      <Container>
        <ContainerData>
          <div>
            วันสั่งซื้อ
            <span style={{ marginLeft: '5px' }}> {formattedOrderDate}</span>
          </div>
          <span> {bill_code}</span>
          <PriceAndTotalContainer>
            <PriceContainer>จำนวนรวม x {sum_qty}</PriceContainer>
            <TotalContainer>{total} THB</TotalContainer>
          </PriceAndTotalContainer>
        </ContainerData>
        <ContainerButton>
          <ButtonFrame>
            <Button primaryCard onClick={() => handleClick()}>
              {status !== 'tracking' ? 'ชำระสินค้า' : 'ดูสถานะจัดส่ง'}
            </Button>
          </ButtonFrame>
          {/* <span style={{ marginTop: '10px' }}>
            {status !== 'tracking' ? 'ชำระสินค้าภายใน 23:59 น.' : 'ชำระสินค้าแล้ว'}
          </span> */}
          {status === 'tracking' && (
            <span style={{ marginTop: '10px' }}>
              {status !== 'tracking' ? '' : 'ชำระสินค้าแล้ว'}
            </span>
          )}
        </ContainerButton>
      </Container>
      <Divider />
      <HiddenBar style={{ marginTop: '5px' }}>
        <HiddenBarText onClick={() => setExpandVisible(!expandVisible)}>
          ดูรายละเอียดสินค้า
          <HiddenBarArrow style={{ margin: '0 10px 0' }} visible={expandVisible} />
        </HiddenBarText>
      </HiddenBar>
      {expandVisible && (
        <DetailContainer>
          <DetailAddress>
            ที่อยู่จัดส่ง
            <span>คุณ {name}</span>
            <span>{full_address}</span>
          </DetailAddress>
          <Divider />
          {data_order.order_items.map((obj) => {
            return obj.product_items.map((objItem) => {
              return (
                <div>
                  <DetailItem>
                    {/* <img src={Woman1} alt="woman1" /> */}
                    {objItem.color.color_img_path !== null ? (
                      <img src={objItem.color.color_img_path} alt="item-pic" />
                    ) : null}
                    <DetailItemPrice>
                      <DetailItemPriceLabel>
                        <div>{obj.product_name}</div>
                        <div>ขนาด : {objItem.size}</div>
                        <div>
                          <div> สี {objItem.color.color_name} </div>
                        </div>
                      </DetailItemPriceLabel>
                      <DetailItemPricePrice>
                        <div>x {objItem.qty}</div>
                        <div>{objItem.sum} THB</div>
                      </DetailItemPricePrice>
                    </DetailItemPrice>
                  </DetailItem>
                  <Divider />
                </div>
              )
            })
          })}

          <DetailSummary>
            <DetailSummaryItem>
              <span>รวม x {sum_qty}</span>
              <div>{sum_price.toLocaleString('th')} THB</div>
            </DetailSummaryItem>
            <DetailSummaryItem>
              <span>{shipping_type}</span>
              <div>{tracking_price.toLocaleString('th')} THB</div>
            </DetailSummaryItem>
          </DetailSummary>

          <Divider />
          <DetailSummaryItem>
            <span>รวมทั้งหมด</span>
            <div>{total.toLocaleString('th')} THB</div>
          </DetailSummaryItem>
        </DetailContainer>
      )}
    </TrackingCardContainer>
  )
}

TrackingCard.propTypes = {
  type: PropTypes.string,
}

export default withRouter(TrackingCard)
