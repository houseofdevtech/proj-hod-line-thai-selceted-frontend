import React, { Component } from 'react'
import styled from 'styled-components'

import Button from '../button'
const Container = styled.div`
  position :aboslute;
  display:flex;
  top: 50vh;
  width : 100%
  z-index: 200;
`
 


class ButtonPane extends Component {
  render() {
    return (
      <Container>
          <Button payment onClick={this.props.clickOne}>Trigger 1</Button>
          <Button payment onClick={this.props.clickTwo}>Trigger 2</Button>
          <Button payment onClick={this.props.clickThree}>Trigger 3</Button>
      </Container>
    )
  }
}


export default ButtonPane
