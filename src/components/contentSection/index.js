import React, { Component } from 'react'
import styled from 'styled-components'
import { opacify } from 'polished'
import _ from 'lodash'
import { Desktop, Mobile } from '../../utils/displayResponsive'

import Carousel from './carousel'
import Card from '../productCard'
import { withRouter } from 'react-router-dom'
import Button from '../button'

const Container = styled.div`
  z-index: 10;
  // padding: 0 0 0 20px;
  display: flex;
  height: max-content;
  // background: #fff;
  margin-bottom: 26px;

  scroll-snap-align: start;
`
const Column = styled.div`
  flex-direction: column;
  flex: 1;
`

const Title = styled.div`
  padding: 0 0 0 5px;
`
const ViewAll = styled.button`
  text-decoration: underline;
  margin-right: 4px;
`

const TitleFlex = styled.div`
  display: flex;
  justify-content: space-between;
`

const Divider = styled.div`
  border-top: 2px solid #a59d82;
  // padding: 5px 16px 5px 5px;
  margin: 5px 10px 5px 5px;
  box-shadow: 2px 2px 5px ${(props) => opacify(0.1, props.theme.grey)};
`

// This Component is for making
// Container for full width slide
const CardContainerCover = styled.div`
  // z-index: 20;
  // width: 100vw;
  // position: relative;
  // left: 50%;
  // right: 50%;
  // margin-left: -53vw;
  // margin-right: -50vw;
  @media only screen and (min-width: 451px) {
    overflow: hidden;
    overflow-y: scroll;
  }
`

const CardContainer = styled.div`
  // height: max-content;
  z-index: 20;
  position: relative;
  // margin: 0 0 0 0 px;
  padding: 5px 0 0 0;
  width: 96vw;
  height: 78vw;
  // height: 90vw;
  @media only screen and (min-width: 451px) {
    width: 450px;
    height: 350px;
    overflow-x: hidden;
  }
`

const CardContainerSliderHidden = styled.div`
  @media only screen and (min-width: 451px) {
    overflow: hidden;
  }
`

const Overlay = styled.div`
  // height: max-content;
  z-index: 20;
  position: relative;
  
  padding: 0 0 0 10px;
  width: 100%;
  display: flex;
  overflow-x: scroll;
  scroll-snap-type: x mandatory;
  // scroll-snap-points-x: repeat(100px);
  flex: 1 0 auto;
  &:before {
    z-index: 20;
    position: relative;
    // background-color: ${(props) => props.theme.secondary};
    background-color: #000;
    left: calc(50% + 1.5px);
    z-index: -1;
    content: '';
    height: 90%;
    // height: 100%;
    // height: max-content;
    width: 3px;
  }
  @media only screen and (min-width: 451px) {
    // overflow-x: hidden;
    overflow-x: scroll;
    &::-webkit-scrollbar {
      display: none;
      width: 0px !important;
      background: transparent;
    }
  }
`
// Emptycard as padding right for Card Container
const EmptyCard = styled.div`
  width: 133px;
  height: 100px;
  flex: 1 0 133px;
`

const ButtonAll = styled.button`
  border-color: transparent;
  background-color: transparent;
  text-decoration: underline;
`

class ContentSection extends Component {
  render() {
    const setting = {
      dragSpeed: 1.25,
      itemWidth: 185,
      itemHeight: 335,
      itemSideOffsets: 10,
    }
    console.log('province namexx', this.props.data)
    console.log('this.props.reg', this.props.reg)

    return (
      <Container>
        <Column>
          <TitleFlex>
            <Title>{this.props.data.provinceName}</Title>
            <ButtonAll
              onClick={() =>
                this.props.history.push(
                  `/product/${this.props.data.provinceName}/${this.props.data.provinceID}`
                )
              }>
              ดูทั้งหมด
            </ButtonAll>
          </TitleFlex>
          <Divider />
          <CardContainerCover>
            <CardContainer>
              <CardContainerSliderHidden>
                <Overlay>
                  {!this.props.reg ? (
                    <Mobile>
                      {this.props.data['0'].map((obj, index) => (
                        <Card mr10 key={index} data={obj} />
                      ))}
                      <EmptyCard />
                    </Mobile>
                  ) : (
                    <Mobile>
                      {this.props.data.length > 0 ? (
                        this.props.data['0'].map((obj, index) => (
                          <Card mr10 key={index} data={obj} />
                        ))
                      ) : (
                        <Card mr10 reg datax={this.props.data['0']} />
                      )}
                      <EmptyCard />
                    </Mobile>
                  )}

                  {!this.props.reg ? (
                    <Desktop>
                      <Carousel _data={this.props.data['0']} {...setting}>
                        {this.props.data['0'].map((obj, index) => (
                          <Card mr10 key={index} data={obj} />
                        ))}
                        <EmptyCard />
                      </Carousel>
                    </Desktop>
                  ) : (
                    <Desktop>
                      <Carousel _data={this.props.data} {...setting}>
                        {this.props.data.length > 0 ? (
                          this.props.data['0'].map((obj, index) => (
                            <Card mr10 key={index} data={obj} />
                          ))
                        ) : (
                          <Card reg datax={this.props.data['0']} />
                        )}
                        <EmptyCard />
                      </Carousel>
                    </Desktop>
                  )}
                </Overlay>
              </CardContainerSliderHidden>
            </CardContainer>
          </CardContainerCover>
        </Column>
      </Container>
    )
  }
}

export default withRouter(ContentSection)
