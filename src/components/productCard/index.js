import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { withRouter } from 'react-router-dom'
import Button from '../button'
import tsClassic from '../../images/ts_classic.jpg'
import tsSig from '../../images/ts_signature.jpg'
import tsUniq from '../../images/ts_uniqe.jpg'

const Container = styled.div`
  z-index: 20;
  position: relative;
  // position: absolute;
  display: flex;
  margin-left: 10px;
  flex-direction: column;
  flex:1 0 42vw;
  height: ${(props) => (props.vertical ? '18rem' : '100%')};
  margin-bottom: 10px;
  width: ${(props) => (props.vertical ? '157px' : 'auto')};
  ${(props) => {
    if (props.mr10) {
      return css`
        margin-right: 10px;
      `
    }
  }}
  border-radius: 5px;
  background: #fff;
  // background: ${(props) => props.theme.primary};
  img {
    z-index: 20;
    width: 100%;
    height: 40vw;
  }
  // justify-content: space-between;
  scroll-snap-align: center;
  box-shadow: 5px 5px 16px rgb(15,27,19,0.3);

  @media only screen and (min-width: 451px)  {
    justify-content: flex-start;
    height: calc(500px * 0.67);
    flex:1 0 calc(500px * 0.37);
    img {
      z-index: 20;
      // width: calc(500px * 0.37);
      width: 100%;
      height: calc(500px * 0.36);
      object-fit: cover;
    }
  }

`
const Type = styled.div`
  font-size: 12px;
  text-align: center;
`
const ButtonModify = styled.div`
  height: 30px;
  width: 90%;
`
const Title = styled.div`
  // padding: 4px 9px 0px 9px;
  font-size: 13px;
  color: #af001b;
  font-weight: 100px;

  @media only screen and (min-width: 451px) {
    font-size: 0.7rem;
  }
`

const RowTitle = styled.div`
  margin: 3px 0 0 0;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 1vw;

  @media only screen and (min-width: 451px) {
    margin-top: 5px;
  }
`
const RowPrice = styled.div`
  margin: 3px 0 0 0;
  display: flex;
  justify-content: center;
  align-items: center;

  @media only screen and (min-width: 451px) {
    margin-top: 10px;
  }
`
const Tel = styled.p`
  text-align: center;
  line-height: 0.2;
  font-size: 10px;
`
const OriginPrice = styled.div`
  // padding: 7px 9px;
  margin-right: 5px;
  font-size: 10px;
  font-weight: 100px;
  text-decoration: line-through;
  color: ${(props) => props.theme.grey};
`

const SalePrice = styled.div`
  // padding: 7px 9px;
  font-size: 10px;
  text-align: center;
  white-space: nowrap;
  width: 100px;
  overflow: hidden;
  text-overflow: ellipsis;
  font-weight: 100px;
  color: ${(props) => props.theme.active};
`

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  button {
    font-size: 10px;
    font-weight: 200px;
  }
  margin-bottom: 5px;

  @media only screen and (min-width: 451px) {
    height: 35px;
    margin-top: 10px;
  }
`
const IMG = styled.div`
  position: absolute;
  height: 30px !important;
  width: 30px !important;
  background-color: red;
`

class ProductCard extends Component {
  handleOpenLink = (href, shopId, productId) => {
    window.location.href = href
  }
  LogoType = (type) => {
    if (type === 'Thai SELECT CLASSIC 2020') {
      return <img className="logoImage" src={tsClassic} />
    } else if (type === 'Thai SELECT SIGNATURE 2020') {
      return <img className="logoImage" src={tsSig} />
    } else {
      return <img className="logoImage" src={tsUniq} />
    }
  }

  render() {
    const {
      storeName,
      provinceID,
      Pic,
      storeID,
      storeTel,
      originalPrice,
      link,
      shopLink,
      type,
      storeAddress,
      shopId,
    } = this.props.data ? this.props.data : ''
    console.log('typexxx', this.props.datax)
    return !this.props.reg ? (
      <Container vertical>
        <div>
          <IMG>{this.LogoType(type)}</IMG>
          <img alt={storeName} src={Pic} />
          <RowTitle style={{ marginTop: '1vw' }}>
            <Title>{storeName}</Title>
          </RowTitle>
          <RowPrice style={{}}>
            <SalePrice>{storeAddress.toLocaleString()}</SalePrice>
          </RowPrice>
          <Tel>
            <p>{storeTel}</p>
            <p>({type})</p>
          </Tel>
          <ButtonContainer>
            <ButtonModify>
              <Button primaryCard onClick={() => this.handleOpenLink(link, storeID, provinceID)}>
                รายละเอียดร้านค้า
              </Button>
            </ButtonModify>
          </ButtonContainer>
        </div>
      </Container>
    ) : this.props.datax.length > 0 ? (
      this.props.datax.map((e) => (
        <Container {...this.props.provinces}>
          <div>
            <IMG>{this.LogoType(e.type)}</IMG>
            <img alt={e.storeName} src={e.Pic} />
            <RowTitle style={{ marginTop: '1vw' }}>
              <Title>{e.storeName}</Title>
            </RowTitle>
            <RowPrice style={{}}>
              <SalePrice>{e.storeAddress.toLocaleString()}</SalePrice>
            </RowPrice>
            <Tel>
              <p>{e.storeTel}</p>
              <p>({e.type})</p>
            </Tel>
            <ButtonContainer>
              <ButtonModify>
                {
                  <Button
                    primaryCard
                    onClick={() => this.handleOpenLink(e.link, e.storeID, e.provinceID)}>
                    รายละเอียดร้านค้า
                  </Button>
                }
              </ButtonModify>
            </ButtonContainer>
          </div>
        </Container>
      ))
    ) : (
      <div />
    )
  }
}

export default withRouter(ProductCard)
