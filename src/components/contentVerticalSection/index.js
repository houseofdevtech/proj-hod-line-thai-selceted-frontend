import React, { Component } from 'react'
import styled from 'styled-components'
import { opacify } from 'polished'
import Card from '../productCard'

const Container = styled.div`
  z-index: 10;
  display: flex;
  // background: #fff;
  overflow: hidden;
`

const Column = styled.div`
  flex: 1;
`

const Title = styled.div`
  padding: 0 0 0 15px;
`

const Divider = styled.div`
  border-top: 2px solid #a59d82;
  // padding: 5px 16px 5px 5px;
  margin: 5px 10px 5px 5px;
  box-shadow: 2px 2px 5px ${(props) => opacify(0.1, props.theme.grey)};
`

const CardContainer = styled.div`
  // height: max-content;
  z-index: 20;
  position: relative;
  // margin: 0 0 0 0 px;
  padding: 5px 0 0 0;
  width: 96vw;
  height: 72vh;
  overflow-x: hidden;
  overflow-y: auto;

  @media only screen and (min-width: 451px) {
    width: 450px;
    // height: 400px;
    &::-webkit-scrollbar {
      display: none;
      width: 0px !important;
      background: transparent;
    }
  }
`

const Overlay = styled.div`
  // height: max-content;
  z-index: 20;
  position: relative;
  padding: 0 10px;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(38vw, 42vw));
  grid-auto-rows: 77vw;
  grid-gap: 10px;
  // scroll-snap-type: y mandatory;
  @media only screen and (min-width: 451px) {
    padding-left: 35px;
    grid-template-columns: repeat(auto-fill, minmax(160px, 185px));
    grid-auto-rows: 340px;
    grid-gap: 10px;
  }
`
// Emptycard as padding right for Card Container
const EmptyCard = styled.div`
  width: 133px;
  height: 100px;
  flex: 1 0 133px;
`

class ContentVerticalSection extends Component {
  state = {
    data: [],
    prevId: null,
  }

  render() {
    return (
      <div>
        <Card vertical data={this.props.data} />
      </div>
    )
  }
}

export default ContentVerticalSection
