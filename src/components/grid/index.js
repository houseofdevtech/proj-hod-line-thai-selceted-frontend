import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const CustomGrid = styled.div`
  margin-top: 0px;

  display: grid;
  grid-template-columns: repeat(
    auto-fit,
    minmax(${props => props.width}, 1vw)
  );
  grid-auto-rows: ${props => props.height || "25px"};
  grid-gap: ${props => props.gap || "30px"};

  // @media (max-width: 767px) {
  //   margin-top: 15px;
  //   grid-gap: 15px;
  // }
`;

class Grid extends Component {
  static propTypes = {
    height: PropTypes.string.isRequired,
    width: PropTypes.string.isRequired
  };

  render() {
    const { children } = this.props;
    return (
      <div>
        <CustomGrid {...this.props}>{children}</CustomGrid>
      </div>
    );
  }
}

export default Grid;
