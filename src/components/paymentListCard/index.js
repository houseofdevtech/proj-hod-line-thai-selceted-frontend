import React, { Component } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
`
const FlexSB = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 0 0 1vw;
  @media only screen and (min-width: 451px) {
    margin: 0 0 5px;
  }
`
const Circle = styled.div`
  background: ${(props) => `${props.color}`};
  width: 2vw;
  height: 2vw;
  border-radius: 2vw;
  align-self: center;
  margin-right: 2vw;
  @media only screen and (min-width: 451px) {
    width: 15px;
    height: 15px;
    border-radius: 15px;
    margin-right: 10px;
  }
`

const Size = styled.div`
  margin-right: 10vw;
  @media only screen and (min-width: 451px) {
    margin-right: 2vw;
  }
`

class PaymentListCard extends Component {
  render() {
    const { name, size, color, quantity, price } = this.props

    return (
      <Container>
        {/* {console.log(`${size}  ${color}`)} */}
        <FlexSB>
          <div>{name}</div>
          <div>x {quantity}</div>
        </FlexSB>
        <FlexSB>
          <div style={{ display: 'flex' }}>
            <Size>ขนาด : {size}</Size>
            <Circle color={color.color_value} />
            <div> {color.color_name}</div>
          </div>
          <div>{price.toLocaleString()} THB</div>
        </FlexSB>
      </Container>
    )
  }
}

export default PaymentListCard

// <div>{`name : ${name}`}</div>
//         <div>{`/size ${size}`}</div>
//         <div>{`/color: ${color}`}</div>
//         <div>{`/quantity: ${quantity}`}</div>
//         <div>{`/price ${price}`}</div>
