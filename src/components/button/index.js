import React, { Component } from "react";
import styled, { css } from "styled-components";
import { darken, opacify } from "polished";

// import BackArrow from '../../images/back-arrow.svg'
import Trash from "../../images/trash.svg";

const StyledButton = styled.button`
  border: none;
  display:flex;
  justify-content:center;
  align-items:center;
  
  // border-radius: 4px;
  // font-size: 16px;
  // font-size: 2.5vh;
  outline: none;
  cursor: pointer;
  text-align: center;
 
    ${(props) => {
      if (props.primary) {
        return css`
          background: ${props.theme.primary};
          border: ${props.theme.primary} solid 1px;
          border-radius: 20px;
          color: white;
          margin: 5px 1vw 5px 1vw;
          height: 40px;
          align-self: flex-end;
          font-size: max(2.5vw, 2.5vh);
          &:hover {
            background: ${darken(0.1, props.theme.primary)};
          }
          flex: 6;
          @media only screen and (min-width: 451px) {
            font-size: 1.3rem;
          }
        `;
      }
      if (props.inactive) {
        return css`
          background: ${props.theme.grey};
          border: ${props.theme.grey} solid 1px;
          border-radius: 20px;
          color: white;
          margin: 5px 1vw 5px 1vw;
          height: 40px;
          align-self: flex-end;
          font-size: max(2.5vw, 2.5vh);
          &:hover {
            background: ${darken(0.1, props.theme.grey)};
          }
          flex: 6;
          @media only screen and (min-width: 451px) {
            font-size: 1.3rem;
          }
        `;
      }

      if (props.primaryCard) {
        if (props.color) {
          return css`
            background: ${props.color};
            border: ${props.color} solid 1px;
            color: white;
            border-radius: 5px;
            &:hover {
              background: ${darken(0.1, props.theme.primary)};
            }
          `;
        }
        return css`
          background: ${props.theme.primary};
          border: ${props.theme.primary} solid 1px;
          border-radius: 5px;
          color: white;
          height: 100%;
          align-self: flex-end;
          &:hover {
            background: ${darken(0.1, props.theme.primary)};
          }
        `;
      }

      if (props.secondary) {
        return css`
          background: ${props.theme.transparent};
          border: ${props.theme.primary} solid 1px;
          border-radius: 5px;
          color: ${props.theme.primary};
          align-self: flex-end;
          box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
          &:hover {
            background: ${opacify(0.5, props.theme.primary)};
            color: ${props.theme.white};
          }
        `;
      }
      if (props.back) {
        return css`
          display: flex;
          justify-content: center;
          background-color: ${props.theme.transparent};
          color: white;
          border: ${props.theme.primary} solid 2.5px;
          border-radius: 20px;
          margin: 5px 2.5px 5px 5px;
          height: 40px;
          align-self: flex-end;
          &:hover {
            background: ${darken(0.1, props.theme.primary)};
          }
          flex: 1;
        `;
      }
      if (props.payment) {
        return css`
          background: ${props.theme.tertiary};
          border: ${props.theme.secondary} solid 1.5px;
          border-radius: 5px;
          color: ${props.theme.textPurple};
          // margin: 0 1vw 0 0;
          height: 40px;
          align-self: flex-end;
          font-size: ${props.fontSize ? props.fontSize : "0.75"}rem;
          &:hover {
            background: ${props.theme.primary};
            color: white;
          }
          flex: 1;
        `;
      }

      if (props.success) {
        return css`
          background-color: #64b705;
          color: white;
          &:hover {
            background: ${darken(0.1, "#64b705")};
          }
        `;
      }
      if (props.red) {
        return css`
          background-color: ${props.theme.red};
          color: white;
          &:hover {
            background: ${darken(0.1, props.theme.red)};
          }
        `;
      }
      if (props.transparent) {
        return css`
        background-color: ${props.theme.transparent};
        color: #a59d82 
        // border: 2px solid white;
        word-break: keep-all;
        padding: 0px;
        &:hover {
          // background: ${opacify(0.5, props.theme.transparent)};
          text-decoration: underline;
          color: ${props.theme.white};
          font-weight: 800;
          line-height: 1.5;
        }
      `;
      }
      if (props.delete) {
        return css`
        flex: 1
        background-color: ${props.theme.white};
        background-image: url(${Trash});
        background-repeat: no-repeat;
        background-position: center center;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
        border-radius: 5px;
      `;
      }

      return css`
        background-color: ${props.theme.grey};
        color: #a59d82;
        !important &:hover {
          background: ${darken(0.1, props.theme.grey)};
        }
      `;
    }}
    ${(props) => {
      if (props.width) {
        if (props.width === "max") {
          return css`
            width: max-content;
          `;
        }
        return css`
          width: ${props.width}px;
        `;
      }
      return css`
        width: 100%;
      `;
    }}
    ${(props) => {
      if (props.height) {
        return css`
          height: ${props.height}px;
        `;
      }
      return css`
        height: 100%;
      `;
    }}
    ${(props) => {
      if (props.left) {
        return css`
          border-radius: 30px 0px 0px 30px;
        `;
      }
      if (props.right) {
        return css`
          border-radius: 0px 30px 30px 0px;
        `;
      }
    }}
    // Selective Override
    ${(props) => {
      if (props.focus) {
        return css`
          &:focus {
            background-color: ${props.theme.secondary};
          }
        `;
      }
      if (props.selected) {
        return css`
          background-color: ${props.theme.primary};
          color: white;
        `;
      }
    }}
    ${(props) => props.full && "width: 100%;"};
`;

// background-image: url(${BackArrow});
// background-repeat: no-repeat;
// background-position: center center;

export class Button extends Component {
  static propTypes = {};

  render() {
    return <StyledButton {...this.props} />;
  }
}

export default Button;
