import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'
import ErrorBoundary from '../errorBoundary'
// import { Route, Switch, Redirect } from 'react-router-dom'

const TabContainer = styled.div`
  width: 80vw;
  height: max-content
  overflow: hidden;

  @media only screen and (min-width: 451px) {
    width: 400px;
    padding: 0 0 0 30px;
  }

`

const TabScroll = styled.div`
  box-sizing: content-box;
  overflow-x: auto;
  overflow-y: hidden;
`

const TabBar = styled.div`
  display: flex;
  border-radius: 4px;
  background-color: ${(props) => props.theme.transparent};

  position: relative;
  // top: 2px;
  z-index: 5;
  width: max-content;

  .active {
    color: ${(props) => props.theme.text};
    background-color: white;
  }

  @media (max-width: 768px) {
    padding-left: 1vw;
    // padding-right: 15px;
  }
`

const TabLabel = styled.div`
  font-size: 5vw;
  
  // background-color: ${(props) => props.theme.secondary};
  background-color: ${(props) => props.theme.white};
  color: ${(props) => props.theme.grey};
  // color:white;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  // width: max-content;
  // width: 5vw;
  // padding: 14px 20px;
  padding: 2px 20px 0px;
  width: 8vw;
  height: 9vw;
  text-align: center;
  font-size: 4vw;

  margin-right: 2vw;
  outline: none;
  transition: all 0.3s;
  border-bottom: 3px solid transparent;


  @media only screen and (min-width: 451px) {
    width: 50px;
    height: 50px;
    margin-right: 10px;
    padding: 5px 20px 0px;
    font-size: 2em;
  }


  :hover {
    color: ${(props) => props.theme.text};
    background-color: white;
    border-bottom: 3px solid transparent;
    cursor: pointer;
  }
`

const TabContent = styled.div`
  display: flex;
  flex-direction: column;

  div {
    font-size: 4vw;
  }

  span {
    padding: 0px 4px;
    margin-top: -5px;
    background: ${(props) => props.theme.tertiary};
    color: ${(props) => props.theme.textTab};
    border: none;
    border-radius: 4px;
    font-size: 2.5vw;
  }

  @media only screen and (min-width: 451px) {
    div {
      font-size: 0.5em;
    }
    span {
      padding: 0px 4px;
      margin-top: 1px;
      background: ${(props) => props.theme.tertiary};
      color: ${(props) => props.theme.textTab};
      border: none;
      border-radius: 4px;
      font-size: 0.4em;
      font-weight: 600;
    }
  }
`

const TabContentMiddle = styled.div`
  padding-top: 1vh;
  div {
    font-size: 4vw;
  }

  @media only screen and (min-width: 451px) {
    padding: 0px;
    div {
      display: inline-block;
      vertical-align: middle;
      font-size: 0.5em;
      // height: 20px;
    }
  }
`

class TabComponent extends Component {
  static propTypes = {}

  state = {}

  genTab = (item, index, activeTab) => (
    <TabLabel
      key={item.key}
      className={activeTab === index ? 'active' : ''}
      role="link"
      tabIndex={item.key}
      onClick={() => this.onClickFunc(index)}
      onKeyDown={() => {}}>
      {item.amount > 0 ? (
        <TabContent>
          <div>{item.label}</div>
          <span>{`x ${item.amount}`}</span>
        </TabContent>
      ) : (
        <TabContentMiddle>
          <div>{item.label}</div>
        </TabContentMiddle>
      )}
    </TabLabel>
  )

  onClickFunc = (index) => {
    const { onChangeTab, tabData } = this.props
    onChangeTab(tabData[index].key)
  }

  render() {
    // const { tabData, location, match, style } = this.props
    // const tabIndex = tabData.findIndex((item) => location.pathname === `${match.url}${item.path}`)
    const { tabData, style } = this.props
    const tabIndex = tabData.findIndex((item) => item.active === true)
    const activeTab = tabIndex > 0 ? tabIndex : 0

    return (
      <ErrorBoundary>
        <TabContainer>
          <TabScroll>
            <TabBar style={style}>
              {tabData.map((item, index) => {
                return this.genTab(item, index, activeTab)
              })}
            </TabBar>
          </TabScroll>
        </TabContainer>
        <TabContent>{activeTab !== null && tabData[activeTab].component}</TabContent>
      </ErrorBoundary>
    )
  }
}

TabComponent.propTypes = {
  tabData: PropTypes.array.isRequired,
  onChangeTab: PropTypes.func,
}

export default withRouter(TabComponent)
