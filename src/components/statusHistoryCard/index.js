import React, { useState } from 'react'
import PropTypes from 'prop-types'
// import { Link } from 'react-router-dom'
// import moment from 'moment'

import Woman1 from '../../images/woman1.png'

import {
  HistoryCardContainer,
  Container,
  ContainerData,
  Divider,
  HiddenBarText,
  HiddenBarArrow,
  DetailContainer,
  DetailAddress,
  DetailItem,
  DetailItemPrice,
  DetailItemPriceLabel,
  DetailItemPricePrice,
  DetailSummary,
  DetailSummaryItem,
} from './style'

const TrackingCard = (props) => {
  const [expandVisible, setExpandVisible] = useState(false)
  // **** Buyer tracking card type
  // **** from props.type can be
  // **** 1. waitpaying (red) no paid
  // **** 2. tracking (green) paid (pass responsible to seller to change status)
  const status = props.type

  // const { date_order } = props.item
  // const {
  //   bill_code,
  //   total,
  //   data_order,
  //   sum_qty,
  //   sum_price,
  //   tracking_price,
  // } = props.item.tracking_items
  // const { name } = props.item.tracking_items.data_user

  // const formattedOrderDate = moment(date_order).format('DD/MM/YY : HH.MM น.')
  // const totalPrice = total
  // const isOrder = 'data_order' in props.item.tracking_items
  // console.log( `data_order :: ${isOrder}`)
  // console.log(props.item)
  //  <div className={className}></div>
  return (
    <HistoryCardContainer status={status}>
      <Container>
        <ContainerData>
          <div>
            หมายเลขคำสั่งซื้อ
            <span style={{ marginLeft: '5px' }}> RE20190831sssssTxxxxxx</span>
          </div>
          <div>
            วันสั่งซื้อ
            <span style={{ marginLeft: '5px' }}> 22 ส.ค. 19 18:00 น.</span>
          </div>
          <div>
            วันที่ชำระสินค้า
            <span style={{ marginLeft: '5px' }}> 22 ส.ค. 19 18:00 น.</span>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
            <div style={{ display: 'flex' }}>
              <div>จำนวนรวม x 2</div>
              <span style={{ marginLeft: '10px' }}>฿ 1,730</span>
            </div>
            <HiddenBarText onClick={() => setExpandVisible(!expandVisible)}>
              ดูรายละเอียดสินค้า
              <HiddenBarArrow style={{ margin: '0 10px 0' }} visible={expandVisible} />
            </HiddenBarText>
          </div>
        </ContainerData>
      </Container>
      {expandVisible && (
        <DetailContainer>
          <Divider />
          <DetailAddress style={{marginTop: '10px'}}>
            ที่อยู่จัดส่ง
            <span>นางสาวสวย ช้อปปิ้งเก่ง</span>
            <span>123/456 คอนโดทวีสุข ตึกเอ แขวงบางซื่อ เขตบางซื่อ</span>
            <span>กรุงเทพฯ 10800</span>
          </DetailAddress>
          <Divider />
          <DetailItem>
            <img src={Woman1} alt="woman1" />
            <DetailItemPrice>
              <DetailItemPriceLabel>
                <div>Charlotte set</div>
                <div>ขนาด : S</div>
                <div>
                  <div> Color </div>
                </div>
              </DetailItemPriceLabel>
              <DetailItemPricePrice>
                <div>x 1</div>
                <div>฿ 850</div>
              </DetailItemPricePrice>
            </DetailItemPrice>
          </DetailItem>
          <Divider />
          <DetailItem>
            <img src={Woman1} alt="woman1" />
            <DetailItemPrice>
              <DetailItemPriceLabel>
                <div>Charlotte set</div>
                <div>ขนาด : S</div>
                <div>
                  <div> Color </div>
                </div>
              </DetailItemPriceLabel>
              <DetailItemPricePrice>
                <div>x 1</div>
                <div>฿ 850</div>
              </DetailItemPricePrice>
            </DetailItemPrice>
          </DetailItem>
          <Divider />
          <DetailSummary>
            <DetailSummaryItem>
              <span>รวม x 2</span>
              <div>฿ 1,700</div>
            </DetailSummaryItem>
            <DetailSummaryItem>
              <span>ลงทะเบียน</span>
              <div>฿ 30</div>
            </DetailSummaryItem>
          </DetailSummary>

          <Divider />
          <DetailSummaryItem>
            <span>รวมทั้งหมด</span>
            <div>฿ 1,750</div>
          </DetailSummaryItem>
        </DetailContainer>
      )}
    </HistoryCardContainer>
  )
}

TrackingCard.propTypes = {
  type: PropTypes.string,
}

export default TrackingCard

//  <Link
//             to={{
//               pathname: `/updateOrder/${props.item.tracking_id}`,
//               state: {
//                 trackingId: props.item.tracking_id,
//                 itemData: props.item,
//                 orderStatus: props.type, // TODO: Pass real order data
//               },
//             }}>
//             <button>
//               {props.type === 'announce' ? (
//                 <div className="tracking-card__actions__button__text">
//                   <div>แจ้งเลขพัสดุแล้ว</div>
//                   <div>EA 4731 2428 0 TH</div>
//                 </div>
//               ) : (
//                 'รอจัดส่ง'
//               )}
//             </button>
//           </Link>
