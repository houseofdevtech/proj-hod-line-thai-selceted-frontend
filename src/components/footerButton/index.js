import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import Button from '../button'

import { Footer, FooterContainer, FooterText, ButtonContainer } from '../../style/productStyle'

const FooterTextInner = styled.div`
  display: flex;
`

export const BackArrow = (props) => {
  // width="19.824"
  // height="15.871"
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="19.824"
      height="15.871"
      viewBox="0 0 19.824 15.871">
      <g id="back-arrow" transform="translate(0 0)">
        <g id="reply">
          <path
            id="Path_842"
            data-name="Path 842"
            d="M7.741,42.674v-3.51s0-.358-.2-.41-.417.158-.417.158L0,45.991l7.125,7.189s.21.3.417.274.2-.274.2-.274V49.2c5.53,0,8.955,1.3,11.719,5.172-.007-.033.211.332.363.225a5.261,5.261,0,0,0-.064-.536C18.734,49.055,15.429,43.772,7.741,42.674Z"
            transform="translate(0 -38.744)"
            fill={props.fill}
          />
        </g>
      </g>
    </svg>
  )
}

class FooterButtons extends Component {
  handleClick = () => {
    this.props.handlePrimaryButton()
  }

  handleInactiveClick = () => {
    this.props.handleInactiveButton()
  }

  handleBack = () => {
    this.props.handleBack()
  }

  isInternalLink = (props) => {
    // console.log(`location state ::${JSON.stringify(props.location.state)}`)
    return props.location.state && 'from' in props.location.state
  }

  isExternalLink = (props) => {
    // import queryString from 'query-string'
    // const qString = queryString.parse(this.props.location.search)
    // Actual querystring from address ===> ?shop_id=42&product_id=204
    // qString ===> {shop_id: 42, product_id:204  }
  }

  render() {
    const { labelPrimary, isActive, isMessage, message, backIconColor } = this.props
    return (
      <Footer height={isMessage ? 16 : 'false'} primaryColor={this.props.primaryColor}>
        {isMessage ? (
          // IF there is message to tell above footer button

          <FooterContainer height={14}>
            <FooterText>
              <FooterTextInner>{message}</FooterTextInner>
            </FooterText>
            <ButtonContainer>
              {this.isInternalLink(this.props) && (
                <Button footer back onClick={() => this.handleBack()}>
                  <BackArrow fill={backIconColor ? backIconColor : '#000000'} />
                </Button>
              )}
              {isActive ? (
                <Button primary onClick={() => this.handleClick()}>
                  {labelPrimary}
                </Button>
              ) : (
                <Button inactive onClick={() => this.handleInactiveClick()}>
                  {labelPrimary}
                </Button>
              )}
            </ButtonContainer>
          </FooterContainer>
        ) : (
          <FooterContainer>
            <ButtonContainer>
              {this.isInternalLink(this.props) && (
                <Button footer back onClick={() => this.handleBack()}>
                  <BackArrow fill={backIconColor ? backIconColor : '#000000'} />
                </Button>
              )}
              {isActive ? (
                <Button primary onClick={() => this.handleClick()}>
                  {labelPrimary}
                </Button>
              ) : (
                <Button inactive onClick={() => this.handleInactiveClick()}>
                  {labelPrimary}
                </Button>
              )}
            </ButtonContainer>
          </FooterContainer>
        )}
      </Footer>
    )
  }
}

export default withRouter(FooterButtons)
