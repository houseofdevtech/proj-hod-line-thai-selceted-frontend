import React, { Component } from 'react'
import { connect } from 'react-redux'

import styled, { css } from 'styled-components'
import { darken, lighten } from 'polished'
import checkedWhite from '../../images/checked-white.svg'
import checkedTheme from '../../images/checked-theme.svg'

const ContentContainer = styled.div`
  display: flex;
`
const Container = styled.div`
  display: grid;
  grid-gap: 1vw;
  // grid-template-columns: repeat(auto-fill, minmax(15vw, 20vw));
  // justify-content: flex-end;
  // background: #efefef;
  grid-template-columns: 20vw 20vw 20vw 20vw;
  grid-auto-rows: 40vw;
  margin: 5px 0px 0px -1vw;
  @media only screen and (min-width: 451px) {
    margin: 5px 0px 0px 5px;
    // grid-template-columns: 60px 60px 60px 60px;
    grid-template-columns: 5em 5em 5em 5em;
    grid-auto-rows: 150px;
  }
`
const ColorContainer = styled.div`
  display: flex;
  flex-direction: column;
  z-index: 10;
  // justify-content: center;
  // justify-self: center;
  // Fix grey Container
  background-color: transparent !important;
`

const Circle = styled.div`
  display:flex;
  justify-content:center;
  align-items:center;
  justify-self: center;

  background: ${(props) => `${props.color}`};
  border : ${(props) => {
    return props.color === '#ffffff' ? `1px solid ${props.theme.primary}` : `none`
  }}
   
  
  border-radius: 12vw;
  width: 12vw;
  height: 12vw;
  // border: none;
  // border-shadow:none;
  // width: 15vw;
  // height: 15vw;
  // div:focus {
  //   border: 2px solid ${(props) => `${darken(0.1, `${props.theme.primary}`)}`};
  // }
  @media only screen and (min-width: 451px) {
    width: 40px;
    height: 40px;
    border-radius: 40px;
  }
`

const CircleOut = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  align-self: center;

  ${(props) => {
    if (props.selected) {
      // console.log(`selected: ${props.selected}`)
      return css`
        border: 5px solid ${props.theme.primary};
        background: #fff;
      `
    }
    // console.log(`selected: ${props.selected}`)
    return css`
       border: 5px solid #fff;
       background: rgba(255, 255, 255, 0);
      // border: 5px solid ${lighten(0.2, `${props.theme.primary}`)};
    `
  }}

  border-radius: 14vw;
  width: 14vw;
  height: 14vw;
  @media only screen and (min-width: 451px) {
    width: 46px;
    height: 46px;
    border-radius: 46px;
  }
`

const Checked = styled.div`
  ${(props) => {
    if (props.selected) {
      // console.log(`selected: ${props.selected}`)
      return css`
        background: ${(props) => {
          return props.color === '#ffffff' ? `url(${checkedTheme})` : `url(${checkedWhite})`
        }};
        background-repeat: no-repeat;
        background-position: center;
      `
    }
    // console.log(`selected: ${props.selected}`)
    return css`
      background: ${(props) => `${props.color}`};
    `
  }}
  // border-radius: 30px;
  // width: 30px;
  // height: 30px;
  border-radius: 30px;
  width: 30px;
  height: 30px;
`

const Text = styled.div`
  font-size: 4vw;
  margin-top: 10px;
  align-self: center;
  text-align: center;
  @media only screen and (min-width: 451px) {
    font-size: 1em;
  }
`

class TemplateColor extends Component {
  _isUpdated = false

  state = {
    colors: [],
    // colors: [
    //   {
    //      key : [number]
    //      color : #color code
    //      name : 'color name'
    //      selected : #false
    //      productImgUrl: 'url'
    //   },
    // ]
    //
  }

  InitialState() {
    const { colors } = this.props
    let result = []
    if (colors) {
      if (colors.length === 1) {
        const colorDistribute = colors.map((color, index) => {
          // remove cart for now, ==> when refresh there is no state to memory choice
          // if (color.color_value === this.props.cart.color) {
          //   return {
          //     key: index,
          //     color_value: color.color_value,
          //     color_name: color.color_name,
          //     productImgUrl: color.color_img_path,
          //     selected: true,
          //   }
          // }
          return {
            key: index,
            color_value: color.color_value,
            color_name: color.color_name,
            productImgUrl: color.color_img_path,
            selected: true,
          }
        })

        return colorDistribute
      } else {
        const colorDistribute = colors.map((color, index) => {
          // remove cart for now, ==> when refresh there is no state to memory choice
          // if (color.color_value === this.props.cart.color) {
          //   return {
          //     key: index,
          //     color_value: color.color_value,
          //     color_name: color.color_name,
          //     productImgUrl: color.color_img_path,
          //     selected: true,
          //   }
          // }
          return {
            key: index,
            color_value: color.color_value,
            color_name: color.color_name,
            productImgUrl: color.color_img_path,
            selected: false,
          }
        })
        return colorDistribute
      }
    }
    return result
  }

  componentDidMount() {
    this.setState({
      colors: this.InitialState(),
    })
  }

  componentDidUpdate() {
    if (!this._isUpdated && this.state.colors.length === 1 && this.props.selectedColor ===null) {
      this._isUpdated = true
      this.props.handleSelect('selectedColor', this.state.colors[0])
    }
  }

  handleChange = (e) => {
    console.log(`id : ${e.target.id}`)
    // console.log(this.state.colors[0])
    const id = e.target.id
    // // Set Main Redux State
    // if (this.state.colors[id].selected === true) {
    //   const payload = {
    //     id : 1,     // single product add
    //     color: '',
    //     productImgUrl: '',
    //   }
    //   this.props.setCartColor(payload)
    // } else {
    //   const payload = {
    //     id : 1,     // single product add
    //     color: this.state.colors[id].color,
    //     productImgUrl: this.state.colors[id].productImgUrl,
    //   }
    //   this.props.setCartColor(payload)
    // }
    if (this.isSelectedColor(id)) {
      this.props.handleSelect('selectedColor', null)
    } else {
      this.props.handleSelect('selectedColor', this.state.colors[id])
    }
    // Set Local State if the Item is selected or not
    this.setState((prevState) => ({
      colors: prevState.colors.map((obj) =>
        // make same data type and compare
        String(obj.key).valueOf() === String(id).valueOf()
          ? { ...obj, selected: !obj.selected }
          : { ...obj, selected: false }
      ),
    }))
  }

  isSelectedColor = (id) => {
    const filterSelectedColor = this.state.colors.filter(
      (obj) => String(obj.key).valueOf() === String(id).valueOf()
    )
    // console.log(filterSelectedColor[0].selected)
    return filterSelectedColor[0].selected
  }

  render() {
    const { colors } = this.state
    // console.log(colors);
    console.log(`templateColor state :: ${JSON.stringify(this.state)}`)
    // console.log(this.props.cart)
    return (
      <ContentContainer>
        <Container>
          {colors.map((obj, index) => (
            //Add 'key' to fix warning

            <ColorContainer
              onClick={this.handleChange}
              key={index}
              id={index}
              color={obj.color_value}
              selected={obj.selected}>
              <CircleOut id={index} color={obj.color_value} selected={obj.selected}>
                <Circle id={index} color={obj.color_value} selected={obj.selected}>
                  <Checked id={index} color={obj.color_value} selected={obj.selected} />
                </Circle>
              </CircleOut>
              <Text id={index} name={obj.color_name}>
                {obj.color_name}
              </Text>
            </ColorContainer>
          ))}
        </Container>
      </ContentContainer>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(TemplateColor)
