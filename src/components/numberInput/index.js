import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import { withRouter } from 'react-router-dom'
import NumericInput from 'react-numeric-input'

const Container = styled.div`
  // display: flex;
  // // justify-content: space-between;
  // // align-content:space-between;
  // // align-items: center;
  // margin: 15px 0px 15px 10px;
  // flex: 1;
  // width: 100%;
`

class NumberInput extends Component {
  state = {
    inputValue: 1,
  }

  handleChange(e) {
    // e.preventDefault()
    // e.stopPropagation()
    let value = Number(e)
    // console.log(value)
    // this.setState({ inputValue: value });
    // this.props.setCartAmount(value)
    this.props.handleSelect('selectedvalue',value)
  }
  render() {
    return (
      <Container>
        <NumericInput
          {...this.props}
          style={{
            input: { width: '53vw', height: '9vw' },
            btnUp: { display: 'none' },
            btnDown: { display: 'none' },
          }}
          mobile={false}
          min={0}
          max={99999}
          value={this.props.value}
          inputmode="none"
          strict
          onKeyDown={() => false}
          onSelect={() => false}
          
        />
      </Container>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NumberInput))

// onChange={this.props.handleChange}