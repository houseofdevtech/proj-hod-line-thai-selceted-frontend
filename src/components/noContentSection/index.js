import React, { Component } from 'react'
import styled from 'styled-components'
import { opacify } from 'polished'

const Container = styled.div`
  z-index: 10;
  padding: 0 0 0 20px;
  display: flex;
  height: max-content;
  // background: #fff;
  margin-bottom: 26px;
`
const Column = styled.div`
  flex-direction: column;
  flex: 1;
`

const Title = styled.div`
  padding: 0 0 0 15px;
`

const Divider = styled.div`
  border-top: 2px solid ${(props) => props.theme.active};
  // padding: 5px 16px 5px 5px;
  margin: 5px 10px 5px 0px;
  box-shadow: 2px 2px 5px ${(props) => opacify(0.1, props.theme.grey)};
`

const CardContainer = styled.div`
  // height: max-content;
  position:relative;
  margin: 0 0 0 15px;
  // padding: 0 0 0 15px;
  height: 250px;
  width: 90vw;
  display: flex;
  overflow-x : auto;
\
  flex: 1 0 auto;
`




class NoContentSection extends Component {
  render() {
    return (
      <Container>
        <Column>
          <Divider />
          <CardContainer>
             ยังไม่มีร้านค้า ในภาคนี้ค่ะ
          </CardContainer>
        </Column>
      </Container>
    )
  }
}

export default NoContentSection
