import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import { connect } from 'react-redux'
// import 'react-responsive-carousel/lib/styles/carousel.min.css'
import './carousel.min.css'
import { Carousel } from 'react-responsive-carousel'

import AskSellerButton from '../buttonAsk'

// import Carousel, { Modal, ModalGateway } from 'react-images'
// import ReactBnbGallery from 'react-bnb-gallery'// cannot zoom
// import ImageView from 'react-native-image-view'// react native cannot compile
// import { Lightbox } from 'react-modal-image'   // Modal cannot Zoom
// import Img from 'react-image'                  // Not Work
// import Lightbox from 'react-image-lightbox'    // Not work
import Modal from './modal'

// import IconPrev from '../../images/ic-prev.svg'
// import IconNext from '../../images/ic-next.svg'

const CoverContainer = styled.div`
  position: relative;
`

const Container = styled.div`
  // height: 50vh;
  
  img {
    // width: 100%;
    width: inherit;
    ${(props) => {
      if (props.small) {
        return css`
          height: 25vh;
          object-fit: cover;

          @media only screen and (min-width: 451px) {
            height: 200px;
          }
        `
      }

      return css`
        // height: 52vh;
        object-fit: contain;
        @media only screen and (min-width: 451px) {
          // height: 400px;
        }
      `
    }}
    

    // background-image: url(${(props) => props.pic});
  }
`

// const PrevButton = styled.button`
//   position: absolute;
//   background: transparent;
//   border: none;
//   width: 15vw;
//   height: 30vh;
//   z-index: 200;
//   top: 10vh;
//   left: 0;
//   img {
//     width: 50px;
//     height: 50px;
//   }
// `

// const NextButton = styled.button`
//   position: absolute;
//   background: transparent;
//   border: none;
//   width: 15vw;
//   height: 30vh;
//   z-index: 200;
//   top: 10vh;
//   right: 0;
//   img {
//     width: 50px;
//     height: 50px;
//   }
// `

const ImageFrame = ({ name, image, onClick }) => (
  <div onClick={onClick}>
    <img alt={name} src={image} />
  </div>
)

// const findHeightFromStyledText = (cssText) => {
//   const first = cssText.indexOf("height:") + 8
//   const last = cssText.length - 1
//   const height = cssText.substring(first,last)
//   return height
// }

class CarouselSection extends Component {
  state = {
    currentSlide: 0,
    isOpen: false,
    height: null,
  }
  _isUpdated = false
  ref = React.createRef()

  // test(e) {
  //   console.log("Ref callback");
  // }

  componentDidMount() {
    this._isUpdated = true
    this.forceUpdate()
    // this.updateHeight()
    // setTimeout(this.updateHeight(), 3000)
    // console.log('carousel didmount')
    // console.log(this.ref.current.itemsWrapperRef.style)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this._isUpdated) {
      this._isUpdated = false
      // setTimeout(this.updateHeight(), 2000)
      this.updateHeight()
    }
  }
  // updateHeight2 = () => {
  //   const current = this.ref.current
  //   const height = current.getBoundingClientRect().height
  //   const roundHeight = Math.round(height)
  //   this.props.setCarouselHeight(roundHeight)
  //   this.setState({ height: roundHeight })
  // }

  updateHeight = () => {
    const height = this.ref.current.itemsWrapperRef.style.height
    // console.log(height)
    // console.log(this.ref.current.itemsWrapperRef.style)
    const current = this.ref.current
    const width = current.itemsRef[0].clientWidth + 'px'
    // console.log(current)
    // console.log(rect)
    if (height === 'auto') {
      // this.forceUpdate()
      this.props.setCarouselHeight(width)
      this.setState({ height: width })
    } else {
      this.props.setCarouselHeight(height)
      this.setState({ height: height })
    }
  }

  next = () => {
    this.setState((state) => ({
      currentSlide: state.currentSlide + 1,
    }))
  }

  prev = () => {
    this.setState((state) => ({
      currentSlide: state.currentSlide - 1,
    }))
  }

  updateCurrentSlide = (index) => {
    const { currentSlide } = this.state
    if (currentSlide !== index) {
      this.props.setPictureIndex(index)
      this.setState({
        currentSlide: index,
      })
      this._isUpdated = true
    } else {
      this.props.setPictureIndex(index)
    }
  }

  handleZoom(e, index) {
    console.log(`handlezoom: ${index}  `)
    this.setState({
      photoIndex: index,
      isOpen: true,
    })
    e.preventDefault()
  }

  closeModal = () => {
    this.setState({
      isOpen: false,
    })
  }

  render() {
    const { isOpen, photoIndex } = this.state
    const { small, images } = this.props
    // const { pictureIndex } = this.props.ask
    // console.log(`ask: ${JSON.stringify(this.props.ask)}`)
    // console.log(`pictureIndex: ${this.state.currentSlide}`)
    // console.log(`pictureIndex: ${this.props.ask.pictureIndex}`)
    // console.log(`pictureIndex: ${picIndex}`)
    // import AskSellerButton from '../../components/askButton'
    // console.log(`Carousel state: ${JSON.stringify(this.state)}`)
    // {!small ? <AskSellerButton link="/ask"  /> : <AskSellerButton top link="/ask"  />}
    return (
      <CoverContainer>
        <AskSellerButton
          top={small ? true : false}
          link="/ask"
          data={this.props.data}
          carouselHeight={this.props.carouselHeight}
        />
        <Container small={small}>
          {/* <PrevButton onClick={this.prev}>
            <img alt="prev" src={IconPrev} />
          </PrevButton>
          <NextButton onClick={this.next}>
            <img alt="next" src={IconNext} />
          </NextButton> */}
          <Carousel
            ref={this.ref}
            showArrows={false}
            showStatus={false}
            showThumbs={false}
            centerMode={false}
            dynamicHeight={true}
            infiniteLoop={false}
            selectedItem={this.state.currentSlide}
            onChange={this.updateCurrentSlide}
            height="320"
            swipeable={true}
            swipeScrollTolerance={4}
            emulateTouch={true}
            verticalSwipe="natural">
            {images &&
              images.map((image, index) => (
                <ImageFrame
                  name={`${index}`}
                  key={index}
                  image={image}
                  onClick={(e) => this.handleZoom(e, index)}
                />
              ))}
          </Carousel>
          {isOpen && (
            <Modal images={images} index={photoIndex} isShow={isOpen} onClose={this.closeModal} />
          )}
        </Container>
      </CoverContainer>
    )
  }
}

const mapStateToProps = (state) => ({
  ask: state.ask,
})

const mapDispatchToProps = (dispatch) => {
  return {
    setPictureIndex: dispatch.ask.setPictureIndex,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarouselSection)
