import React, { Component } from 'react'
// import CustomModal from '../customModal'
import { mobileModalStyle } from './customAddNewModal'

import ReactImageLightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'
import ErrorBoundary from '../errorBoundary'

// import big1 from '../../images/big1.jpg'

class ModalImages extends Component {
  state = {
    index: -1,
  }

  componentDidMount() {
    const { index } = this.props
    this.setState({
      index: index,
    })
  }

  renderContent = () => {
    const { onClose, images } = this.props
    const { index } = this.state
    // console.log(this.state.index)
    return (
      <div>
        <ReactImageLightbox
          mainSrc={images[index]}
          nextSrc={images[(index + 1) % images.length]}
          prevSrc={images[(index + images.length - 1) % images.length]}

          onCloseRequest={onClose}
          onMovePrevRequest={() =>
            this.setState({
              index: (index + images.length - 1) % images.length,
            })
          }
          onMoveNextRequest={() =>
            this.setState({
              index: (index + 1) % images.length,
            })
          }
          enableZoom={true}
          reactModalStyle={mobileModalStyle}
        />
      </div>
    )
  }

  render() {
    return <ErrorBoundary>{this.renderContent()}</ErrorBoundary>
  }
}

export default ModalImages
