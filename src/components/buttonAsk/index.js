import React, { Component } from 'react'
import styled, { css } from 'styled-components'
import CommentIcon from '../../images/comment.svg'
import { withRouter, Link } from 'react-router-dom'

const Container = styled.div`
  position: absolute;
  z-index: 100;

  display: flex;
  justify-content: center;
  align-items: center;

  background: ${(props) => props.theme.primary};
  ${(props) => {
    if (props.top) {
      return css`
        right: 5vw;
        top: 15vh;
        @media only screen and (min-width: 451px) {
          left: 25em;
          top: 6em;
        }
      `
    }
    return css`
      right: 5vw;
      top: calc(${props.carouselHeight} - 14%);
      // top: 42vh;

      @media only screen and (min-width: 451px) {
        right: 5vw;
        top: calc(${props.carouselHeight} - 14%);
      }
    `
  }}
  border-radius: 12vw;
  border: none;
  width: 12vw;
  height: 12vw;
  cursor: pointer;

  // img {
  //   // background-size:100%;
  //   width: 7vw;
  //   height: 7vw;
  //   object-fit: fill;
  // }

  @media only screen and (min-width: 451px) {
    border-radius: 4em;
    width: 4em;
    height: 4em;
    img {
      width: 2em;
      height: 2em;
    }
  }
`
const Icon = styled.img`
  width: 7vw;
  height: 7vw;
  object-fit: fill;
`

// const Icon = styled.div`
//   background: url(${CommentIcon});
//   background-repeat: no-repeat;
//   background-position: center;
// `

class AskButton extends Component {
  render() {
    const { link, top, carouselHeight } = this.props

    return (
      <Link
        to={{
          pathname: link,
          state: { from: this.props.location.pathname, data: this.props.data },
        }}>
        <Container top={top} carouselHeight={carouselHeight}>
          {/* {console.log(this.props.pos)} */}
          <Icon alt="CommentIcon" src={CommentIcon} />
        </Container>
      </Link>
    )
  }
}

export default withRouter(AskButton)
