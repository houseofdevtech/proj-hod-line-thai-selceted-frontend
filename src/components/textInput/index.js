import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Container, Label, Input, ErrorMsg } from './styles'

export class TextInput extends Component {
  static defaultProps = {
    type: 'text',
  }

  static propTypes = {
    value: PropTypes.string,
    error: PropTypes.string,
    containerStyle: PropTypes.object,
    onChange: PropTypes.func,
    type: PropTypes.string,
  }

  render() {
    const {
      value,
      error,
      containerStyle,
      onChange,
      name,
      type,
      placeholder,
      hideLabel,
      required,
      altName,
      ...props
    } = this.props
    return (
      <Container style={containerStyle} {...props}>
        <Label htmlFor={name}>
          {hideLabel || (
            <div className="color">
              {altName || name}
              {required ? <span style={{ color: 'red' }}> *</span> : null}
            </div>
          )}
          <Input
            id={name}
            type={type}
            name={name}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            error={error}
            {...props}
          />
          {error && <ErrorMsg>{error}</ErrorMsg>}
        </Label>
      </Container>
    )
  }
}

export default TextInput
