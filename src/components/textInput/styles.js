import styled from 'styled-components'

export const Container = styled.div`
  margin-bottom: 10px;

  .color {
    color: ${(props) => (props.altColor ? props.theme.grey : '')};
  }
`
export const Label = styled.label`
  color: ${(props) => props.theme.grey};
  font-size: 12px;
  text-transform: capitalize;
`
export const ErrorMsg = styled.p`
  margin-top: 5px;
  color: red;
  font-size: 12px;
`
export const Input = styled.input`
  border: none;
  height: 30px;
  font-size: 14px;
  border-bottom: 1px solid rgba(194, 194, 194, 0.5);
  transition: border 0.3s;
  color: ${(props) => props.theme['greyish-brown']};
  ${(props) => props.full && 'width: 100%;'}
  ${(props) => props.error && 'border-bottom: 1px solid red;'}

  :focus {
    border-bottom: 1px solid ${(props) => props.theme.primaryButton};
    ${(props) => props.error && 'border-bottom: 1px solid red;'}
    outline: none;
  }
`
