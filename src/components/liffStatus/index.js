import React, { Component } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  // position: relative;
`

const Content = styled.div`
  height: 15vh;
  padding: 5vw;
`

class LiffStatus extends Component {
  render() {
    const { displayName, userId } = this.props.data

    return (
      <Container>
        {this.props.show === 'on' &&
          (displayName !== '' ? (
            <Content>
              <div> {`displayname :${displayName}`} </div>
              <div> {`userId [555]:${userId}`} </div>
            </Content>
          ) : (
            <Content> LIFF init failed </Content>
          ))}
      </Container>
    )
  }
}

export default LiffStatus
