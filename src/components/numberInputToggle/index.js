import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled, { withTheme } from 'styled-components'

import { withRouter, Link } from 'react-router-dom'
import NumericInput from 'react-numeric-input'

import { Desktop, Mobile } from '../../utils/displayResponsive'

const Container = styled.div`
  display: flex;
  // justify-content: space-between;
  // align-content:space-between;
  // align-items: center;
  margin: 15px 0px 15px 10px;
  flex: 1;
  width: 100%;
`

const HyperLinkText = styled.div`
  font-size: 0.8em;
  align-self: center;
  text-align: center
  flex:1;
  cursor: pointer;
  a:visited {
    color : ${(props) => `${props.theme.text}`};
  }
`

class NumberInputToggle extends Component {
  state = {
    amount: 0,
  }

  handleChange(e) {
    // e.preventDefault()
    // e.stopPropagation()
    let value = Number(e)
    // console.log(value)
    // const payload = {
    //   id: 1, // single product add
    //   amount: value,
    // }
    this.props.handleSelect('selectedAmount', value)
    // this.props.setCartAmount(payload)
    // this.props.setCartTotal()
    this.setState({ amount: value })
  }
  render() {
    const { maxAmount, selectedAmount } = this.props
    // const { amount } = this.state
    const amount = selectedAmount
    // console.log(this.state)
    // this.props.cartBig.product[0] && console.log(this.props.cartBig.product[0])

    return (
      <Container>
        <Mobile>
          <NumericInput
            style={{
              input: { width: '40vw', height: '11vw', pointerEvents: 'none'  },
              plus: {
                background: 'rgba(255,255,255,1)',
                width: 2,
                height: 14,
                margin: '-7px 0 0 -1px',
                borderRadius: 1,
              },
              minus: {
                background: 'rgba(255,255,255,1)',
                width: 14,
                height: 2,
                margin: '-1px 0 0 -7px',
                borderRadius: 1,
              },
              'btnUp.mobile': {
                width: '10vw',
                height: '10vw',
                border: 'none',
                borderRadius: '30px',
                background: this.props.theme.primary,
                margin: '0.5px 2px 0px 0px',
              },
              'btnDown.mobile': {
                width: '10vw',
                height: '10vw',
                border: 'none',
                borderRadius: '30px',
                background: this.props.theme.primary,
                margin: '0.5px 0px 0px 2px',
              },
              'input.mobile': {
                background: this.props.theme.secondary,
                borderRadius: 30,
                border: 'none',
              },
            }}
            mobile={true}
            min={0}
            max={maxAmount}
            value={amount}
            inputMode="none"
            strict
            onFocus={() => false}
            onKeyDown={() => false}
            onSelect={() => false}
            onClick={() => false}
            onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
          />
          {amount > 1 && (
            // Add Logic for size and color
            //&& (size !== '' && color !== '')
            <HyperLinkText>
              <Link to={{ pathname: '/detail', state: { from: this.props.location.pathname } }}>
                เลือกสินค้าแบบละเอียด
              </Link>
            </HyperLinkText>
          )}
        </Mobile>
        <Desktop>
          <NumericInput
            style={{
              input: { width: '200px', height: '40px' },
              plus: {
                background: 'rgba(255,255,255,1)',
                width: 2,
                height: 14,
                margin: '-7px 0 0 -1px',
                borderRadius: 1,
              },
              minus: {
                background: 'rgba(255,255,255,1)',
                width: 14,
                height: 2,
                margin: '-1px 0 0 -7px',
                borderRadius: 1,
              },
              'btnUp.mobile': {
                width: '36px',
                height: '36px',
                border: 'none',
                borderRadius: '36px',
                background: this.props.theme.primary,
                margin: '0.5px 2px 0px 0px',
              },
              'btnDown.mobile': {
                width: '36px',
                height: '36px',
                border: 'none',
                borderRadius: '36px',
                background: this.props.theme.primary,
                margin: '0.5px 0px 0px 2px',
              },
              'input.mobile': {
                background: this.props.theme.secondary,
                borderRadius: 30,
                border: 'none',
              },
            }}
            mobile={true}
            min={0}
            max={maxAmount}
            value={amount}
            inputMode="none"
            strict
            onFocus={() => false}
            onKeyDown={() => false}
            onSelect={() => false}
            onClick={() => false}
            onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
          />
          {amount > 1 && (
            // Add Logic for size and color
            //&& (size !== '' && color !== '')
            <HyperLinkText>
              <Link to={{ pathname: '/detail', state: { from: this.props.location.pathname } }}>
                เลือกสินค้าแบบละเอียด
              </Link>
            </HyperLinkText>
          )}
        </Desktop>
      </Container>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(withRouter(NumberInputToggle)))
