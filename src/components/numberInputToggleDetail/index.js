import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withTheme } from 'styled-components'

// import { Link } from 'react-router-dom'
import NumericInput from 'react-numeric-input'

import { Desktop, Mobile } from '../../utils/displayResponsive'

class NumberInputToggleDetail extends Component {
  state = {}

  handleChange(e) {
    // e.preventDefault()
    // e.stopPropagation()
    let value = Number(e)
    // console.log(this.props.id)
    this.props.onChangeSelectedItems(this.props.id, value)
    // this.setState({ amount: value });
    // this.props.setCartAmount(value)
  }
  render() {
    const { maxQty, value } = this.props
    console.log(`value:: ${value} `)
    // const { amount } = this.props.cart
    // console.log(this.state)
    // console.log(this.props.cart)
    // background: 'rgba(255,255,255,1)',
    return (
      <div>
        <Mobile>
          <NumericInput
            style={{
              input: { width: '45vw', height: '11vw', pointerEvents: 'none' },
              plus: {
                background: 'rgba(255,255,255,1)',
                width: 2,
                height: 14,
                margin: '-7px 0 0 -1px',
                borderRadius: 1,
              },
              minus: {
                background: 'rgba(255,255,255,1)',
                width: 14,
                height: 2,
                margin: '-1px 0 0 -7px',
                borderRadius: 1,
              },
              'btnUp.mobile': {
                width: '10vw',
                height: '10vw',
                border: 'none',
                borderRadius: '30px',
                background: this.props.theme.primary,
                margin: '0.5px 2px 0px 0px',
              },
              'btnDown.mobile': {
                width: '10vw',
                height: '10vw',
                border: 'none',
                borderRadius: '30px',
                background: this.props.theme.primary,
                margin: '0.5px 0px 0px 2px',
              },
              'input.mobile': {
                background: this.props.theme.secondary,
                borderRadius: 30,
                border: 'none',
              },
            }}
            mobile={true}
            min={0}
            max={maxQty}
            value={value}
            inputMode="none"
            strict
            onFocus={() => false}
            onKeyDown={() => false}
            onSelect={() => false}
            onClick={() => false}
            onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
          />
        </Mobile>
        <Desktop>
          <NumericInput
            style={{
              input: { width: '200px', height: '40px' },
              plus: {
                background: 'rgba(255,255,255,1)',
                width: 2,
                height: 14,
                margin: '-7px 0 0 -1px',
                borderRadius: 1,
              },
              minus: {
                background: 'rgba(255,255,255,1)',
                width: 14,
                height: 2,
                margin: '-1px 0 0 -7px',
                borderRadius: 1,
              },
              'btnUp.mobile': {
                width: '36px',
                height: '36px',
                border: 'none',
                borderRadius: '36px',
                background: this.props.theme.primary,
                margin: '0.5px 2px 0px 0px',
              },
              'btnDown.mobile': {
                width: '36px',
                height: '36px',
                border: 'none',
                borderRadius: '36px',
                background: this.props.theme.primary,
                margin: '0.5px 0px 0px 2px',
              },
              'input.mobile': {
                background: this.props.theme.secondary,
                borderRadius: 30,
                border: 'none',
              },
            }}
            mobile={true}
            min={0}
            max={maxQty}
            value={value}
            inputMode="none"
            strict
            onFocus={() => false}
            onKeyDown={() => false}
            onSelect={() => false}
            onClick={() => false}
            onChange={(valueAsNumber) => this.handleChange(valueAsNumber)}
          />
        </Desktop>
      </div>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => {
  return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(withTheme(NumberInputToggleDetail))
