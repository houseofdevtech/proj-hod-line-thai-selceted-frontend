import React, { Component } from 'react'
import Select from 'react-select'

class Selector extends Component {
  render() {
    return (
      <div>
        <Select 
          {...this.props} 
          options={this.props.options} 
          onChange={this.props.onChange} />
      </div>
    )
  }
}

export default Selector
