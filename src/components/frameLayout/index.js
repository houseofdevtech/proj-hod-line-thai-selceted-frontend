import React, { Component } from 'react'
import {
  Container,
  SubContainer,
  TopBackground,
  TopContainer,
  ThemesContainer,
  SelectedContainer,
  BottomRowContainer,
  BottomColumnContainer,
  BottomLeftFiller,
  BottomLeftBackground,
  BottomLeftSquare,
  BottomLeftShadow,
  FlexCenterHorizon,
} from '../../style/generalStyle'

class FrameLayout extends Component {
  render() {
    return (
      <Container 
        primaryColor={this.props.primaryColor} 
        overflowHidden={this.props.overflowHidden}
        frameHeight={this.props.frameHeight}
        >
        <SubContainer>
          <TopBackground secondaryColor={this.props.secondaryColor}>
            <TopContainer topHeight={this.props.topHeight} primaryColor={this.props.primaryColor}>
              {this.props.topContent}
            </TopContainer>
          </TopBackground>

          <BottomColumnContainer paddingBottom={this.props.paddingBottom}>
            <BottomRowContainer>
              <BottomLeftFiller secondaryColor={this.props.secondaryColor}>
                <BottomLeftBackground secondaryColor={this.props.secondaryColor}>
                  <BottomLeftSquare primaryColor={this.props.primaryColor}>
                    <BottomLeftShadow />
                  </BottomLeftSquare>
                </BottomLeftBackground>
              </BottomLeftFiller>
              <ThemesContainer primaryColor={this.props.primaryColor}>
                <SelectedContainer
                  bodyHeight={this.props.bodyHeight}
                  heightDesktop={this.props.heightDesktop}
                  secondaryColor={this.props.secondaryColor}
                  marginRight={this.props.marginRight}>
                  <FlexCenterHorizon
                    justifyContent={this.props.justifyContent}
                    marginLeft={this.props.marginLeft}
                    marginRight={this.props.marginRight}>
                    {this.props.children}
                  </FlexCenterHorizon>
                </SelectedContainer>
              </ThemesContainer>
            </BottomRowContainer>
          </BottomColumnContainer>
          {this.props.bottomContent}
        </SubContainer>
      </Container>
    )
  }
}

export default FrameLayout

//<FooterFiller prDimaryColor={this.props.primaryColor} />
