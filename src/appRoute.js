import React, { Component } from 'react'

import { BrowserRouter, Route, Switch } from 'react-router-dom'

import {
  handleCloseApp,
  handleBack,
  handleSubmitAskSeller,
  handleSubmitOrder,
  handleRequestQRcode,
  handleMockupFlow,
  handleAsk1,
  handleAsk2,
  handleAsk3,
} from './appCommand'

// import All Pages
import AppControl from './appControl'
import CheckPaymentPage from './containers/checkPaymentPage'

import ProductPage from './containers/productPage'
import DetailPage from './containers/detailPage'
import AskPage from './containers/askPage'

import ProductListPage from './containers/productListPage'

import AddressPage from './containers/addressPage'
import PaymentPage from './containers/paymentPage'
import EditProductPage from './containers/editProductPage'

import TrackingPage from './containers/trackingPage'

import MyStatusPage from './containers/myStatusPage'

import PaymentConfirmPage from './containers/paymentConfirmPage'

import SlipPage from './containers/slipPage'

import LoadingPage from './containers/loadingRedirectPage'

import CardCheckoutPage from './containers/cardCheckoutPage'

import NotFoundPage from './views/notFoundPage'

import EmptyOrderPage from './containers/emptyOrderPage'

class AppRoute extends Component {
  state = {}

  componentDidMount() {
    this.setState({ ...this.props.stateData })
  }

  render() {
    return (
      <div>
        <BrowserRouter basename="/buyer">
          {/* <BrowserRouter basename="/"> */}
          {/* {console.log(process.env.PUBLIC_URL)} */}
          <Switch>
            <Route path="/" exact render={() => <AppControl />} />
            <Route
              path="/product/:name/:id"
              exact
              render={() => (
                <ProductPage
                  handleCloseApp={handleCloseApp}
                  handleSubmitOrder={handleSubmitOrder}
                  handleRequestQRcode={handleRequestQRcode}
                  handleMockupFlow={handleMockupFlow}
                  handleBack={handleBack}
                  state={this.state}
                />
              )}
            />
            <Route
              path="/ask"
              exact
              render={() => (
                <AskPage
                  handleBack={handleBack}
                  handleSubmitAskSeller={handleSubmitAskSeller}
                  handleAsk1={handleAsk1}
                  handleAsk2={handleAsk2}
                  handleAsk3={handleAsk3}
                  state={this.state}
                />
              )}
            />
            <Route
              path="/detail"
              exact
              render={() => (
                <DetailPage
                  handleBack={handleBack}
                  handleSubmitOrder={handleSubmitOrder}
                  handleRequestQRcode={handleRequestQRcode}
                  handleMockupFlow={handleMockupFlow}
                  state={this.state}
                />
              )}
            />
            <Route
              path="/list"
              exact
              render={() => <ProductListPage handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/edit"
              exact
              render={() => (
                <EditProductPage handleBack={handleBack} handleCloseApp={handleCloseApp} />
              )}
            />
            <Route
              path="/payment"
              exact
              render={() => <PaymentPage handleBack={handleBack} handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/paymentConfirm"
              exact
              render={() => (
                <PaymentConfirmPage handleBack={handleBack} handleCloseApp={handleCloseApp} />
              )}
            />
            <Route
              path="/address"
              exact
              render={() => <AddressPage handleBack={handleBack} handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/tracking"
              exact
              render={() => <TrackingPage handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/mystatus"
              exact
              render={() => <MyStatusPage handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/notFound"
              exact
              render={() => <NotFoundPage handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/loading"
              exact
              render={() => <LoadingPage handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/checkpayment"
              exact
              render={() => <CheckPaymentPage handleCloseApp={handleCloseApp} />}
            />
            <Route path="/slip" exact render={() => <SlipPage handleCloseApp={handleCloseApp} />} />
            <Route
              path="/checkout"
              exact
              render={() => <CardCheckoutPage handleCloseApp={handleCloseApp} />}
            />
            <Route
              path="/emptyorder"
              exact
              render={() => <EmptyOrderPage handleCloseApp={handleCloseApp} />}
            />
          </Switch>
        </BrowserRouter>
      </div>
    )
  }
}

export default AppRoute

// path={`${process.env.PUBLIC_URL}/`}
// path={`${process.env.PUBLIC_URL}/form`}
// path={`${process.env.PUBLIC_URL}/ask`}
// path={`${process.env.PUBLIC_URL}/loading`}
