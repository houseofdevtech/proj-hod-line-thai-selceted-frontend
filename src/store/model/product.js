import request from '../../utils/urlRequest'

export const product = {
  state: {
    provinces: [],
    regions: [],
    tenStore: null,
  },
  reducers: {
    // handle state changes with pure functions

    // set with Local Data (inputData/productData)
    setProductData(state, payload) {
      // console.log(payload)
      return {
        ...state,
        provinces: payload,
      }
    },
    setRegions(state, payload) {
      return {
        ...state,
        regions: payload,
      }
    },
    setTenStore(state, payload) {
      console.log(payload)
      return {
        ...state,
        tenStore: payload,
      }
    },
    // set with Real API
    setProduct(state, payload) {
      // console.log(payload)
      return {
        ...state,
        product: payload,
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    async getProduct(payload, rootState) {
      //  await new Promise(resolve => setTimeout(resolve, 1000))
      //  dispatch.count.increment(payload)
      const res = await request.get(
        `/api/products/shop/${payload.shop_id}?product_id=${payload.product_id}`
      )
      if (res) {
        // console.log(res)
        if ('message' in res.data) {
          console.log(res.data.message)
        } else {
          console.log('getProduct')
          console.log(res.data)
          this.setProduct(res.data)
        }
      }
      return res.data
    },
  }),
}
