import request from '../../utils/urlRequest'

// const liff = window.liff

export const checkPayment = {
  state: {
    check: null,
    // check: payload [] {}
    // output: payload //api yim .... rootState.checkPayment.output
  },
  reducers: {
    // handle state changes with pure functions
    setCheckState(state, payload) {
      console.log(`setCheckState :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        check: payload, // true , false  {check : ..., data: .... } check: pay_by_type
      }
    },
    setOrderID(state, payload) {
      console.log(payload)
      return {
        ...state,
        order_id: payload,
      }
    },
    setOutputState(state, payload) {
      console.log(`setCheckState :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        output: payload,
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions

    // API 34/2 -get_bill_by_order_id
    async getOrder(payload, rootState) {
      try {
        request.interceptors.request.use((request) => {
          console.log('Starting Request', request)
          return request
        })
        request.interceptors.response.use((response) => {
          console.log('Response:', response)
          return response
        })

        const res = await request.get(`/api/order/buyer/${payload.chattyUserId}?shop=45`)
        console.log(res.data)
        console.log(res.data)
        this.setOrderID(res.data.order_id)

        const res_2 = await request.get(`/api/getbill_by_order/${res.data.order_id}`)
        if (res_2) {
          if (!res_2.data.message) {
            console.log(res.data && res.data.is_success === false)
            if (res_2.data.pay_by_type === 'qr' && res.data.is_success === false) {
              console.log()

              this.setCheckState(res_2.data.pay_by_type)
              // return true
            }
            if (res_2.data.pay_by_type === 'bank' && res.data.is_success === false) {
              this.setCheckState(res_2.data.pay_by_type)
            }
            if (res_2.data.pay_by_type === 'credit_card' && res.data.is_success === false) {
              this.setCheckState(res_2.data.pay_by_type)
            } else {
              // return false
            }

            // if (res.data.payment_options.qr_code === true){

            // }

            // write the condition to check payment_type condition
            return true
          } else {
            this.setError(res.data.message)
            console.log(`error api 29 /order ${res.data.message}`)
            return false
          }
        } else {
          console.log('getOrder:: Cannot fetch the data')
          return false
        }
      } catch (err) {
        console.log(err)
      }
    },

    // =====================================================
    // async postOrder(payload, rootState) {
    //   //  await new Promise(resolve => setTimeout(resolve, 1000))
    //   //  dispatch.count.increment(payload)

    //   request.interceptors.request.use((request) => {
    //     console.log('Starting Request', request)
    //     return request
    //   })
    //   request.interceptors.response.use((response) => {
    //     console.log('Response:', response)
    //     return response
    //   })
    //   // const orderString = rootState.order.toString()
    //   // const orderString = JSON.stringify(rootState.order)
    //   // console.log(`orderString :: ${orderString}`)
    //   // const res = await request.post(`/order`,orderString)
    //   const res = await request.post(`/api/order`, rootState.order)
    //   if (res) {
    //     console.log(res.data)
    //     liff.closeWindow()
    //     // this.setProduct(res.data)
    //   }
    //   // return res.data
    // },

    // async editOrder(payload, rootState) {
    //   //  await new Promise(resolve => setTimeout(resolve, 1000))
    //   //  dispatch.count.increment(payload)

    //   request.interceptors.request.use((request) => {
    //     console.log('Starting Request', request)
    //     return request
    //   })
    //   request.interceptors.response.use((response) => {
    //     console.log('Response:', response)
    //     return response
    //   })
    //   // const orderString = rootState.order.toString()
    //   // const orderString = JSON.stringify(rootState.order)
    //   // console.log(`orderString :: ${orderString}`)
    //   // const res = await request.post(`/order`,orderString)
    //   const res = await request.post(`/api/order`, rootState.order)
    //   if (res) {
    //     console.log(res.data)
    //     return true
    //     // this.setProduct(res.data)
    //   }
    //   // return res.data
    // },
  }),
}
