import request from '../../utils/urlRequest'

const liff = window.liff

export const bills = {
  state: {
    // "user_id": 3,
    // "order_id": 3,
    // "shipping_type_id": "2",
    // "payment_type": "qr"
  },
  reducers: {
    setBills(state, payload) {
      console.log(`setBills :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions

    // API 30 - create_bills (get)
    async postBills(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      console.log(rootState.bills)
      const res = await request.post(`/api/bills`, rootState.bills)
      if (res) {
        console.log(res.data)
        if ('message' in res.data && res.data.message.includes('Not completed')) {
          this.setBills({ reRender: true })
        } else {
          liff.closeWindow()
        }
      }
      // return res.data
    },

    async postBillsRedirect(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      console.log(rootState.bills)
      const res = await request.post(`/api/bills`, rootState.bills)
      const res_bill = await request.get(`/api/bills/${res.data.bill_id}`)
      if (res) {
        if ('message' in res.data && res.data.message.includes('Not completed')) {
          this.setBills({ reRender: true })
        } else {
          console.log(`RESDATA BILL ::  ${JSON.stringify(res.data)}`)
          console.log(`RESDATA GET BILL ::  ${JSON.stringify(res_bill.data.bill_code)}`)
          const tracking_id = res.data.tracking_id
          // console.log(
          //   `ENDPOINT CREDITCARD ::  line://app/1653922588-dAwYl7qa?amount=${payload.amount}&product_name=${payload.product_name}&tracking=${tracking_id}&bill_code=${res_bill.data.bill_code}`
          // )
          // window.location.href = `https://liff.line.me/1653922588-dAwYl7qa/?amount=${payload.amount}&product_name=${payload.product_name}&tracking=${tracking_id}`
          //* REDIRECT TO PAYMENT ENDPOINT
          window.location.href = `line://app/1653922588-dAwYl7qa?amount=${payload.amount}&product_name=${payload.product_name}&tracking=${tracking_id}&bill_code=${res_bill.data.bill_code}`
        }
      }
      // return res.data
    },

    // API 30/1 - put_bills (get)
    async putBills(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      console.log(rootState.bills)
      const res = await request.put(`/api/bills`, rootState.bills)
      if (res) {
        console.log(res.data)
        const isError =
          ('message' in res.data && res.data.message.includes('Not completed')) ||
          ('canBuy' in res.data && res.data.canBuy === false)
        if (isError) {
          console.log(`isError :: ${res.data.message}`)
          this.setBills({ reRender: true })
        } else {
          liff.closeWindow()
        }
        // this.setProduct(res.data)
      }
      // return res.data
    },

    // API 30/1 - put_bills (get)
    async putBillsRedirect(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      console.log(rootState.bills)
      const res = await request.put(`/api/bills`, rootState.bills)
      const res_bill = await request.get(`/api/bills/${res.data.bill_id}`)
      if (res) {
        console.log(res.data)
        const isError =
          ('message' in res.data && res.data.message.includes('Not completed')) ||
          ('canBuy' in res.data && res.data.canBuy === false)
        if (isError) {
          console.log(`isError :: ${res.data.message}`)
          this.setBills({ reRender: true })
        } else {
          console.log(res.data)
          console.log(`RESDATA BILL ::  ${JSON.stringify(res.data)}`)
          console.log(`RESDATA GET BILL ::  ${JSON.stringify(res_bill.data.bill_code)}`)
          const tracking_id = res.data.tracking_id
          // console.log(
          //   `ENDPOINT CREDITCARD ::  line://app/1653922588-dAwYl7qa?amount=${payload.amount}&product_name=${payload.product_name}&tracking=${tracking_id}&bill_code=${res_bill.data.bill_code}`
          // )
          // window.location.href = `https://liff.line.me/1653922588-dAwYl7qa/?amount=${payload.amount}&product_name=${payload.product_name}&tracking=${tracking_id}`
          //* REDIRECT TO PAYMENT ENDPOINT
          window.location.href = `line://app/1653922588-dAwYl7qa?amount=${payload.amount}&product_name=${payload.product_name}&tracking=${tracking_id}&bill_code=${res_bill.data.bill_code}`
        }
        // this.setProduct(res.data)
      }
      // return res.data
    },
  }),
}
