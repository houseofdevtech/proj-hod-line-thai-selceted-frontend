import request from '../../utils/urlRequest'
import _ from 'lodash'
const liff = window.liff

export const order = {
  state: {
    // "user_id": 3,
    //  "product_id": 239,
    //  "items": [
    //      { "id": 1420, "qty": 11},
    //      { "id": 1421, "qty": 22},
    //      { "id": 1422, "qty": 55}
    //    ]
  },
  reducers: {
    // handle state changes with pure functions
    setOrderUserId(state, payload) {
      // console.log(payload)
      return {
        ...state,
        user_id: payload,
      }
    },

    setOrderID(state, payload) {
      console.log(payload)
      return {
        ...state,
        order_id: payload,
      }
    },

    setOrder(state, payload) {
      console.log(`setOrder :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        ...payload,
      }
    },
    deleteOrderItems(state, payload) {
      // console.log(`deleteOrderItems `)
      let { items, ...res } = state
      // console.log(res)
      return res
    },
    addOldOrder(state, payload) {},
    addOrder(state, payload) {
      return {
        ...state,
        ...payload,
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    async postOrder(payload, rootState) {
      //  await new Promise(resolve => setTimeout(resolve, 1000))
      //  dispatch.count.increment(payload)

      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      // const orderString = rootState.order.toString()
      // const orderString = JSON.stringify(rootState.order)
      // console.log(`orderString :: ${orderString}`)
      // const res = await request.post(`/order`,orderString)
      const res = await request.post(`/api/order`, rootState.order)
      if (res) {
        console.log(res.data)
        liff.closeWindow()
        // this.setProduct(res.data)
      }
      // return res.data
    },
    async editOrderByUserId(payload, rootState){
      console.log(JSON.stringify(`BODY OLD: ${JSON.stringify(rootState.order)}`))

      const res_OldOrder = await request.get(`/api/order/buyer/${payload.chattyUserId}?shop=45`)

      console.log(JSON.stringify(res_OldOrder.data.order_items))
      // console.log(_.isEmpty(res_OldOrder.data))
      let old_data = []

      let result_data = []

      !_.isEmpty(res_OldOrder.data) && res_OldOrder.data.order_items.map((product) => {
        product.product_items.map((product_item) => {
          let obj = {
            id: product_item.sku_id,
            qty: product_item.qty,
          }
          old_data.push(obj)
          return null
        })
        return null
      })
  

      let rootState_item = rootState.order.items
      rootState_item.map((items_root) => {
        old_data.map((items_old) => {
          if (items_old.id === items_root.id) {
            items_root.qty += items_old.qty
          } else {
            result_data.push(items_old)
          }
          return null
        })
        result_data.push(items_root)
        return null
      })

      rootState.order.items = result_data
      console.log(JSON.stringify(`BODY NEW: ${JSON.stringify(rootState.order)}`))


      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      const res = await request.post(`/api/order`, rootState.order)
      if (res) {
        console.log(res.data)
        liff.closeWindow()
        return true
      }
    },

    async editOrder(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      const res = await request.post(`/api/order`, rootState.order)
      if (res) {
        console.log(res.data)
        return true
      }
    }, // API 29 - get_order
    async getCheckOrder(payload, rootState) {
      const res = await request.get(`/api/order/buyer/${payload.chattyUserId}`)
      if (res) {
        if (!res.data.message) {
          if ('is_success' in res.data && !res.data.is_success) {
            return true
          } else {
            return false
          }
        } else {
          this.setError(res.data.message)
          console.log(`error api 29 /order ${res.data.message}`)
          return false
        }
      } else {
        console.log('getOrder:: Cannot fetch the data')
        return false
      }
    },
    // API 49 - all_shop_bank (get)
    async getBank(payload, rootState) {
      const res = await request.get(`/api/shop_bank_all/${payload}`)
      if (res) {
        this.setBank(res.data)
        return true
      } else {
        console.log('getBank:: Cannot fetch the data')
        return false
      }
    },
  }),
}
