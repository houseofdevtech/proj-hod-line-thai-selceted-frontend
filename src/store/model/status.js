import request from '../../utils/urlRequest'

export const status = {
  state: {
    // "user_id": 3,
    // "order_id": 3,
    // "shipping_type_id": "2",
    // "payment_type": "qr"
  },
  reducers: {
    setTracking(state, payload) {
      console.log(`setTracking :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        tracking: payload,
      }
    },
    setError(state, payload) {
      console.log(`setError :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        error: payload,
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions

    // API 36/1 - all_tracking_buyer (get)
    async getTracking(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      // console.log(rootState.bills)
      const res = await request.get(`/api/tracking/buyer/${payload}`)
      if (res) {
        console.log(res.data)
        if (!res.data.message) {
          const addedTypeData = res.data.map((obj) => {
            let type = ''
            if (obj.status === 'ยังไม่ได้ชำระสินค้า') {
              type = 'waitpaying'
            } else {
              type = 'tracking'
            }
            return {
              ...obj,
              type: type,
            }
          })
          this.setTracking(addedTypeData)
        } else {
          this.setError(res.data.message)
          console.log(`error api 36/1 /tracking_buyer ${res.data.message}`)
        }
      }
      // return res.data
    },
  }),
}
