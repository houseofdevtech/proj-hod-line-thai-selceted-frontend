import request from '../../utils/urlRequest'
import picBaseURL  from '../../utils/urlPicture'

// const liff = window.liff

export const slip = {
  state: {
    // Render Input
    // ใช้ api34 : get_slip_bills
    // ชื่อธนาคาร ธนาคารกสิกร
    // ชื่อร้าน
    // เลขบัญชี
    // รวมทั้งสิ้น
    // เราสามารถ get bills จาก
    // body_data: {
    //   "user_id": 15,    //  user_id from app
    //   "bank_id": 20,    //  ?? need to ask api49 or api33
    //   "bill_id": 62,    //
    // }
    // slip_img_path : {
    // }
    render :null,
    img_path: null,
    isRender: false,
    isMounted: false,
  },
  reducers: {
    setSlip(state, payload) {
      return {
        ...state,
        ...payload,
      }
    },
    isSlipRenderCompleted(state, payload) {
      return {
        ...state,
        isRender:
          state.render !== null &&
          'bankNumber' in state.render &&
          'bankName' in state.render &&
          'shopName' in state.render &&
          'total' in state.render,
      }
    },
    setSlipRender(state, payload) {
      console.log(`setSlipRender:: ${JSON.stringify(payload)} `)
      return {
        ...state,
        render: {
          ...state.render,
          ...payload,
        },
      }
    },
    setSlipBody(state, payload) {
      console.log(`setSlipBody:: ${JSON.stringify(payload)} `)
      return {
        ...state,
        body_data: {
          ...payload,
        },
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions





    // API 32 - get_bills
    async getBills(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      const res = await request.get(`/api/bills/${payload.bill_id}`)
      if (res) {
        // console.log(res.data)
        if ('message' in res.data) {
          console.log(res.data.message)
        } else {
          this.setSlipRender({
            total: res.data.total,
          })
          this.isSlipRenderCompleted()
        }
      }
    },
    // API 46 - get_shop
    async getShopDetail(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      const res = await request.get(`/api/shop_setting/${payload.shop_id}`)
      if (res) {
        console.log(res.data)
        if ('message' in res.data) {
          console.log(res.data.message)
        } else {
          this.setSlipRender({
            shopName: res.data.shop_name,
          })
          this.isSlipRenderCompleted()
        }
      }
    },
    // API 50 - get_shop_bank
    async getShopBank(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      const res = await request.get(`/api/bank/${payload.bank_id}`)
      if (res) {
        console.log(res.data)
        if ('message' in res.data) {
          console.log(res.data.message)
        } else {
          this.setSlipRender({
            bankNumber: res.data.bank_number,
            bankName: res.data.bank_name,
          })
          this.isSlipRenderCompleted()
        }
      }
    },

    // API 33 - create_slip
    async postSlip(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })

      const res = await request.post(`/api/bills_slip/shop/${payload.shop_id}`, payload.formData, {
        headers: {
          // 'content-type': 'application/json',
          'content-type': 'multipart/form-data',
        },
      })
      if (res) {
        console.log(res.data)
        if ('id' in res.data) {
          this.getSlip(res.data.id)
        } else {
          console.log(res.data.message)
        }
      }
    },
    // API 34 - get_slip_bill
    async getSlip(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      const res = await request.get(`/api/bills_slip/bill/${payload}`)
      if (res) {
        console.log(res.data)
        this.setSlip({img_path: picBaseURL + res.data.img_path})
        // liff.closeWindow()
        // this.setProduct(res.data)
      }
      return res.data
    },
  }),
}
