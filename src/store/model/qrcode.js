import request from '../../utils/urlRequest'

const liff = window.liff
export const qrcode = {
  state: {
    bill_id: null,
    qrUrl: null,
    line_id: null,
  },
  reducers: {
    setQRcodeState(state,payload) {
      console.log(`setQRcodeState :${JSON.stringify(payload)}`)
      return {
        ...state,
        ...payload,
      }
    },

  },
  effects: (dispatch) => ({
    //API34/3 get_qr
    async getQrcode(payload, rootState) {
      //get order_id
      const res = await request.get(`/api/order/buyer/${payload.chattyUserId}?shop=45`)

      //get bill_id
      const res_2 = await request.get(`/api/getbill_by_order/${res.data.order_id}`)

      const res_3 = await request.get(`/api/qr_code/${res_2.data.bill_id}`)

      const body = {
        bill_id: res_2.data.bill_id,
        line_id: payload.chatty.line_id,
        qrUrl: res_3.data.url.qr_image_url,
      }
      // save body to qrcode (this) redux state
      this.setQRcodeState(body)

      // trigger next action to send qrcode from body
      this.sendQrcode()


      // console.log(res)
      // console.log(res2.bill_id);
      // console.log(payload.chatty.line_id);
      // this.setState({
      //   bill_id: res2.bill_id,
      //   line_id: this.payload.chatty.line_id,
        
      // })
      // this.setState((state) => {
      //   return {
      //     bill_id: res_2.data.bill_id,
      //     line_id: res.data.user_id,
      //     qr_url: res_3.data.qr_image_url,
      //   }
      //  })
      // console.log(res_3)
    },
    //API34/4 send QR
    async sendQrcode(payload, rootState) {
      // const res = await request.get(`/api/order/buyer/${payload.chattyUserId}`)
      // const res_2 = await request.get(`/api/getbill_by_order/${res.data.order_id}`)
      // const res_3 = await request.get(`/api/qr_code/${res_2.data.bill_id}`)
      // const body = {
      //   bill_id: res_2.data.bill_id,
      //   line_id: payload.chatty.line_id,
      //   qrUrl: res_3.data.url.qr_image_url,
      // }
      // console.log(payload.chatty.line_id)
      // console.log(res_3.data.url.qr_image_url)
      // console.log(body)

      // const res_4 = await request.post(`/api/qr_code`, body)
      const res_4 = await request.post(`/api/qr_code`, rootState.qrcode)
      console.log(res_4)
      if (res_4) {
        console.log(res_4.data)
        liff.closeWindow()
      }
      console.log(res_4)
    },
  }),
}
