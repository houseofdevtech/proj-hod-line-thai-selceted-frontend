import request from '../../utils/urlRequest'

export const address = {
  state: {
    // address: null,
    // {
    //   name: Jame malalnakrub,
    //   tel: 0895698236,
    //   addr1:
    //   addr2:
    //   postcode:
    // }
    //   user_id: 54,
    //   line_id: 'testliff456',
    //   name: 'Lisa',
    //   telephone: '',
    //   address: '',
    //   full_address: null,
    //   province: null,
    //   district: null,
    //   sub_district: 0, // get :: {} , put :: sub_district_id
  },
  reducers: {
    // handle state changes with pure functions
    setAddress(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        ...payload,
      }
    },

    isCompleteAddress(state, payload) {
      const result =
        state.user_id > 0 &&
        state.line_id !== '' &&
        state.name !== '' &&
        state.telephone !== '' &&
        state.address !== '' &&
        state.sub_district !== 0
      return {
        ...state,
        isCompleteAddress: result,
      }
    },
  },

  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    // async incrementAsync(payload, rootState) {
    //   await new Promise(resolve => setTimeout(resolve, 1000))
    //   dispatch.count.increment(payload)
    // }
    async getAddress(payload, rootState) {
      request.interceptors.request.use((request) => {
        console.log('Starting Request', request)
        return request
      })
      request.interceptors.response.use((response) => {
        console.log('Response:', response)
        return response
      })
      const res = await request.get(`/api/user/${payload.chattyUserId}`)
      if (res) {
        // console.log(res.data)
        // this.setAddress(res.data).then(() => this.isCompleteAddress() )
        this.setAddress(res.data).then(() => this.isCompleteAddress() )
      }
      return res.data
    },

    async editAddress(payload, rootState) {
      const outgoingMsg = {
        name: payload.name,
        telephone: payload.telephone,
        address: payload.address,
        sub_district_id: payload.sub_district.sub_district_id,
      }
      console.log(`outgoingMsg :: ${JSON.stringify(outgoingMsg)}`)
      const res = await request.post(`/api/user/${payload.user_id}`,outgoingMsg)
      if (res){
        console.log(res.data)
      }
      console.log('editAddress')
      // await new Promise(resolve => setTimeout(resolve, 1000))
      // const res = {messages:"ok"}
      return res
    },
  }),
}
