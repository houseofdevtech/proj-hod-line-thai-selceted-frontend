import request from '../../../utils/urlRequest'

export const edit = {
  state: {
    product: [],
  },
  reducers: {
    setEdit(state, payload) {
      console.log(`setEdit :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        ...payload,
      }
    },
    setProduct(state, payload) {
      console.log(`setEdit :: ${JSON.stringify(payload)} `)
      let result = state.product
      result.push(payload)
      console.log(`setEdit (result ):: ${JSON.stringify(result)} `)
      return {
        ...state,
        product: result,
      }
    },
    clearProduct(state, payload) {
      console.log(`setEdit :: clearProduct `)
      return {
        ...state,
        product: [],
      }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions

    // API 11 - get_products (get)
    async getProduct(payload, rootState) {
      //  await new Promise(resolve => setTimeout(resolve, 1000))
      //  dispatch.count.increment(payload)
      const res = await request.get(
        `/api/products/shop/${payload.shop_id}?product_id=${payload.product_id}`
      )
      if (res) {
        // console.log(res)
        if ('message' in res.data) {
          console.log(res.data.message)
        } else {
          this.setProduct(res.data)
        }
      }
      return res.data
    },
  }),
}
