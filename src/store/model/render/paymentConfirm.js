import request from '../../../utils/urlRequest'

export const paymentConfirm = {
  state: {
    // address: null,
    // {
    //   name: Jame malalnakrub,
    //   tel: 0895698236,
    //   addr1:
    //   addr2:
    //   postcode:
    // }
    //   user_id: 54,
    //   line_id: 'testliff456',
    //   name: 'Lisa',
    //   telephone: '',
    //   address: '',
    //   full_address: null,
    //   province: null,
    //   district: null,
    //   sub_district: 0, // get :: {} , put :: sub_district_id
  },
  reducers: {
    // handle state changes with pure functions
    setAddress(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        address: { ...payload },
      }
    },
    setOrder(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        order: { ...payload },
      }
    },
    setBank(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        bank: payload,
      }
    },
    setUpdated(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        isUpdated: payload,
      }
    },
    setError(state, payload) {
      console.log(`setError :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        error: payload,
      }
    },
 
    setPayment(state, payload){
      return {
        ...state,
        ...payload,
      }
    },
    
    isCompleteAddress(state, payload) {
      const result =
        state.address.user_id > 0 &&
        state.address.line_id !== '' &&
        state.address.name !== '' &&
        state.address.telephone !== '' &&
        state.address.address !== '' &&
        state.address.sub_district !== 0
      return {
        ...state,
        isCompleteAddress: result,
      }
    },
    isEmptyQuantity(state, payload) {
      console.log(`isEmptyQuantity :: ${JSON.stringify(payload)}`)
      // let result = false
      const result = payload.data_order.order_items.some((product) => {
        return product.product_items.some(
          (obj) => obj.qty === 0
        )
      })
      return { ...state, isEmptyStockTrigger: result }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    //  await new Promise(resolve => setTimeout(resolve, 1000))
    //  dispatch.count.increment(payload)

    // API 03 - get_address_user
    async getAddress(payload, rootState) {
      const res = await request.get(`/api/user/${payload.chattyUserId}`)
      if (res) {
        // console.log(res.data)
        // this.setAddress(res.data).then(() => this.isCompleteAddress())
        this.setAddress(res.data).then(() => this.isCompleteAddress())

        return true
      } else {
        console.log('getAddress:: Cannot fetch the data')
        return false
      }
    },
    // API 32 - get_bills
    async getBills(payload, rootState) {
      const res = await request.get(`/api/bills/${payload.bill_id}`)
      if (res) {
        if (!res.data.message) {
          this.setOrder(res.data)
          this.isEmptyQuantity(res.data)
          return true
        } else {
          this.setError(res.data.message)
          console.log(`error api 29 /order ${res.data.message}`)
          return false
        }
      } else {
        console.log('getOrder:: Cannot fetch the data')
        return false
      }
    },
    // API 49 - all_shop_bank (get)
    async getBank(payload, rootState) {
      const res = await request.get(`/api/shop_bank_all/${payload}`)
      if (res) {
        this.setBank(res.data)
        return true
      } else {
        console.log('getBank:: Cannot fetch the data')
        return false
      }
    },
  }),
}
