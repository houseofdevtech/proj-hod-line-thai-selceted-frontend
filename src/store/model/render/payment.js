import request from '../../../utils/urlRequest'
import _ from 'lodash'

export const payment = {
  state: {
    // address: null,
    // {
    //   name: Jame malalnakrub,
    //   tel: 0895698236,
    //   addr1:
    //   addr2:
    //   postcode:
    // }
    //   user_id: 54,
    //   line_id: 'testliff456',
    //   name: 'Lisa',
    //   telephone: '',
    //   address: '',
    //   full_address: null,
    //   province: null,
    //   district: null,
    //   sub_district: 0, // get :: {} , put :: sub_district_id
    // order: {
    //   shop_id: 30,
    //   payment_options: { qr_code: true, bank: true, credit_card: false },
    //   user_id: 18,
    //   order_id: 70,
    //   order_items: [
    //     {
    //       product_id: 209,
    //       product_name: 'BEVERLY HILLS POLO CLUB (UNISEX) - YELLOW',
    //       product_items: [
    //         {
    //           order_items_id: 358,
    //           sku_id: 1783,
    //           sku_code: 'BHU004_2',
    //           color: { color_name: 'YELLOW', color_value: '#e0c948', color_img_path: '' },
    //           size: 'L',
    //           price: 199,
    //           qty: 2,
    //           qty_product_items: 6,
    //           sum: 398,
    //         },
    //         {
    //           order_items_id: 359,
    //           sku_id: 1782,
    //           sku_code: 'BHU004_1',
    //           color: { color_name: 'YELLOW', color_value: '#e0c948', color_img_path: '' },
    //           size: 'M',
    //           price: 199,
    //           qty: 2,
    //           qty_product_items: 6,
    //           sum: 398,
    //         },
    //       ],
    //     },
    //   ],
    //   shipping_price: { std: 50, ems: 100, kerry: 150 },
    //   sum: 796,
    //   create_date: '2020-02-18 15:23:49',
    //   update_date: '2020-02-18 15:23:49',
    // },
    allProductId: null,
  },
  reducers: {
    // handle state changes with pure functions
    setAddress(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        address: { ...payload },
      }
    },
    setOrder(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        order: { ...payload },
      }
    },
    setBank(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        bank: payload,
      }
    },
    setUpdated(state, payload) {
      // console.log(state)
      // console.log(payload)
      return {
        ...state,
        isUpdated: payload,
      }
    },
    setError(state, payload) {
      console.log(`setError :: ${JSON.stringify(payload)} `)
      return {
        ...state,
        error: payload,
      }
    },
    setProductIds(state, payload) {
      console.log(`setProductIds :: ${JSON.stringify(payload)} `)
      if (payload !== '') {
        let result = payload.order_items.map((product) => {
          return product.product_id
        })
        return { ...state, allProductId: result }
      }
      return { ...state }
    },

    setPayment(state, payload) {
      return {
        ...state,
        ...payload,
      }
    },

    isCompleteAddress(state, payload) {
      const result =
        state.address.user_id > 0 &&
        state.address.line_id !== '' &&
        state.address.name !== '' &&
        state.address.telephone !== '' &&
        state.address.address !== '' &&
        state.address.sub_district !== 0
      return {
        ...state,
        isCompleteAddress: result,
      }
    },
    isEmptyQuantity(state, payload) {
      console.log(`isEmptyQuantity :: ${JSON.stringify(payload)}`)
      // let result = false
      if (payload !== '') {
        const result =
          payload.order_items.some((product) => {
            return product.product_items.some((obj) => obj.qty === 0)
          }) || _.isEmpty(payload.order_items)
        return { ...state, isEmptyStockTrigger: result }
      }
      return { ...state }
    },
    isEmptyOrder(state, payload) {
      console.log(`isEmptyOrder :: ${JSON.stringify(payload)}`)
      // let result = false
      const result = _.isEmpty(payload.order_items)
      return { ...state, isEmptyOrderTrigger: result }
    },
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    //  await new Promise(resolve => setTimeout(resolve, 1000))
    //  dispatch.count.increment(payload)

    // API 03 - get_address_user
    async getAddress(payload, rootState) {
      const res = await request.get(`/api/user/${payload.chattyUserId}`)
      if (res) {
        // console.log(res.data)
        // this.setAddress(res.data).then(() => this.isCompleteAddress())
        this.setAddress(res.data).then(() => this.isCompleteAddress())

        return true
      } else {
        console.log('getAddress:: Cannot fetch the data')
        return false
      }
    },
    // API 29 - get_order
    async getOrder(payload, rootState) {
      const res = await request.get(`/api/order/buyer/${payload.chattyUserId}?shop=45`)
      if (res) {
        if (!res.data.message) {
          this.setOrder(res.data)
          this.setProductIds(res.data)
          this.isEmptyQuantity(res.data)
          this.isEmptyOrder(res.data)
          return true
        } else {
          this.setError(res.data.message)
          console.log(`error api 29 /order ${res.data.message}`)
          return false
        }
      } else {
        console.log('getOrder:: Cannot fetch the data')
        return false
      }
    },
    // API 49 - all_shop_bank (get)
    async getBank(payload, rootState) {
      const res = await request.get(`/api/shop_bank_all/${payload}`)
      if (res) {
        this.setBank(res.data)
        return true
      } else {
        console.log('getBank:: Cannot fetch the data')
        return false
      }
    },
  }),
}
