import styled, { css } from 'styled-components'

export const Container = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
      color: ${props.theme.text};
    `
  }}
  display: flex;
  flex-direction: column;
  flex: 1;

  // height: 100vh;
  // width: 100vw;
  align-content: space-between;
  justify-content: space-between;

  @media only screen and (min-width: 451px) {
    width: 500px;
  }
`

export const WhiteContainer = styled.div`
  background: #fff;
`

export const ThemesContainer = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
    `
  }}
  flex:1;
`
export const ThemesContainerFooter = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
    `
  }}
  display:block;
  height: 100px;
`

export const FrameContainer = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
    `
  }}
  ${(props) => {
    if (props.paddingBottom) {
      return css`
        padding: 5px 30px ${props.paddingBottom};
        @media only screen and (min-width: 451px) {
          padding: 5px 30px ${props.paddingBottom};
        }
      `
    }
    return `
      padding: 5px 30px 10px;
      @media only screen and (min-width: 451px) {
        padding: 5px 30px 10px;
      }
    `
  }}
  
  border-radius: 0 0 7vw 0;

  @media only screen and (min-width: 451px) {
    width: inherit;
    padding: 5px 30px 10px;
    border-radius: 0 0 50px 0;
  }
`

export const TitleArea = styled.div`
  display: flex;
  justify-content: space-between;
`
export const Title = styled.div`
  align-self: center;
  text-align: left;
  // font-size: 16px;
  font-size: 1em;
  font-weight: 400;
  width: 55vw;
  span {
    font-size: 5vw;
    font-weight: 700;
  }
  @media only screen and (max-width: 360px) {
    font-size: 0.9em;
    width: 50vw;
  }
  @media only screen and (min-width: 451px) {
    font-size: 1em;
    font-weight: 400;
    width: inherit;
    span {
      font-size: 1.5em;
      font-weight: 700;
    }
  }
`

export const Text = styled.div`
  font-size: 1em;

  @media only screen and (max-width: 360px) {
    font-size: 0.7em;
  }
  @media only screen and (min-width: 451px) {
    font-size: 1.5em;
  }
`

export const Price = styled.div`
  font-size: 1em;
  text-align: left;
  // font-size: 5vw;
  font-weight: 400;
  span {
    // font-size: 4vw;
    font-weight: 100;
  }
  @media only screen and (max-width: 360px) {
    font-size: 0.9em;
    font-weight: 400;
    span {
      font-size: 1.3em;
      font-weight: 700;
    }
  }
  @media only screen and (min-width: 451px) {
    font-size: 0.9em;
    font-weight: 400;
    span {
      font-size: 1.3em;
      font-weight: 700;
    }
  }
`
export const DescriptionArea = styled.div`
  margin-top: 10px;
  text-align: left;
  font-size: 3.5vw;

  @media only screen and (min-width: 451px) {
    font-size: 1em;
  }
`

export const MoreBar = styled.div`
  margin-top: 10px;
  display: flex;
  flex-direction: column;
`

export const More = styled.div`
  align-self: flex-end;
  font-size: 3.5vw;
  cursor: pointer;
`

export const BottomRowContainer = styled.div`
  display: flex;
  width: 100%;
`

export const BottomColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
      // padding-bottom: 10vh;
    `
  }}
`

export const BottomLeftFiller = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
    `
  }}
  position: relative;
  top: 1px;
  // left: -5px;
  // bottom: 0px;
  width: 3vw;
  @media only screen and (min-width: 961px) {
    width: 10px;
  }
  // width: 12px;
  // height: 100%;
`

export const BottomLeftBackground = styled.div`
  background: #fff;

  // width: 17px;
  // border-radius: 0 0 30px 0;
  position: relative;
  height: 99.8%;
  z-index: 2;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`
export const BottomLeftSquare = styled.div`
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
    `
  }}
  // background: #fff;
  position: relative;
  top: -1px;
  width: 100%;
  height: 100%;
  border-radius: 0 0 120%/120px 0;
  z-index: 5;
`

export const Warning = styled.div`
  font-size: 4vw;
  color: red;
`

export const SelectedContainer = styled.div`
  position: relative;
  top: -1vh;
  height: 100%;
  background: #fff;
  margin-top: 1vh;
  display: flex;
  flex: 1;
  flex-direction: column;
  overflow: auto;
  border-radius: 7vw 0 7vw 0;

  @media only screen and (min-width: 451px) {
    width: 100%;
    overflow-x: hidden;
    border-radius: 50px 0 50px 0;
  }
`

export const SelectedSection = styled.div`
  margin: 10px 15px 0px 10px;
  @media only screen and (min-width: 451px) and (max-width: 960px) {
    width: 90%;
    margin: 20px 15px 0px 20px;
  }
  @media only screen and (min-width: 961px) {
    width: 90%;
    margin: 20px 15px 0px 20px;
  }
`

export const SelectedBar = styled.div`
  margin-top: 1vh;
  padding-left: 15px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;

  @media only screen and (min-width: 451px) and (max-width: 960px) {
    margin-top: 10px;
    padding: 15px 0px 15px 20px;
  }
  @media only screen and (min-width: 961px) {
    margin-top: 10px;
    padding: 15px 0px 15px 20px;
  }
`

// export const SelectedAmount = styled.div`
//   margin-top: 1vh;
//   padding-left: 15px;
//   display: flex;
//   justify-content: space-between;
// `

export const Divider = styled.div`
  border-top: 1.5px solid #d1ceda;
  // padding: 10px 0px;
  margin: 10px 0px;
  // box-shadow: 2px 2px 5px grey;
`
