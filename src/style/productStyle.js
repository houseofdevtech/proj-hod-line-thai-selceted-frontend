import styled, { css } from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  // position: absolute;
  // flex: 1;
  ${(props) => {
    return css`
      background: ${props.theme.tertiary};
      color: ${props.theme.text};
    `
  }}
  overflow: hidden;
`
export const SubContainer = styled.div`
  /* Mobile Styles */
  @media only screen and (max-width: 450px) {
    width: 100%;
  }

  /* Tablet & Styles */
  @media only screen and (min-width: 451px) {
    width: 500px;
  }
`

export const Header = styled.div`
  z-index: 300;
  // position: relative;
  display:block;
  // display: flex;
  // flex-direction: column;
  // flex: 1;
  // font-size: 1rem;
  // text-align: center;
  // justify-content: flex-start;
  justify-content: space-between;

  overflow: auto;
  &::-webkit-scrollbar {
    display: none;
    width: 0px;
    background: transparent;
  }
  width: inherit;
  padding-bottom: 20vh;
  // height: 100%;
  // height: 120vh;
`

export const Footer = styled.div`
  z-index: 300;
  position: fixed;
  bottom: 0;
  flex:1;
  // right: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  ${(props) => {
    return css`
      background: ${props.primaryColor ? props.primaryColor : props.theme.tertiary};
    `
  }}
  ${(props) => {
    if (props.height) {
      return css`
        // height: ${props.height}vh;
        // height: max(${props.height}vh,(${props.height}*0.5)vw) ;
        @media only screen and (min-width: 451px) {
          height: 80px;
        }
        // height: 22vw;
        height: 24vw;
        `
    }
    return css`
      @media only screen and (min-width: 451px) {
        height: 55px;
      }
      // height: 16vw;
      height: 18vw;
    `
  }}
  
  ${(props) => {
    if (props.width) {
      return css`
        width: ${props.width}px;
      `
    }
    return css`
      width: inherit;
      // width: 100%;
    `
  }}
  div {
    color: ${(props) => props.theme.textRed};
  }
`
export const FooterContainer = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  
  align-self:center;
  // width: 100%;
  width: inherit;
  ${(props) => {
    if (props.height) {
      return css`
        // height: ${props.height}vh ;
        // height: min(${props.height}vh,(${props.height}*0.6)vw) ;
        @media only screen and (min-width: 451px)  {
          height: 80px;
          padding-bottom: 0px;
        }
        height:  22vw;
        padding-bottom: 30px;
      `
    }
    return css`
      // height: 9vh;
      @media only screen and (min-width: 451px) {
        height: 55px;
        padding-bottom: 0px;
      }
      height: 16vw;
      padding-bottom: 30px;
    `
  }}
  
  background: #fff;
  // background: ${(props) => props.theme.primary};
  border-radius: 20px 20px 0 0;
  // padding: 0 1vw 0 1vw;
`

export const FooterText = styled.div`
  display: block;
  align-self: center;
  margin-bottom: 2vw;
  ${(props) => {
    if (props.medium) {
      return css`
        div {
          font-size: 1.0em;
        }
      `
    }
    return css`
      div {
        font-size: 0.9em;
      }
    `
  }}}

  @media only screen and (min-width: 451px) {
    margin-bottom: 7px;
  }
`

export const ButtonContainer = styled.div`
  position: relative;
  display: flex;
  // height: 7vh;
  height: 12vw;
  @media only screen and (min-width: 451px) {
    height: 40px;
  }
  width: 100%;
  background: #fff;
  border-radius: 15px 15px 0 0;
`
