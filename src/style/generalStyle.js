import styled, { css } from "styled-components";

export const Container = styled.div`
  ${(props) => {
    if (props.overflowHidden) {
      return css`
        overflow-y: hidden;
      `;
    }
    return css`
    background: ${
      props.primaryColor ? props.primaryColor : props.theme.secondary
    } ;
      // color: ${props.theme.text};
    overflow-y: auto;
    `;
  }}

  ${(props) => {
    if (props.frameHeight) {
      return css`
        height: ${props.frameHeight};
      `;
    }
    return css`
      height: 100vh;
    `;
  }}

  display: flex;
  flex-direction: column;
  align-items: center;
  // flex: 1;

  // height: max(100vh, 100vw);
  // width: 100vw;
  // align-content: space-between;
  // justify-content: space-between;
`;

export const SubContainer = styled.div`
  /* Mobile Styles */
  height: max-content;
  @media only screen and (max-width: 450px) {
    width: 100%;
  }

  /* Tablet & Styles */
  @media only screen and (min-width: 451px) {
    width: 500px;
  }
`;

export const TopBackground = styled.div`
  ${(props) => {
    return css`
      background: ${
        props.secondaryColor ? props.secondaryColor : props.theme.tertiary
      };
      // background: ${props.theme.tertiary};
    `;
  }}
`;

export const TopContainer = styled.div`
  ${(props) => {
    if (props.topHeight) {
      return css`
        height: ${props.topHeight};
      `;
    }
    return css`
      height: 60px;
    `;
  }}

  ${(props) => {
    return css`
    background: ${
      props.primaryColor ? props.primaryColor : props.theme.secondary
    };
    // background: ${props.theme.secondary};
    // color: ${props.theme.text};
`;
  }}
  // display:flex; 
  display:block;
  border-radius: 0 0 30px 0;
`;

export const TopContent = styled.div`
  padding: 19px 0 0 24px;
  display: flex;
  // flex: 1;
  width: 90vw;
  overflow: auto;
`;

export const ButtonContainer = styled.div`
  display: flex;
  
`;

export const ThemesContainer = styled.div`
  ${(props) => {
    return css`
    background: ${
      props.primaryColor ? props.primaryColor : props.theme.secondary
    };
      // background: ${props.theme.secondary};
    `;
  }}
  flex:1;
  overflow: hidden;
`;

export const BottomRowContainer = styled.div`
  display: flex;
  overflow: hidden;
`;

export const BottomColumnContainer = styled.div`
  display: flex;
  flex-direction: column
  overflow:auto;
  // padding-bottom: 20px;
  ${(props) => {
    if (props.paddingBottom) {
      return css`
        padding-bottom: 15vh;
      `;
    }
  }}
`;

export const BottomLeftFiller = styled.div`
  ${(props) => {
    return css`
    // background: ${
      props.primaryColor ? props.primaryColor : props.theme.secondary
    };
    background: ${
      props.secondaryColor ? props.secondaryColor : props.theme.tertiary
    };
      // background: ${props.theme.secondary};
    `;
  }}
  // background: #fff;
  position:relative;
  // top: -2px;
  // left: -5px;
  bottom: 1px;

  // width: 12px;
  width: 3vw;
  // height: 100%;
  // margin: 0 0 10px 0;
  // box-shadow: 0 3px 6px rgb(0, 0, 0, 0.29);
`;

export const BottomLeftBackground = styled.div`
  ${(props) => {
    return css`
      background: ${
        props.secondaryColor ? props.secondaryColor : props.theme.tertiary
      };
      // background: ${props.theme.tertiary};
    `;
  }}
  position: relative;
  height: 99.8%;
  z-index: 2;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const BottomLeftSquare = styled.div`
  ${(props) => {
    return css`
    background: ${
      props.primaryColor ? props.primaryColor : props.theme.secondary
    };
      // background: ${props.theme.secondary};
    `;
  }}
  display: block;
  // top: 50px;
  position: relative;
  top: 1px;
  width: 100%;
  height: 100%;
  border-radius: 0 0 120%/120px 0;
  z-index: 2;
`;

export const BottomLeftShadow = styled.div`
  z-index: 1;
  margin-right: -5px;
  width: 2px;
  height: 80%;
  float: right;
  clear: right;
  // border-radius: 0 0 0 -120%/120px ;
  // box-shadow: -5px 3px 6px rgb(0, 0, 0, 0.29);
`;

export const SelectedContainer = styled.div`
  ${(props) => {
    if (props.bodyHeight && props.heightDesktop) {
      return css`
        height: ${props.bodyHeight};
        @media only screen and (min-width: 451px) {
          height: ${props.heightDesktop};
        }
      `;
    }
    if (props.bodyHeight && !props.heightDesktop) {
      return css`
        height: ${props.bodyHeight};
      `;
    }
    return css`
      height: 80vh;
    `;
  }}
  ${(props) => {
    return css`
    background: ${
      props.secondaryColor ? props.secondaryColor : props.theme.white
    };
    // background: ${props.theme.tertiary};
    `;
  }}
  ${(props) => {
    if (props.marginRight) {
      return css`
        margin-right: ${props.marginRight};
      `;
    } else {
      return css`
        margin-right: 0px;
      `;
    }
  }}

  width:100%;
  position: relative;
  top: -1px;
  left: 0px;
  padding: 20px 0 10px 0;
  display: flex;
  flex: 1;
  flex-direction: column;
  overflow: hidden;
  border-radius: 30px 0 30px 0;
  // box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.29);
`;

export const FlexCenterHorizon = styled.div`
  display: flex;
  width: 94%;

  ${(props) => {
    if (props.justifyContent) {
      return css`
        justify-content: ${props.justifyContent};
      `;
    }
  }}
  ${(props) => {
    if (props.marginRight) {
      return css`
        margin-right: ${props.marginRight};
      `;
    } else {
      return css`
        margin-right: 4vw;
      `;
    }
  }}
  ${(props) => {
    if (props.marginLeft) {
      return css`
        margin-left: ${props.marginLeft};
      `;
    } else {
      return css`
        margin-left: 1vw;
      `;
    }
  }}
  @media only screen and (min-width: 451px) {
    margin-right: 1vw;
    margin-left: 1vw;
  }
`;

export const ContentContainer = styled.div`
  z-index: 20;
  position: absolute;
  margin: 20px 0 0 4vw;
  padding: 0px 0 10px 0;

  top: 65px;
  width: 96vw;
  height: 77vh;
  overflow-x: hidden;
  scroll-snap-type: y mandatory;
  scroll-snap-points-y: repeat(200px);
  overflow-y: scroll;
  border-radius: 30px 0 20px 0;

  @media only screen and (min-width: 451px) {
    width: inherit;
    // height: inherit;
    overflow-x: scroll;
    &::-webkit-scrollbar {
      display: none;
      width: 0px !important;
      background: transparent;
    }
  }
`;

export const ContentVerticalContainer = styled.div`
  z-index: 20;
  position: absolute;
  margin: 20px 0 0 4vw;
  // padding: 0 0 0 0;
  top: 65px;
  width: 96vw;
  height: 80vh;
  overflow-y: scroll;
  border-radius: 30px 0 20px 0;

  @media only screen and (min-width: 451px) {
    width: 450px;
  }
`;

// export const FooterFiller = styled.div`
//   ${(props) => {
//     return css`
//       background: ${props.primaryColor};
//     `
//   }}
//   display:flex;
//   width: 100%;
//   height: 100vh;
//   flex:1;
// `

//Black Section for snap scroll for last section
export const EmptyContentContainer = styled.div`
  z-index: 10;
  padding: 0 0 0 20px;
  display: flex;
  height: 150px;
  // background: #fff;
  //margin-bottom: 26px;

  scroll-snap-align: start;
`;

export const Divider = styled.div`
  border-top: 1.5px solid #d1ceda;
  margin: 10px 0px;
  // padding: 10px 0px;
  // box-shadow: 2px 2px 5px grey;
`;

export const MockUI = styled.div`
  z-index: 200;
  position: absolute;
  height: 85vh;
  width: 100vw;
  img {
    width: 100%;
    height: 100%;
    object-fit: fill;
  }
`;
export const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
`;
