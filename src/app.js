import React, { Component } from 'react'
import { connect } from 'react-redux'
// import styled from 'styled-components'

// import Loading from './views/loadingPage'
import AppChatty from './appChatty'

import './index.css'
// import { ProductData } from './inputData/productData'

// [Purpose]
// initiate Liff
// get all user information from liff api to state

const liff = window.liff

// const MainApp = styled.div`
//   position: absolute;
//   width: 50vw;
// `
// const MainAppContainer = styled.div`
//   width: 50vw;
// `

class App extends Component {
  state = {
    accessToken: '',
    displayName: '',
    userId: '',
    pictureUrl: '',
    statusMessage: '',
  }

  componentDidMount() {
    // this.forceUpdate()
    // Load local product to Show Mock up
    // this.props.setProductData(ProductData)
    // this.props.createProduct(ProductData)
    // 191112 Comment for a while until Api ready
    // this.props.createMultiProducts(ProductData)

    // mock UserId
    console.log(`.env :: ${process.env.REACT_APP_DEV}`)
    if (process.env.REACT_APP_DEV === 'local') {
      // this.props.setUserState({ userId: 'testliff3' })
      // Wit line UserId
      // this.props.setUserState({ userId: 'U490acedd6ae42b2d97b939351d6b19b4' })
      // this.props.setUserState({ userId: 'Ub786b410d7e823ef18464bd33bb32191' })
      // Yim line UserId
      // this.props.setUserState({ userId: 'U057929397ad992310eafa5084cdf1efd' })
      // Pon line UserId
      this.props.setUserState({ userId: 'U813c1d6a5af5631875dd3d8450c16e6c' })
      // this.props.setUserState({ userId: 'U490acedd6ae42b2d97b939351d6b19b4'})
      // this.props.setUserState({ userId: 'Ue4896c3cdbe38bba70885c0cedb96e23' })
    } else {
      // this.props.setUserState({ userId: 'wittest001' })
    }

    window.addEventListener('load', this.initialize)
  }

  // setLiffData() {
  //   const browserLanguage = liff.getLanguage()
  //   const sdkVersion = liff.getVersion()
  //   const isInClient = liff.isInClient()
  //   const isLoggedIn = liff.isLoggedIn()
  //   const deviceOS = liff.getOS()

  //   this.props.setAppState({
  //     browserLanguage: browserLanguage,
  //     sdkVersion: sdkVersion,
  //     isInClient: isInClient,
  //     isLoggedIn: isLoggedIn,
  //     deviceOS: deviceOS,
  //   })
  // }
  // {
  //   liffId: '1614257670-5ZnebxeG', // testAPI3-Personal (Wit)
  //   successCallback: (info) => { console.log(JSON.stringify(info))},
  // }
  // { liffId: '1595765269-Jz06QmPW' },  // HOD_SHOP_TEST
  initialize = () => {
    liff.init(
      async (data) => {
        let profile = await liff.getProfile()
        // let accessToken = await liff.getAccessToken()
        console.log(`profile :: ${JSON.stringify(profile)}`)
        //Set User Profile to User state (user.js // redux state)
        this.props.setUserState(profile)
        console.log(`profile.userId :: ${profile.userId}`)
        console.log(`checkUser from liff init`)
        this.props.checkUser(profile.userId)

        //Set UserId to cart for backend to response back qrcode
        // this.props.setCartUserId(profile.userId)
        // this.props.setQrcodeUserId(profile.userId)
        // this.props.setReceiptUserId(profile.userId)

        //Set Liff Data in store/model/appState.js
        // this.setLiffData()
      },
      (err) => {
        console.log('LIFF init failed')
      }
    )

    // try {
    //   ...
    // } catch (err) {
    //   console.log('Try Catch')
    //   console.log(err)
    // }
  }

  // <MainApp className="app-container">
  // <MainAppContainer>

  // {this.props.user.line.userId !== '' ? (
  //   <AppChatty />
  // ) : (
  //   <Loading label="Waiting for Liff init" />
  // )}
  render() {
    // console.log(`user.line.userId :: ${this.props.user.line.userId}`)
    return (
      <div>
        <AppChatty />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  appState: state.appState,
  product: state.product,
  user: state.user,
})

const mapDispatchToProps = (dispatch) => {
  return {
    setUserState: dispatch.user.setUserState,
    checkUser: dispatch.user.checkUser,
    setProductData: dispatch.product.setProductData,

    setAppState: dispatch.appState.setAppState,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
